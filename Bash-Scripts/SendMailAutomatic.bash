
#!/bin/bash
l=0
daydate=`date +"%d.%m.%y"`

path=$(pwd)/

pw=$(sed -n '1p;2q' pw.txt)

checktime="00:00:0"
echo "waiting till ${checktime:0:-2}  and read from database"
while true;
do 
	
	TIME123=`date +%T`
	TIME123=${TIME123:0:-1}

	if [ $TIME123 = $checktime ]
	#if [ $l = 0 ]

		then  
		# immer um 0 Uhr wird die Datenbank ausgelesen
		# k = 0 heisst, dass beim 0-ten Eintrag im Array begonnen wird und dann jeweilig der naechste Eintrag 
		# im Array ausgefuehrt wird
		k=0

		echo "Reading queries from Database"
		query1="USE DatabaseComplete; SELECT ProfileID FROM Profile WHERE Enabled=1 ORDER BY sendingtime ASC; "
		profilid=$(mysql -N -h'localhost' -P'3306' -u'root' -p"$pw" -e"${query1}")
		
		query3="USE DatabaseComplete; SELECT Profilename FROM Profile WHERE Enabled=1 ORDER BY sendingtime ASC; "
		profilname=$(mysql -N -h'localhost' -P'3306' -u'root' -p"$pw" -e"${query3}")
		
		query4="USE DatabaseComplete; SELECT Email FROM User INNER JOIN Profile ON Profile.UserID = 
		User.UserID WHERE Enabled=1 ORDER BY sendingtime ASC; "
		email=$(mysql -N -h'localhost' -P'3306' -u'root' -p"$pw" -e"${query4}")
		
		query5="USE DatabaseComplete; SELECT Sendingtime FROM Profile WHERE Enabled=1 ORDER BY sendingtime ASC; "
		sendingtime=$(mysql -N -h'localhost' -P'3306' -u'root' -p"$pw" -e"${query5}")
		
		profilname=($(for i in $profilname; do echo $i; done))
		profilid=($(for i in $profilid; do echo $i; done))
		email=($(for i in $email; do echo $i; done))
		sendingtime=($(for i in $sendingtime; do echo $i; done))

		sendingtime[$k]=${sendingtime[$k]:0:-3}
		echo "Waiting for ${sendingtime[$k]}, recipient: ${email[$k]}"
		((l++))
	
	fi
	
	# am restlichen Tag wird im Abstand von 10 Sekunden die Uhrzeit aus Array ausgelesen 
	TIME123=${TIME123:0:-2}
	
	if [ "${sendingtime[$k]}" = $TIME123 ]
	then

	
	if [ "${profilid[$k]}" != "" ]
	then
	FILE=$path"/"${profilid[$k]}"/"${profilname[$k]}"_"$daydate".pdf"
	
	# hier Presseclipping File erstellen
	`javac -cp .:mail_1.4.jar MainFunction.java`
	`java -cp .:mail_1.4.jar MainFunction ${email[$k]} ${profilname[$k]} $FILE $pw`
	echo "Mail send to ${email[$k]} at ${sendingtime[$k]}"
	echo "------------------------------------"
	((k++))
	if [ "${sendingtime[$k]}" != "" ]
	then
	sendingtime[$k]=${sendingtime[$k]:0:-3}
	echo "Waiting for ${sendingtime[$k]}, recipient: ${email[$k]}"
	fi
	fi
	fi
	`sleep 10s`
done
