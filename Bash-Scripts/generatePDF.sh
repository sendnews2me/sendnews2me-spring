#!/bin/bash

pw=$(sed -n '1p;2q' pw.txt)

ProfilID="$1"

query1="USE DatabaseComplete; SELECT Profilename FROM Profile WHERE ProfileID= "$ProfilID"; "
Profilname=$(mysql -N -h'localhost' -P'3306' -u'root' -p"$pw" -e"${query1}")
if [ "${Profilname}" != "" ]
then

echo $ProfilID >> "log.log"

query2="USE DatabaseComplete; SELECT Email FROM User INNER JOIN Profile ON Profile.UserID = User.UserID
WHERE Profile.ProfileID="$ProfilID"; "
Username=$(mysql -N -h'localhost' -P'3306' -u'root' -p"$pw" -e"${query2}")
day=`date +%A`
daydate=`date +"%d.%m.%y"`

FILENAME=$Profilname"_"$daydate".tex"
echo $FILENAME >> "log.log"
echo "\documentclass[12pt]{scrartcl}% siehe <http://www.komascript.de>
\usepackage[a4paper,left=2cm,right=2cm,top=2cm,bottom=4cm,bindingoffset=5mm]{geometry}

\usepackage[german]{babel}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
% Einstellungen, wenn man deutsch schreiben will, z.B. Trennregeln
\usepackage[utf8]{inputenc}  % f\"ur Unix-Systeme
% erm\"oglicht die direkte Eingabe von Umlauten und ss
% evt. obige Zeile ersetzen durch
% \usepackage[ansinew]{inputenc}  % f\"ur Windows
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{scrpage2}

\usepackage{color}
\definecolor{white}{rgb}{1,1,1}
\definecolor{darkred}{rgb}{0.3,0,0}
\definecolor{darkgreen}{rgb}{0,0.3,0}
\definecolor{darkblue}{rgb}{0,0,0.3}
\definecolor{pink}{rgb}{0.78,0.09,0.51}
\definecolor{purple}{rgb}{0.28,0.24,0.55}
\definecolor{orange}{rgb}{1,0.6,0.0}
\definecolor{grey}{rgb}{0.4,0.4,0.4}
\definecolor{aquamarine}{rgb}{0.4,0.8,0.65}

% für Tabelle über mehrere Seiten
\usepackage{longtable}

\usepackage[colorlinks = true,
linkcolor = grey,
urlcolor  = blue,
citecolor = blue,
anchorcolor = blue]{hyperref}
%\addtokomafont{footsepline}{\color{blue}}

\pagestyle{scrheadings} %Kopf und Fusszeile
%\pagenumbering{Roman} %So werden Seiten nummeriert
\ohead{} 
\ihead{}
\chead{\href{http://sendnews2.me/home}{\includegraphics[scale=0.5]{/opt/fullArticles/images/Logo}}}
\cfoot{\color{darkblue}{Hier geht es zu unserer \href{http://sendnews2.me/home}{Webseite}, den \href{http://sendnews2.me/clipping/}{Quellen} und dem \href{https://sendnews2me.wixsite.com/sendnews2me/impressum}{Impressum} }}               
\setfootsepline{2.5pt}
 
\setlength{\headsep}{60pt} % K\"urzt den Abstand zwischen Kopfzeile und Textk\"orper, aber nicht bei den Chapterseiten
 
\usepackage{array}
 \newcolumntype{C}[1]{>{\arraybackslash}m{#1}}

\begin{document}
\label{Uebersicht}" > $FILENAME
# translating english weekdays to german 
d1="Monday"
d2="Tuesday"
d3="Wednesday"
d4="Thursday"
d5="Friday"
d6="Saturday"
d7="Sunday"
case $day in
  $d1 )	weekday="Montag"; 
  ;;
  $d2 ) weekday="Dienstag";
  ;;
  $d3 ) weekday="Mittwoch";
  ;;
  $d4 ) weekday="Donnerstag";
  ;;
  $d5 ) weekday="Freitag";
  ;;
  $d6 ) weekday="Samstag";
  ;;
  $d7 ) weekday="Sonntag";
  ;;
esac

echo "\begin{center} 
\textbf{"$Profilname"} von "$Username" am "$weekday" den "$daydate" \end{center} \\\\
 \begin{longtable}{|C{2cm}|C{8.7cm}|C{4.35cm}|}
	\hline 
	\textbf{Quelle} & \textbf{Inhalt} & \textbf{Bild} \\\\
	\hline ">> $FILENAME
	
query3="USE DatabaseComplete; SELECT Article.ArticleID FROM Article INNER JOIN ProfileArticle ON Article.ArticleID 
= ProfileArticle.ArticleID INNER JOIN Profile ON Profile.ProfileID = ProfileArticle.ProfileID 
WHERE Profile.ProfileID="$ProfilID" AND PubDate > TIMESTAMP(DATE_SUB(NOW(), INTERVAL 24 hour))ORDER BY PubDate DESC; "
Articles=$(mysql -N -h'localhost' -P'3306' -u'root' -p"$pw" -e"${query3}")

query4="USE DatabaseComplete; SELECT COUNT(Article.ArticleID) FROM Article INNER JOIN ProfileArticle ON Article.ArticleID 
= ProfileArticle.ArticleID INNER JOIN Profile ON Profile.ProfileID = ProfileArticle.ProfileID 
WHERE Profile.ProfileID="$ProfilID"; "
Num=$(mysql -N -h'localhost' -P'3306' -u'root' -p"$pw" -e"${query4}")
Articles=($(for i in $Articles; do echo $i; done))
j=0
while (( j < $Num ))
do 

query5="USE DatabaseComplete; SELECT Title FROM Article INNER JOIN ProfileArticle ON Article.ArticleID 
= ProfileArticle.ArticleID INNER JOIN Profile ON Profile.ProfileID = ProfileArticle.ProfileID 
WHERE Profile.ProfileID= "$ProfilID" AND Article.ArticleID="${Articles[$j]}"; "
Title=$(mysql -N -h'localhost' -P'3306' -u'root' -p"$pw" -e"${query5}")
#Title=($(for i in $Title; do echo $i; done))

query6="USE DatabaseComplete; SELECT Description FROM Article INNER JOIN ProfileArticle ON Article.ArticleID
= ProfileArticle.ArticleID INNER JOIN Profile ON Profile.ProfileID = ProfileArticle.ProfileID 
WHERE Profile.ProfileID= "$ProfilID" AND Article.ArticleID="${Articles[$j]}"; "
Descr=$(mysql -N -h'localhost' -P'3306' -u'root' -p"$pw" -e"${query6}")

query8="USE DatabaseComplete; SELECT SourceName FROM Sources INNER JOIN Article ON
Article.SourceID = Sources.SourceID
INNER JOIN ProfileArticle ON Article.ArticleID = ProfileArticle.ArticleID INNER JOIN Profile ON Profile.ProfileID = ProfileArticle.ProfileID 
WHERE Profile.ProfileID= "$ProfilID" AND Article.ArticleID="${Articles[$j]}"; "
Source=$(mysql -N -h'localhost' -P'3306' -u'root' -p"$pw" -e"${query8}")

query9="USE DatabaseComplete; SELECT LinkSite FROM Sources INNER JOIN Article ON
Article.SourceID = Sources.SourceID
INNER JOIN ProfileArticle ON Article.ArticleID = ProfileArticle.ArticleID INNER JOIN Profile ON Profile.ProfileID = ProfileArticle.ProfileID 
WHERE Profile.ProfileID= "$ProfilID" AND Article.ArticleID="${Articles[$j]}"; "
SourceLink=$(mysql -N -h'localhost' -P'3306' -u'root' -p"$pw" -e"${query9}")

echo "\href{$SourceLink}{$Source}& \hyperref[art"${Articles[$j]}"]{\textbf{"$Title"} "$Descr"} &
\hyperref[art"${Articles[$j]}"]{\includegraphics[width=0.25\textwidth]{/opt/fullArticles/images/"${Articles[$j]}"}} \\\\  \hline " >> $FILENAME
((j++))
done
echo "\end{longtable}  " >>$FILENAME

i=0
while (( i < $Num ))
do
query5="USE DatabaseComplete; SELECT Title FROM Article INNER JOIN ProfileArticle ON Article.ArticleID 
= ProfileArticle.ArticleID INNER JOIN Profile ON Profile.ProfileID = ProfileArticle.ProfileID 
WHERE Profile.ProfileID= "$ProfilID" AND Article.ArticleID="${Articles[$i]}"; "
Title=$(mysql -N -h'localhost' -P'3306' -u'root' -p"$pw" -e"${query5}")

echo "
\cleardoublepage
\chead{}
\cfoot{Zurück zur \hyperref[Uebersicht]{Übersicht}}
\section*{\color{grey}{"$Title"}}
\label{art"${Articles[$i]}"} 
" >>$FILENAME

FileNa="${Articles[$i]}"
cat "/opt/fullArticles/"$FileNa".txt" >>$FILENAME

query7="USE DatabaseComplete; SELECT Link FROM Article INNER JOIN ProfileArticle ON Article.ArticleID
= ProfileArticle.ArticleID INNER JOIN Profile ON Profile.ProfileID = ProfileArticle.ProfileID 
WHERE Profile.ProfileID= "$ProfilID" AND Article.ArticleID="${Articles[$i]}"; "
Link=$(mysql -N -h'localhost' -P'3306' -u'root' -p"$pw" -e"${query7}")
Link=($(for i in $Link; do echo $i; done))

echo " \\\\
\href{"$Link"}{Zur Nachrichtenseite} " >>$FILENAME
((i++))
done

echo "\end{document}" >> $FILENAME

`sed -i 's,ü,\\\"u,g' ${FILENAME}`
`sed -i 's,Ü,\\\"U,g' ${FILENAME}`

`sed -i 's,ö,\\\"o,g' ${FILENAME}`
`sed -i 's,Ö,\\\"O,g' ${FILENAME}`

`sed -i 's,ä,\\\"a,g' ${FILENAME}`
`sed -i 's,Ä,\\\"A,g' ${FILENAME}`

`sed -i 's,ß,\\\ss ,g' ${FILENAME}`

#`sed "s/>/>\n/g" ${FILENAME}`

#`egrep -v '<([^()]|(?R))*>' ${FILENAME}`

#`grep -v '>$' ${FILENAME}`

abc=$(pdflatex -interaction=batchmode ${FILENAME})
abc=$(pdflatex -interaction=batchmode ${FILENAME})

`cp ${FILENAME} ${Profilname}".pdf"`

`rm ${FILENAME:0:-3}aux`
`rm ${FILENAME:0:-3}out`
#`rm ${FILENAME:0:-3}log`
fi

