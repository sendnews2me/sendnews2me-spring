
#!/bin/bash
l=0
daydate=`date +"%d.%m.%y"`

pw=$(sed -n '1p;2q' pw.txt)

checktime="00:00:0"
echo "waiting till ${checktime:0:-2}  and read from database"
while true;
do 
	
	TIME123=`date +%T`
	TIME123=${TIME123:0:-1}

	if [ $TIME123 = $checktime ]
	#if [ $TIME123 = $TIME123 ] 
	#if [ $l = 0 ]

		then  
		# immer um 0 Uhr wird die Datenbank ausgelesen
		# k = 0 heisst, dass beim 0-ten Eintrag im Array begonnen wird und dann jeweilig der naechste Eintrag 
		# im Array ausgefuehrt wird
		k=0

		echo "Reading queries from Database"
		query1="USE DatabaseComplete; SELECT ProfileID FROM Profile WHERE Profile.Enabled=1 ORDER BY sendingtime ASC;"
		profilid=$(mysql -N -h'localhost' -P'3306' -u'root' -p"$pw" -e"${query1}")
		
		query3="USE DatabaseComplete; SELECT Profilename FROM Profile WHERE Enabled=1 ORDER BY sendingtime ASC; "
		profilname=$(mysql -N -h'localhost' -P'3306' -u'root' -p"$pw" -e"${query3}")
		
		query4="USE DatabaseComplete; SELECT Email FROM User INNER JOIN Profile ON Profile.UserID = 
		User.UserID WHERE Enabled=1 ORDER BY sendingtime ASC; "
		email=$(mysql -N -h'localhost' -P'3306' -u'root' -p"$pw" -e"${query4}")
		
		query5="USE DatabaseComplete; SELECT Sendingtime FROM Profile WHERE Enabled=1 
		ORDER BY sendingtime ASC; "
		sendingtime=$(mysql -N -h'localhost' -P'3306' -u'root' -p"$pw" -e"${query5}")
		


		profilid=($(for i in $profilid; do echo $i; done))
		email=($(for i in $email; do echo $i; done))
		sendingtime=($(for i in $sendingtime; do echo $i; done))
		profilname=($(for i in $profilname; do echo $i; done))

		sendingtime[$k]=${sendingtime[$k]:0:-3}
		prepare[$k]=`date +%T -d "${sendingtime[$k]} 5 minutes ago"`
		prepare[$k]=${prepare[$k]:0:-3}
		echo "Waiting for ${sendingtime[$k]}, Time for preparing PDF: ${prepare[$k]},"
		echo "recipient: ${email[$k]} for profile ${profilid[$k]}"
		echo "read database information" > "log.log"
		((l++))
	fi
	
	# am restlichen Tag wird im Abstand von 10 Sekunden die Uhrzeit aus Array ausgelesen 
	TIME123=${TIME123:0:-2}

	if [ $TIME123 == "${prepare[$k]}" ]
	#if [ "$TIME123" == "$TIME123" ]
	then
	if [ "${profilid[$k]}" != "" ]
	then

	folder=$(mkdir "${profilid[$k]}")
	echo "Prepare PDF for ${email[$k]}, profilid ${profilid[$k]}, at ${prepare[$k]}"
	abc=$(./generatePDF.sh ${profilid[$k]})
	
	$(mv ${profilname[$k]}"_"${daydate}".pdf" ${profilid[$k]}"/")
	$(mv ${profilname[$k]}"_"${daydate}".tex" ${profilid[$k]}"/")
	echo "done"
	echo "------------------------------"
	((k++))
	sendingtime[$k]=${sendingtime[$k]:0:-3}
	prepare[$k]=`date +%T -d "${sendingtime[$k]} 5 minutes ago"`
	prepare[$k]=${prepare[$k]:0:-3}
	if [ "${profilid[$k]}" != "" ]
	then
	echo "EMail will be sent at ${sendingtime[$k]}, Time for preparing PDF: ${prepare[$k]}," 
	echo "recipient: ${email[$k]} for profile ${profilid[$k]}"
	fi
	fi
	fi
	`sleep 10s`


done
