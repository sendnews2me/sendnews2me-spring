package com.sendnews2me.service;

import com.sendnews2me.service.DatabaseRepresentation.Article;
import com.sendnews2me.service.DatabaseRepresentation.Profile;
import com.sendnews2me.service.DatabaseRepresentation.Source;
import com.sendnews2me.service.DatabaseRepresentation.User;
import org.junit.Test;

import java.sql.SQLException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

public class DatabaseForViewTest
{
    DatabaseForView db = new DatabaseForView();


    @Test
    public void User() throws SQLException {
    //    User user = new User(45,"testmail@web.de",32,true,2);
   //     db.addUser(user,"abc");
   //     assertNotNull(user);
   //     User user2;
   //     user2 = db.getUser("testmail@web.de");
   //     assertEquals(45,user2.getUserID());
   //     assertEquals(2,user2.getPermits());
   //     assertEquals(32,user2.getCompanyID());
   //     assertEquals("testmail@web.de",user2.getUserMail());
    }


    @Test
    public void getUserPassword() throws SQLException {
        assertEquals("abc",db.getUserPassword("test@web.de"));
    }

    @Test
    public void getUserAuthentication()
    {
    }

    @Test
    public void getUserProfiles()
    {
        DatabaseForView database = new DatabaseForView();
        List<Profile> profiles = null;
        try {
            profiles = database.getUserProfiles(20);
        } catch (Exception e) {
            System.out.println("Sqlexception");
            System.out.println(e.getMessage());
            e.printStackTrace();
            return;
        }
        if(profiles==null){
            System.out.println("Keine Profile");
            return;
        }

        System.out.println("\n" + profiles.size() + "Profile:");
        for(Profile profile: profiles){
            System.out.println("\nProfil:");
            System.out.println(profile.getProfilID());
            System.out.println(profile.getProfilName());
            System.out.println(profile.getSendingTime());
            System.out.println("\nSearchterms:");
            if(profile.getSearchTerms() != null) {
                for (String searchterm : profile.getSearchTerms()) {
                    System.out.println(searchterm);
                }
            }else{
                System.out.println("keine suchwörter");
            }
            System.out.println("\nSources:");
            if(profile.getSources() != null) {
                for (Source source : profile.getSources()) {
                    System.out.println(source.getSourceName());
                }
            }else{
                System.out.println("keine sources");
            }
            System.out.println("\nArticles");
            if(profile.getArticles()!=null) {
                for (Article article : profile.getArticles()) {
                    System.out.println(article.getTitle());
                }
            }else{
                System.out.println("no articles");
            }
        }
    }

    @Test
    public void getProfileSearchWords()
    {
    }

    @Test
    public void addSearchWords()
    {
    }

    @Test
    public void updateUser()
    {
    }

    @Test
    public void addProfile()
    {
    }

    @Test
    public void addTemplate()
    {
    }

    @Test
    public void removeUser()
    {
        //auskommentiert da Jenkins dies sonst ausführt
        //Boolean result = db.removeUser("testUser");
        //assertEquals(true, result);
    }

    @Test
    public void removeProfile()
    {
        //auskommentiert da Jenkins dies sonst ausführt
       //Boolean result = db.removeProfile("test@web.de", 3);
        //assertEquals(true, result);
    }

    @Test
    public void removeTemplate()
    {
        //auskommentiert da Jenkins dies sonst ausführt

        //Boolean result = db.removeTemplate(1, 1);
        //assertEquals(true, result);
    }

    @Test
    public void createProfile(){
        String profilename = "Apple";
        String[] inputSources = new String[]{"Die Welt, Stuttgarter Nachrichten", "Zeit Online"};
        String[] keywords = new String[]{"Apple", "iPhone", "Macbook"};
        Time sendingtime = Time.valueOf("07:30:00");
        int userID = 1;

        DatabaseForView database = new DatabaseForView();
        Profile profile = database.getProfileFromUser(profilename, userID);
        if(profile != null){
            System.out.println("Profil existiert bereits");
            return;
        }

        profile = new Profile();
        profile.setProfilName(profilename);
        List<Source> sourcesFromDatabase = null;
        try {
            sourcesFromDatabase = database.getSources();
        } catch (SQLException e) {
            System.out.println("fehler beim sourcen holen");
            return;
        }
        List<Source> profileSources = new ArrayList<Source>();
        for(Source source: sourcesFromDatabase){
            for(String sourceName: inputSources){
                if(sourceName.equals(source.getSourceName())){
                    profileSources.add(source);
                    break;
                }
            }
        }
        profile.setSources(profileSources);
        profile.setSendingTime(sendingtime);
        List<String> searchTerms = new ArrayList<String>(Arrays.asList(keywords));
        profile.setSearchTerms(searchTerms);
        profile.setArticles(new ArrayList<Article>());
        profile.setActive(true);
        if(!database.addProfile(userID, profile)){
            System.out.println("Fehler beim Hinzufügen");
            return;
        }
        System.out.println("erfolgreich");
    }

    @Test
    public void updateProfile(){
        //Testvalues
        String oldProfilename = "PolitikNew";
        String newProfilename = "Politik";
        String[] newInputSources = new String[]{"Stuttgarter Nachrichten", "Zeit Online"};
        String[] newKeywords = new String[]{"blabla", "DankeMerkel", "AfD", "Grüne"};
        Time newSendingtime = Time.valueOf("17:20:00");

        DatabaseForView database = new DatabaseForView();
        Profile profile = database.getProfileFromUser(oldProfilename, 20);
        if(profile == null){
            //Zu änderndes Profil existiert nicht in der Datebank
            System.out.println("profile not existing");
            return;
        }

        //Profil ändern
        profile.setProfilName(newProfilename);
        List<Source> sourcesFromDatabase = null;
        try {
            sourcesFromDatabase = database.getSources();
        } catch (SQLException e) {
            System.out.println("sqlexception");
            return;
        }
        List<Source> profileSources = new ArrayList<Source>();
        for(Source source: sourcesFromDatabase){
            for(String sourceName: newInputSources){
                if(sourceName.equals(source.getSourceName())){
                    profileSources.add(source);
                    break;
                }
            }
        }
        profile.setSources(profileSources);
        profile.setSendingTime(newSendingtime);
        List<String> searchTerms = new ArrayList<String>(Arrays.asList(newKeywords));
        profile.setSearchTerms(searchTerms);
        profile.setArticles(new ArrayList<Article>());
        try {
            database.updateProfileData(profile);
        } catch (SQLException e) {
            System.out.println("sqlexception");
        }

        System.out.println("erfolgreich");
    }
}