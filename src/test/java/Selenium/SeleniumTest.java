package Selenium;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.CapabilityType;


import java.io.File;


public class SeleniumTest {

    public void loginasadmin(WebDriver driver){
        driver.navigate().to("http://sendnews2.me");
        driver.findElement(By.className("cd-signin")).click();
        driver.findElement(By.id("signin-email")).sendKeys("test@web.de");
        driver.findElement(By.id("signin-password")).sendKeys("abc");
        driver.findElement(By.name("submit")).click();
        Assert.assertEquals("Error: Login Failed!",
                "http://sendnews2.me/account/login",driver.getCurrentUrl());
    }


    String pathGekodriverWindows =  "C:\\Users\\benni\\IdeaProjects\\geckodriver.exe";
    String pathGekodriverLinux = "/usr/bin/geckodriver";
    String firefoxPathLinux = "/usr/bin/firefox";

    //for linux firefox path
    FirefoxOptions options = new FirefoxOptions();


    @Before
    public void initialise() {
        //options for linux

        System.setProperty("webdriver.gecko.driver",pathGekodriverLinux);
        //System.setProperty("webdriver.firefox.driver",firefoxPathLinux);

        options.setCapability("marionette", true);

        // for Linux firefox path
      /*  File pathBinary = new File(firefoxPathLinux);
        System.out.println(pathBinary);
        FirefoxBinary firefoxBinary = new FirefoxBinary(pathBinary);
        options.setCapability(FirefoxOptions.FIREFOX_OPTIONS, options.setBinary(firefoxBinary));
        options.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);*/
    }

    @Test
    public void anmelden(){
        WebDriver driver = new FirefoxDriver(options);
        driver.navigate().to("http://sendnews2.me");
        driver.findElement(By.className("cd-signin")).click();
        driver.findElement(By.id("signin-email")).sendKeys("test@web.de");
        driver.findElement(By.id("signin-password")).sendKeys("abc");
        driver.findElement(By.name("submit")).click();
        Assert.assertEquals("Error: Login Failed!",
                            "http://sendnews2.me/account/login",driver.getCurrentUrl());
        driver.quit();
    }
    @Test
    public void anmeldenmitfalschenangaben(){
        WebDriver driver = new FirefoxDriver(options);
        driver.navigate().to("http://sendnews2.me");
        driver.findElement(By.className("cd-signin")).click();
        driver.findElement(By.id("signin-email")).sendKeys("test123@web123.de");
        driver.findElement(By.id("signin-password")).sendKeys("abcd123");
        driver.findElement(By.name("submit")).click();
        Assert.assertNotEquals( "Fatal Error: anmeldenmitfalschenangaben",
                                "http://sendnews2.me/account/login",driver.getCurrentUrl());
        driver.quit();
    }
    @Test
    public void anmeldenmitfalschenangaben1(){
        WebDriver driver = new FirefoxDriver(options);
        driver.navigate().to("http://sendnews2.me");
        driver.findElement(By.className("cd-signin")).click();
        driver.findElement(By.id("signin-email")).sendKeys("|@%.$");
        driver.findElement(By.id("signin-password")).sendKeys("$%/");
        driver.findElement(By.name("submit")).click();
        Assert.assertNotEquals( "Fatal Error: anmeldenmitfalschenangaben 1: Sonderzeichen",
                "http://sendnews2.me/account/login",driver.getCurrentUrl());
        driver.quit();
    }
    @Test
    public void anmeldenmitfalschenangaben2(){
        WebDriver driver = new FirefoxDriver(options);
        driver.navigate().to("http://sendnews2.me");
        driver.findElement(By.className("cd-signin")).click();
        driver.findElement(By.id("signin-email")).sendKeys("");
        driver.findElement(By.id("signin-password")).sendKeys("");
        driver.findElement(By.name("submit")).click();
        Assert.assertNotEquals( "Fatal Error: anmeldenmitfalschenangaben 2: Keine Eingabe",
                "http://sendnews2.me/account/login",driver.getCurrentUrl());
        driver.quit();
    }
    @Test
    public void mehrereNutzer(){
        WebDriver driver = new FirefoxDriver(options);
        loginasadmin(driver);

        WebDriver driver2 = new FirefoxDriver(options);
        driver2.navigate().to("http://sendnews2.me");
        driver2.findElement(By.className("cd-signin")).click();
        driver2.findElement(By.id("signin-email")).sendKeys("test2@web.de");
        driver2.findElement(By.id("signin-password")).sendKeys("test");
        driver2.findElement(By.name("submit")).click();
        Assert.assertEquals("Error: Login Failed!",
                "http://sendnews2.me/account/login",driver2.getCurrentUrl());

        WebDriver driver3 = new FirefoxDriver(options);
        driver3.navigate().to("http://sendnews2.me");
        driver3.findElement(By.className("cd-signin")).click();
        driver3.findElement(By.id("signin-email")).sendKeys("test3@web.de");
        driver3.findElement(By.id("signin-password")).sendKeys("test");
        driver3.findElement(By.name("submit")).click();
        Assert.assertEquals("Error: Login Failed!",
                "http://sendnews2.me/account/login",driver3.getCurrentUrl());

        driver.quit();
        driver2.quit();
        driver3.quit();
    }

    @Test //Zugriff auf Mitarbeiterverwaltung als admin
    public void adminmenue1(){
        WebDriver driver = new FirefoxDriver(options);
        loginasadmin(driver);
        driver.findElement(By.className("main-nav")).click();
        Assert.assertNotEquals(null, driver.findElement(By.linkText("Mitarbeiterverwaltung")));
        driver.findElement(By.linkText("Mitarbeiterverwaltung")).click();
        Assert.assertEquals("Error: Access to Mitarbeiterverwaltung Failed!",
                "http://sendnews2.me/account/manageUsers",driver.getCurrentUrl());
        driver.quit();
    }

    @Test //Zugriff auf Mitarbeiterverwaltung als NICHT admin
    public void adminmenue2(){
        WebDriver driver = new FirefoxDriver(options);
        driver.navigate().to("http://sendnews2.me");
        driver.findElement(By.className("cd-signin")).click();
        driver.findElement(By.id("signin-email")).sendKeys("test2@web.de");
        driver.findElement(By.id("signin-password")).sendKeys("test");
        driver.findElement(By.name("submit")).click();
        Assert.assertEquals("Error: Login Failed!",
                "http://sendnews2.me/account/login",driver.getCurrentUrl());
        driver.navigate().to("http://sendnews2.me/account/manageUsers");
        Assert.assertEquals("Error: Access to Mitarbeiterverwaltung!","http://sendnews2.me/account/manageUsers", driver.getCurrentUrl());
        driver.quit();
    }

    //NOT FINISHED @Test
    public void suchprofilerstellen(){
        WebDriver driver = new FirefoxDriver(options);
        loginasadmin(driver);
        driver.findElement(By.className("main-nav")).click();
        //Assert.assertNotEquals(null, driver.findElement(By.linkText("Mitarbeiterverwaltung")));
        driver.findElement(By.linkText("Pressespiegel")).click();
        Assert.assertEquals("Error: Access to Mitarbeiterverwaltung Failed!",
                "http://sendnews2.me/clipping",driver.getCurrentUrl());
        driver.findElement(By.className("fab")).click();
        driver.findElement(By.className("mdl-textfield mdl-js-textfield mdl-textfield--floating-label")).sendKeys("Testprofil123");

        //driver.quit();
    }



}
