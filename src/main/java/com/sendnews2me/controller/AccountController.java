package com.sendnews2me.controller;

import com.sendnews2me.service.Constants;
import com.sendnews2me.service.DatabaseForView;
import com.sendnews2me.service.DatabaseRepresentation.Company;
import com.sendnews2me.service.DatabaseRepresentation.Profile;
import com.sendnews2me.service.DatabaseRepresentation.User;
import com.sendnews2me.service.LoggingEntry;
import com.sendnews2me.service.UserActionLogger;

import com.sun.javafx.util.Logging;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.mail.*;
import javax.mail.internet.*;
import java.util.Date;
import java.util.Properties;


/**
 * @author fpreuschoff
 */
@Controller
@RequestMapping("/account")
public class AccountController implements Constants {

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String register(ModelMap model, HttpSession session, @RequestParam(value = "selectedCompany") String selectedCompany, @RequestParam(value = "signup-email") String usermail, @RequestParam(value = "signup-password_1") String password1, @RequestParam(value = "signup-password_2") String password2) {

        /*
        Registrierungsprozess durchführen:
        -> RequestParams auf Richtigkeit überprüfen
        -> User zu Datenbank hinzufügen
        -> möglicherweise E-Mail an Firmen-Admin senden, dass sich ein neuer User registriert hat?
        -> zu Validierungsseite weiterleiten

        Hinweis: return "redirect:/index?act=reg"; leitet zur Indexseite weiter, öffnet dort das Registrieren
                    Popup und zeigt die übergebene Error-Message an.
        */

        if (usermail.length() < 1 || password1.length() < 1 || selectedCompany.equals("0")) {
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_EMPTY_USER_PW_COMPANY_FIELD);
            return REDIRECT_INDEX_OPEN_SIGNUP;
        } else if (!password1.equals(password2)) {
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_DIFFERENT_PASSWORDS);
            return REDIRECT_INDEX_OPEN_SIGNUP;
        }

        DatabaseForView database = new DatabaseForView();
        User user = null;
        try {
            user = database.getUser(usermail);
        } catch (SQLException e) {
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_SQL_EXCEPTION);
            return REDIRECT_INDEX_OPEN_SIGNUP;
        }

        if (user == null) {
            //Wenn User in Datenbank nicht existiert, erstelle neuen User
            user = new User();
            user.setUserMail(usermail);
            user.setValidated(false);
            user.setPermits(USER);

            //Hole vorhandene Firmen aus Datenbank
            List<Company> companies = null;
            try {
                companies = database.getCompanies();
            } catch (SQLException e) {
                model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_SQL_EXCEPTION);
                return REDIRECT_INDEX_OPEN_SIGNUP;
            }
            //Überprüfe welche CompanyID die vom Benutzer ausgewählte Firma hat
            for (Company company : companies) {
                if (company.getCompanyName().equals(selectedCompany)) {
                    user.setCompanyID(company.getCompanyID());
                    break;
                }
            }

            // schreibe User Action in das Logging-File
            UserActionLogger.logUserAction(user.getCompanyID(), user.getUserMail(), USER_ACTION_REGISTRATION, "Nutzer hat sich registriert.");

            //User in Datenbank hinzufügen
            database.addUser(user, password1);
            model.addAttribute(MODELATTR_SUCCESSMSG, SUCCESSMSG_ACCOUNT_CREATED);

            //Session erzeugen
            session.setAttribute("user", user);
            session.setAttribute("permits", user.getPermits());
            //session.setAttribute("logger", logger);

            //zu Validierungsseite weiterleiten
            return REDIRECT_VALIDATE;
        } else {
            //User existiert bereits
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_USER_ALREADY_EXISTING);
            return REDIRECT_INDEX_OPEN_SIGNUP;
        }
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(ModelMap model, HttpSession session, @RequestParam(value = "signin-email") String usermail, @RequestParam(value = "signin-password") String password) {
        /*
        Login-Prozess durchführen:
        -> Datenbank auf User überprüfen -> wenn ja: Passwort überprüfen
        -> Benutzer bereits validiert?
        -> Session erzeugen
        -> zu Homepage weiterleiten (bereits implementiert)

        Hinweis: return "redirect:/index?act=nuf"; leitet zur Indexseite weiter, öffnet dort das Login
                    Popup und zeigt die übergebene Error-Message an.
        */


        //Email- oder Passwortfeld ist leer -> mit Error zu Indexseite weiterleiten
        if (usermail.length() < 1 || password.length() < 1) {
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_EMPTY_USER_PW_FIELD);
            return REDIRECT_INDEX_OPEN_SIGNIN;
        }

        //User aus Datenbank holen (-> getUser() gibt "null" zurück, wenn Email nicht in Datenbank vorhanden ist)
        DatabaseForView database = new DatabaseForView();
        User user = null;
        try {
            user = database.getUser(usermail);
        } catch (Exception e) {
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_SQL_EXCEPTION);
            return REDIRECT_INDEX_OPEN_SIGNIN;
        }

        if (user != null) {
            // Benutzer in Datenbank vorhanden, überprüfe ob Passwort übereinstimmt
            try {
                if (database.getUserPassword(usermail).equals(password)) {
                    // schreibe User Action in das Logging-File
                    UserActionLogger.logUserAction(user.getCompanyID(), user.getUserMail(), USER_ACTION_LOGIN, "Nutzer hat sich eingeloggt");

                    //E-Mail und Passwort stimmen überein -> Success Message hinzufügen
                    model.addAttribute(MODELATTR_SUCCESSMSG, "Willkommen " + user.getUserMail() + SUCCESSMSG_LOGGED_IN);

                    //Session erzeugen
                    session.setAttribute("user", user);
                    session.setAttribute("permits", user.getPermits());

                    //Wenn Benutzer noch nicht vom Firmen-Admin validiert wurde, leite zur Validate-Seite weiter
                    //sonst -> Indexseite
                    if (!user.isValidated()) {
                        return REDIRECT_VALIDATE;
                    } else {
                        return REDIRECT_INDEX;
                    }

                } else {
                    // schreibe User Action in das Logging-File
                    UserActionLogger.logUserAction(user.getCompanyID(), user.getUserMail(), USER_ACTION_LOGIN, "Nutzer gab falsches Passwort ein.");

                    //Benutzer vorhanden, aber falsches Passwort
                    model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_USERMAIL_OR_PASSWORD);
                    return REDIRECT_INDEX_OPEN_SIGNIN;
                }
            } catch (SQLException e) {
                model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_SQL_EXCEPTION);
                return REDIRECT_INDEX_OPEN_SIGNIN;
            }
        } else {
            //User-Objekt ist null -> Benutzer mit der eingegebenen E-Mail ist nicht in Datenbank vorhanden
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_USERMAIL_OR_PASSWORD);
            return REDIRECT_INDEX_OPEN_SIGNIN;
        }
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(ModelMap model, HttpSession session) {

        /*
        Logout-Prozess durchführen:
        -> Session invalidieren
        -> zu Indexseite weiterleiten
        */
        User user;
        if ((user = (User) session.getAttribute("user")) == null) {
            return REDIRECT_INDEX;
        }

        // schreibe User Action in das Logging-File
        UserActionLogger.logUserAction(user.getCompanyID(), user.getUserMail(), USER_ACTION_LOGOUT, "Nutzer hat sich ausgeloggt.");

        if (session != null) {
            //Wenn Session vorhanden -> invalidieren
            session.invalidate();
        }

        model.addAttribute(MODELATTR_SUCCESSMSG, SUCCESSMSG_LOGGED_OUT);


        return REDIRECT_INDEX;
    }

    @RequestMapping(value = "/resetPassword", method = RequestMethod.POST)
    public String resetPW(ModelMap model, HttpSession session, @RequestParam(value = "reset-email") String usermail) {

        /*
        Passwort vergessen
        -> zufälliges PW generieren
        -> neues PW an Mail senden
        */

        if (usermail.length() < 1) {
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_EMPTY_USER_FIELD);
            return REDIRECT_INDEX_OPEN_SIGNIN;
        }

        DatabaseForView database = new DatabaseForView();
        User user = null;
        try {
            user = database.getUser(usermail);
        } catch (Exception e) {
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_SQL_EXCEPTION);
            return REDIRECT_INDEX_OPEN_SIGNIN;
        }

        if (user != null) {

            // schreibe User Action in das Logging-File
            UserActionLogger.logUserAction(user.getCompanyID(), user.getUserMail(), USER_ACTION_PW_CHANGED, "Passwort aufgrund von \"Passwort vergessen\" geändert.");

            // neues Passwort generieren aus Buchstaben und Zahlen mit der Länge 6
            String avaiableSigns = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890abcdefghijklmnopqrstuvwxyz";
            StringBuilder newPW = new StringBuilder();
            Random rnd = new Random();
            while (newPW.length() < 6) { // length of the random string.
                int index = (int) (rnd.nextFloat() * avaiableSigns.length());
                newPW.append(avaiableSigns.charAt(index));
            }
            String newPassword = newPW.toString();

            // Sende E-Mail an eingegebene Adresse mit neuem Passwort

            Properties properties = new Properties();
            properties.put("mail.smtp.host", "smtp.gmail.com");
            properties.put("mail.smtp.port", "587");
            properties.put("mail.smtp.auth", "true");
            properties.put("mail.smtp.starttls.enable", "true");
            properties.put("mail.user", "sendnewstwome@gmail.com");
            properties.put("mail.password", "Goldyg123");

            try {
                Authenticator auth = new Authenticator() {
                    public PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication("sendnewstwome@gmail.com", "Goldyg123");
                    }
                };
                Session sessionMail = Session.getInstance(properties, auth);
                Message message = new MimeMessage(sessionMail);
                message.setFrom(new InternetAddress("sendnewstwome@gmail.com"));
                message.setRecipient(Message.RecipientType.TO, new InternetAddress(usermail));
                message.setSentDate(new Date());
                message.setSubject("Zurücksetzen Ihres Passworts");
                message.setText("Sehr geehrte/r " + usermail + ",\n\nSie haben für diesen Benutzer der Seite \"SendNews2.Me\" ein neues Passwort angefordert. \n\n" +
                        "Ihr neues Passwort lautet:  " + newPassword + "\n\nFalls Sie kein neues Passwort angefordert haben,\n" +
                        "kontaktieren Sie bitte ihren Ansprechpartner unserer Firma. \n\n\n" +
                        "Mit freundlichen Grüßen\n\nSendNews2.Me");
                Transport.send(message);


                //Passwort in Datenbank aktualisieren
                database.updateUser(usermail, newPassword);


            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            //User-Objekt ist null -> Benutzer mit der eingegebenen E-Mail ist nicht in Datenbank vorhanden
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_USERMAIL_NOT_EXISTING);
            return REDIRECT_INDEX_OPEN_SIGNIN;
        }

        // TODO : Pop up mit Successfull reset pw
        model.addAttribute(MODELATTR_SUCCESSMSG, SUCCESSMSG_PW_SEND);
        return INDEX;
    }


    @RequestMapping(value = "/settings", method = RequestMethod.GET)
    public String showAccountSettings(ModelMap model, HttpSession session) {

        /*
        Anzeigen der Account-Einstellungen (Passwort ändern, Account löschen, ...):
        -> Derzeitige Account-Einstellungen aus Datenbank laden und daraus Model erstellen
        -> zu Account-Einstellungen-Seite weiterleiten
        */

        //Überprüfe ob Account-Einstellungen angezeigt werden können
        if (((User) session.getAttribute("user")) == null) {
            //Benutzer nicht eingeloggt -> zu Startseite weiterleiten
            return REDIRECT_INDEX;
        } else if(!((User) session.getAttribute("user")).isValidated()){
            //Benutzer nicht validiert -> Profile nicht laden -> direkt zu settings
            return ACC_SETTINGS;
        }

        //Benutzer eingeloggt -> zeige Account-Einstellungen
        DatabaseForView database = new DatabaseForView();
        List<Profile> profiles;
        try {
            profiles = database.getProfilesFromUserWithoutArticles(((User) session.getAttribute("user")).getUserID());
        } catch (SQLException e) {
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_SQL_EXCEPTION);
            return ACC_SETTINGS;
        }

        model.addAttribute(MODELATTR_PROFILES, profiles);

        return ACC_SETTINGS;
    }

    @RequestMapping(value = "/mainManageUsers", method = RequestMethod.GET)
    public String mainUserManagement(ModelMap model, HttpSession session) {

        //Überprüfe ob User Management angezeigt werden darf
        if (((User) session.getAttribute("user")) == null) {
            //Benutzer nicht eingeloggt
            return REDIRECT_INDEX;
        } else {
            //Benutzer eingeloggt
            if (!((User) session.getAttribute("user")).isValidated()) {
                //Benutzer noch nicht validiert
                return REDIRECT_VALIDATE;
            } else if (((User) session.getAttribute("user")).getPermits() < FULL_ADMIN) {
                //Benutzer validiert, aber besitzt keine Full-Admin Rechte
                return REDIRECT_INDEX;
            }
        }

        DatabaseForView database = new DatabaseForView();
        List<Company> companies = null;
        try {
            companies = database.getCompanies();
        } catch (SQLException e) {
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_SQL_EXCEPTION);
            return MAIN_USER_MANAGEMENT;
        }

        List<User> allUsers = new ArrayList<User>();
        try {
            allUsers.addAll(database.getUsersFromCompany(0)); //Systemadmins
        } catch (SQLException e) {
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_SQL_EXCEPTION);
            return MAIN_USER_MANAGEMENT;
        }
        for(User user: allUsers){
            user.setCompanyName("(Main-Admin)");
        }
        for (Company company : companies) {
            List<User> users = null;
            try {
                users = database.getUsersFromCompany(company.getCompanyID());
            } catch (SQLException e) {
                model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_SQL_EXCEPTION);
                return MAIN_USER_MANAGEMENT;
            }
            if(users != null) {
                for (User user : users) {
                    user.setCompanyName(company.getCompanyName());
                }
                allUsers.addAll(users);
            }
        }

        model.addAttribute(MODELATTR_ALL_USERS, allUsers);

        return MAIN_USER_MANAGEMENT;
    }


    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public String deleteAccount(ModelMap model, HttpSession session) {

        /*
        Eingeloggten Account löschen:
        -> aktuell eingeloggten User holen
        -> User aus Datenbank löschen (durch OnCascade werden alle Daten gelöscht z.B. Profile ...)
        -> Session invalidieren
        -> zu Indexseite weiterleiten
        */

        User user = (User) session.getAttribute("user");
        DatabaseForView database = new DatabaseForView();
        database.removeUser(user.getUserMail());

        // schreibe User Action in das Logging-File
        UserActionLogger.logUserAction(user.getCompanyID(), user.getUserMail(), USER_ACTION_ACCOUNT_DELETED, "Account wurde gelöscht.");

        session.invalidate();

        return REDIRECT_INDEX;
    }

    @RequestMapping(value = "/manageUsers", method = RequestMethod.GET)
    public String showUserManagement(ModelMap model, HttpSession session) {

        /*
        User-Management für Firmen-Admin anzeigen:
        -> User Permits überprüfen
        -> alle User mit derselben CompanyID holen
        -> zu management-Seite weiterleiten
        */

        //Überprüfe ob User Management angezeigt werden darf
        if (((User) session.getAttribute("user")) == null) {
            //Benutzer nicht eingeloggt
            return REDIRECT_INDEX;
        } else {
            //Benutzer eingeloggt
            if (!((User) session.getAttribute("user")).isValidated()) {
                //Benutzer noch nicht validiert
                return REDIRECT_VALIDATE;
            } else if (((User) session.getAttribute("user")).getPermits() < COMPANY_ADMIN) {
                //Benutzer validiert, aber besitzt keine Firmen-Admin Rechte
                return REDIRECT_INDEX;
            } else if (((User) session.getAttribute("user")).getPermits() == FULL_ADMIN) {
                //Main-Admin darf nur Main-Manage-Users Seite einsehen
                return REDIRECT_INDEX;
            }
        }

        //Benutzer eingeloggt, validiert und bestizt die nötigen Rechte zum Einsehen der Mitarbeiterverwaltung
        //-> hole alle User, mit derselben CompanyID wie der Benutzer, aus der Datenbank
        User user = (User) session.getAttribute("user");
        int companyID = user.getCompanyID();
        DatabaseForView database = new DatabaseForView();
        List<User> users = new ArrayList<User>();
        try {
            users = database.getUsersFromCompany(companyID);
        } catch (SQLException e) {
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_SQL_EXCEPTION);
            return USER_MANAGEMENT;
        }

        //Liste mit User-Objekten zu Model hinzufügen
        model.addAttribute("users", users);

        return USER_MANAGEMENT;
    }

    @RequestMapping(value = "/updateUser", method = RequestMethod.POST)
    public String updateUserPermit(ModelMap model, HttpSession session, @RequestParam(value = "validationType", required = false) String updateType, @RequestParam(value = "toggle", required = false) boolean toggle, @RequestParam(value = "email") String usermail) {

        /*
        Benutzer Rechte aktualisieren (durch Firmen-Admin Toggle Button ausgelöst):
        -> Toggle-Status überprüfen
        -> toggle == 1: übergebenen Benutzer Firmen-Admin-Rechte zuweisen
        -> toggle == 0: übergebenen Benutzer Standard-User-Rechte zuweisen
        -> zu Mitarbeiterverwaltung weiterleiten
        */

        User user;
        String redirectValue = REDIRECT_INDEX;

        //Überprüfe ob User Management angezeigt werden darf
        if ((user = (User) session.getAttribute("user")) == null) {
            //Benutzer nicht eingeloggt
            return REDIRECT_INDEX;
        } else {
            //Benutzer eingeloggt
            if (!((User) session.getAttribute("user")).isValidated()) {
                //Benutzer noch nicht validiert
                return REDIRECT_VALIDATE;

            } else if (((User) session.getAttribute("user")).getPermits() == USER) {
                //Benutzer validiert und Standard-User
                return REDIRECT_INDEX;
            } else if (((User) session.getAttribute("user")).getPermits() == COMPANY_ADMIN) {
                //Benutzer validiert und Firmen-Admin
                redirectValue = REDIRECT_USER_MANAGEMENT;
            } else if (((User) session.getAttribute("user")).getPermits() == FULL_ADMIN) {
                //Benutzer validiert und Full-Admin
                redirectValue = REDIRECT_MAIN_USER_MANAGEMENT;
            }
        }

        DatabaseForView database = new DatabaseForView();
        if (updateType.equals("0")) {
            if (toggle) {
                //Firmen-Admin Toggle ist auf true
                try {
                    //Weise Firmen-Admin-Rechte zu
                    database.updateUserPermit(usermail, COMPANY_ADMIN);

                    // schreibe User Action in das Logging-File
                    UserActionLogger.logUserAction(user.getCompanyID(), user.getUserMail(), USER_ACTION_UPDATED_USER_PERMISSION, "\"" + usermail + "\" wurden Firmen-Admin-Rechte zugeteilt.");

                    model.addAttribute(MODELATTR_SUCCESSMSG, usermail + SUCCESSMSG_COMPANY_PERMITS_GIVEN);
                } catch (SQLException e) {
                    model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_SQL_EXCEPTION);
                    return redirectValue;
                }
            } else {
                //Firmen-Admin Toggle ist auf false
                try {
                    //Weise Standard-User-Rechte zu
                    database.updateUserPermit(usermail, USER);

                    // schreibe User Action in das Logging-File
                    UserActionLogger.logUserAction(user.getCompanyID(), user.getUserMail(), USER_ACTION_UPDATED_USER_PERMISSION, "\"" + usermail + "\"wurden Standard-Rechte zugeteilt.");

                    model.addAttribute(MODELATTR_SUCCESSMSG, usermail + SUCCESSMSG_COMPANY_PERMITS_NOT_GIVEN);
                } catch (SQLException e) {
                    model.addAttribute(MODELATTR_ERRORMSG, e.getMessage());
                    return redirectValue;
                }
            }
        } else if (updateType.equals("1")) {
            //invalidate user
            try {
                database.updateUserValidation(usermail, false);

                // schreibe User Action in das Logging-File
                UserActionLogger.logUserAction(user.getCompanyID(), user.getUserMail(), USER_ACTION_UPDATED_USER_PERMISSION, "\"" + usermail + "\" wurde invalidiert.");

                model.addAttribute(MODELATTR_SUCCESSMSG, usermail + SUCCESSMSG_USER_INVALIDATED);
            } catch (SQLException e) {
                model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_SQL_EXCEPTION);
                return redirectValue;
            }
        } else if (updateType.equals("2")) {
            //validate user
            try {
                database.updateUserValidation(usermail, true);

                // schreibe User Action in das Logging-File
                UserActionLogger.logUserAction(user.getCompanyID(), user.getUserMail(), USER_ACTION_UPDATED_USER_PERMISSION, "\"" + usermail + "\" wurde validiert.");

                model.addAttribute(MODELATTR_SUCCESSMSG, usermail + SUCCESSMSG_USER_VALIDATED);
            } catch (SQLException e) {
                model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_SQL_EXCEPTION);
                return redirectValue;
            }
        } else if(updateType.equals("3")){
            //delete User
            if (((User) session.getAttribute("user")) == null) {
                //Benutzer nicht eingeloggt
                return REDIRECT_INDEX;
            } else {
                //Benutzer eingeloggt
                if (!((User) session.getAttribute("user")).isValidated()) {
                    //Benutzer noch nicht validiert
                    return REDIRECT_VALIDATE;
                } else if (((User) session.getAttribute("user")).getPermits() < FULL_ADMIN) {
                    //Benutzer validiert, aber besitzt keine Full-Admin Rechte
                    return REDIRECT_INDEX;
                }
            }

            //permits == fullAdmin -> Benutzer löschen erlaubt
            if(database.removeUser(usermail)){
                //user gelöscht
                model.addAttribute(MODELATTR_SUCCESSMSG, "Benutzer " + usermail + SUCCESSMSG_USER_DELETED);
            }else{
                //user nicht gelöscht
                model.addAttribute(MODELATTR_ERRORMSG, "Benutzer " + usermail + ERRORMSG_USER_NOT_DELETED);
            }
        }

        return redirectValue;
    }

    /**
     * Ließt den Logging-File aus und gibt die Daten an die Logging View weiter
     */
    @RequestMapping(value = "/logging", method = RequestMethod.GET)
    public String readLoggingFile(ModelMap model, HttpSession session) {

        /*
        Logdatei des entsprechenden Unternehmens auslesen
        Daten an die View weiterleiten
         */

        User user;
        if ((user = (User) session.getAttribute("user")) == null) {
            return REDIRECT_INDEX;
        } else {
            if (!((User) session.getAttribute("user")).isValidated()) {
                return REDIRECT_VALIDATE;
            }
        }

        List<LoggingEntry> logs = new ArrayList<LoggingEntry>();
        int companyId = user.getCompanyID();

        // jede Zeile des Logging-Files auslesen
        try {
            BufferedReader reader = new BufferedReader(new FileReader(Constants.PATH_TO_LOGGING_FILES));
            String currentLine;

            while ((currentLine = reader.readLine()) != null) {
                // prüfen, ob der eingeloggte Nutzer zu der entsprechenden Company gehört
                if (isEntryFromCompany(companyId, currentLine)) {
                    // Zeile an Index 0 einfügen
                    // Zuletzt aufgetretene User Actions werden zuerst angezeigt
                    logs.add(0, parseLoggingEntry(currentLine));
                }
            }
        } catch (Exception e) {
            logs.add(new LoggingEntry(new SimpleDateFormat("dd.MM.yy kk:mm:ss").format(new Date()).toString(), "SystemError", "Es ist ein Fehler beim Auslesen der Logging Datei aufgetreten."));
        }

        // Daten an View weiterleiten
        model.addAttribute("logs", logs);

        return LOGGING;
    }

    // prüft der Eintrag zu der Company gehört
    // Company steht am Anfang jedes Logging Eintrags
    private boolean isEntryFromCompany(int companyId, String entry) {

        if (companyId == Integer.parseInt(entry.substring(0, entry.indexOf(';')))) {
            return true;
        } else {
            return false;
        }
    }

    // parsed den Logging Eintrag
    // einzelne Elemente sind mit einem Semikolon ; getrennt
    // companyId;Date;action;message
    private LoggingEntry parseLoggingEntry(String entry) {
        String[] loggingParts = entry.split(";");

        return new LoggingEntry(loggingParts[1], loggingParts[2], loggingParts[3]);
    }

    @RequestMapping(value = "/changePw", method = RequestMethod.POST)
    public String changePw(ModelMap model, HttpSession session, @RequestParam(value = "oldPw", required = true) String oldPw, @RequestParam(value = "newPw", required = true) String newPw, @RequestParam(value = "repeatedPw", required = true) String repeatedPw) {

        if (((User) session.getAttribute("user")) == null) {
            return REDIRECT_INDEX;
        } else {
            if (!((User) session.getAttribute("user")).isValidated()) {
                return REDIRECT_VALIDATE;
            }
        }

        User user = (User) session.getAttribute("user");
        if (newPw.length() == 0 || repeatedPw.length() == 0 || oldPw.length() == 0) {
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_NO_PW_ENTERED);

            // schreibe User Action in das Logging-File
            UserActionLogger.logUserAction(user.getCompanyID(), user.getUserMail(), USER_ACTION_PW_CHANGED, LOGGER_NO_PW_ENTERED);

            return REDIRECT_SETTINGS;
        }

        DatabaseForView database = new DatabaseForView();

        String pw;
        try {
            pw = database.getUserPassword(((User) session.getAttribute("user")).getUserMail());
        } catch (SQLException e) {
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_SQL_EXCEPTION);
            return REDIRECT_SETTINGS;
        }

        if (oldPw.equals(pw) && newPw.equals(repeatedPw)) {
            try {
                database.updateUser(((User) session.getAttribute("user")).getUserMail(), newPw);
            } catch (Exception e) {
                model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_SQL_EXCEPTION);
                return REDIRECT_SETTINGS;
            }
            model.addAttribute(MODELATTR_SUCCESSMSG, SUCCESSMSG_PW_CHANGED);

            // schreibe User Action in das Logging-File
            UserActionLogger.logUserAction(user.getCompanyID(), user.getUserMail(), USER_ACTION_PW_CHANGED, LOGGER_PW_CHANGED);
        } else {
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_PW_CHANGE_FAILED);

            // schreibe User Action in das Logging-File
            UserActionLogger.logUserAction(user.getCompanyID(), user.getUserMail(), USER_ACTION_PW_CHANGED, LOGGER_PW_CHANGE_FAILED);
        }

        return REDIRECT_SETTINGS;
    }

    @RequestMapping(value = "/sendMailToSystemAdmin", method = RequestMethod.POST)
    public String sendMailToSystemAdmin(ModelMap model, HttpSession session, @RequestParam(value = "textareaEmailText", required = true) String userText) {

        if (((User) session.getAttribute("user")) == null) {
            return REDIRECT_INDEX;
        } else {
            if (!((User) session.getAttribute("user")).isValidated()) {
                return REDIRECT_VALIDATE;
            }
        }

        User user = (User) session.getAttribute("user");
        if (userText.length() < 1) {
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_NO_TEXT_ENTERED);

            return REDIRECT_SETTINGS;
        }


        String userMail = ((User) session.getAttribute("user")).getUserMail();

        DatabaseForView database = new DatabaseForView();
        List<String> adminMails;
        try {
            adminMails = database.getSystemAdminMails();
        } catch (SQLException e) {
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_SQL_EXCEPTION);
            return REDIRECT_SETTINGS;
        }


        for(String adminmail:adminMails) {
            // Sende E-Mail an SystemAdmins mit UserText

            Properties properties = new Properties();
            properties.put("mail.smtp.host", "smtp.gmail.com");
            properties.put("mail.smtp.port", "587");
            properties.put("mail.smtp.auth", "true");
            properties.put("mail.smtp.starttls.enable", "true");
            properties.put("mail.user", "sendnewstwome@gmail.com");
            properties.put("mail.password", "Goldyg123");

            try {
                Authenticator auth = new Authenticator() {
                    public PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication("sendnewstwome@gmail.com", "Goldyg123");
                    }
                };
                Session sessionMail = Session.getInstance(properties, auth);
                Message message = new MimeMessage(sessionMail);
                message.setFrom(new InternetAddress("sendnewstwome@gmail.com"));
                message.setRecipient(Message.RecipientType.TO, new InternetAddress(adminmail));
                message.setSentDate(new Date());
                message.setSubject("Anfrage für eine neue Nachrichtenquelle");
                message.setText("Sehr geehrte/r " + adminmail + ",\n\nEin Benutzer ihres Dienstes möchte eine neue Nachrichtenquelle zur Verfügung gestellt haben. \n\n" +
                        "Die Anfrage des Benutzers " + userMail + ":  \n\n---------------------------------------------------\n" + userText + "\n---------------------------------------------------\n\nSie können über den " +
                        "Navigationspunkt \"Quellen\" neue Nachrichtenquellen im System hinterlegen.\nBitte beachten Sie, dass die neue Nachrichtenquelle" +
                        " über einen RSS-Feed verfügen muss um für das System verwertbar zu sein!" +
                        "\n\n\n" +
                        "Mit freundlichen Grüßen\n\nSendNews2.Me");
                Transport.send(message);

            } catch (AddressException e) {
                e.printStackTrace();
            } catch (MessagingException e) {
                e.printStackTrace();
            }
        }

        model.addAttribute(MODELATTR_SUCCESSMSG, SUCCESSMSG_MAIL_SENT);

        return REDIRECT_SETTINGS;
    }
}