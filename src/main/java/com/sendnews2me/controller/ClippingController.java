package com.sendnews2me.controller;

import com.sendnews2me.service.Constants;
import com.sendnews2me.service.DatabaseForView;
import com.sendnews2me.service.DatabaseRepresentation.*;
import com.sendnews2me.service.UserActionLogger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.sql.SQLException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/clipping")
public class ClippingController implements Constants {

    @RequestMapping(value = {"", "/"}, method = RequestMethod.GET)
    public String showPressclipping(ModelMap model, HttpSession session) {

        /*
        Pressespiegel (Archiv) nach aktivierten Profilen gefiltert anzeigen:
        -> Profile des Users aus Datenbank holen
        -> Überprüfen welche Profile aktiviert sind
        -> Alle, in aktivierten Profilen, verknüpften Artikel holen und anzeigen
        -> zu Pressespiegel weiterleiten (bereits implementiert)
        */

        if (((User) session.getAttribute("user")) == null) {
            return REDIRECT_INDEX;
        } else {
            if (!((User) session.getAttribute("user")).isValidated()) {
                return REDIRECT_VALIDATE;
            }
        }

        int userID = ((User)session.getAttribute("user")).getUserID();
        int companyID = ((User)session.getAttribute("user")).getCompanyID();

        DatabaseForView database = new DatabaseForView();
        //Quellen von Datenbank holen für Profilerstellung
        List<Source> sources = null;
        try{
            sources = database.getSources();
        }catch(SQLException e){
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_SQL_EXCEPTION);
            return CLIPPING;
        }
        model.addAttribute(MODELATTR_SOURCES, sources);

        //Templates von Datenbank holen für Profilerstellung
        List<Template> templates = null;
        try{
            templates = database.getTemplatesFromCompany(companyID);
        }catch(SQLException e){
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_SQL_EXCEPTION);
            return CLIPPING;
        }
        model.addAttribute(MODELATTR_TEMPLATES, templates);

        //Profile + damit verknüpfte Artikel aus Datenbank holen
        List<Profile> profiles = null;
        try {
            profiles = database.getUserProfiles(userID);
            if (profiles == null){
                //keine Profile vorhanden
                //model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_NO_PROFILES_EXISTING);
                return CLIPPING;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_SQL_EXCEPTION);
            return CLIPPING;
        }
        model.addAttribute(MODELATTR_PROFILES, profiles);

        return CLIPPING;
    }

    @RequestMapping(value="/updateProfile", method = RequestMethod.POST)
    public String updateProfile(ModelMap model, HttpSession session, @RequestParam(value="updateFunction") String updateFunction, @RequestParam(value="profilID") String profilIDString, @RequestParam(value="newProfilename") String newProfilename){

        /*
        Profil umbenennen/aktivieren/deaktivieren/löschen
         */


        User user = (User)session.getAttribute("user");

        int profilID;
        try{
            profilID = Integer.valueOf(profilIDString);
        }catch(Exception e){
            return REDIRECT_CLIPPING;

        }

        DatabaseForView database = new DatabaseForView();
        String profilename = null;
        try {
            profilename = database.getProfilenameFromProfileID(profilID);
        } catch (SQLException e) {
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_SQL_EXCEPTION);
            return REDIRECT_CLIPPING;
        }

        if(updateFunction.equals("rename")) {
            try {
                database.updateProfilename(profilID, newProfilename);

                // schreibe User Action in das Logging-File
                UserActionLogger.logUserAction(user.getCompanyID(), user.getUserMail(), USER_ACTION_PROFILE_UPDATED, "Profil \"" + profilename + " wurde in \"" + newProfilename + "\" umbenannt.");

                model.addAttribute(MODELATTR_SUCCESSMSG, SUCCESSMSG_PROFILENAME_UPDATED);
            } catch (SQLException e) {
                model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_SQL_EXCEPTION);
                return REDIRECT_CLIPPING;
            }
        } else if(updateFunction.equals("deactivate")){
            try {
                database.updateProfileStatus(profilID, false);

                // schreibe User Action in das Logging-File
                UserActionLogger.logUserAction(user.getCompanyID(), user.getUserMail(), USER_ACTION_PROFILE_UPDATED, "Profil \"" + profilename + "\" wurde deaktiviert.");

                model.addAttribute(MODELATTR_SUCCESSMSG, SUCCESSMSG_PROFILESTATUS_DISABLED + profilename);
            } catch (SQLException e) {
                model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_SQL_EXCEPTION);
                return REDIRECT_CLIPPING;
            }
        }else if(updateFunction.equals("activate")){
            try {
                database.updateProfileStatus(profilID, true);

                // schreibe User Action in das Logging-File
                UserActionLogger.logUserAction(user.getCompanyID(), user.getUserMail(), USER_ACTION_PROFILE_UPDATED, "Profil \"" + profilename + "\" wurde aktiviert.");

                model.addAttribute(MODELATTR_SUCCESSMSG, SUCCESSMSG_PROFILESTATUS_ENABLED + profilename);
            } catch (SQLException e) {
                model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_SQL_EXCEPTION);
                return REDIRECT_CLIPPING;
            }
        }else if(updateFunction.equals("delete")){
            database.removeProfile(profilID);

            // schreibe User Action in das Logging-File
            UserActionLogger.logUserAction(user.getCompanyID(), user.getUserMail(), USER_ACTION_PROFILE_UPDATED, "Profil \"" + profilename + "\" wurde entfernt.");

            model.addAttribute(MODELATTR_SUCCESSMSG, "Profil " + profilename + SUCCESSMSG_PROFILE_DELETED);
        }else{
            System.out.println("no correct function");
        }

        return REDIRECT_CLIPPING;
    }

    @RequestMapping(value = "/changeProfileData", method = RequestMethod.POST)
    public String changeProfile(ModelMap model, HttpSession session, @RequestParam(value="oldProfilenameChange") String oldProfilename, @RequestParam(value="profilenameChange") String newProfilename, @RequestParam(value="sourceIdListChange") String sourceIdList, @RequestParam(value="sendingTimeSelectValueChange") String sendingTimeSelectValue, @RequestParam(value="searchwordsInputChange") String searchwordsInputString) {


        /*
        Profil ändern
        */

        String[] newInputSources = sourceIdList.replaceAll("Change", "").split(";");
        searchwordsInputString = searchwordsInputString.replaceAll("undefined", "");
        if(searchwordsInputString.length() < 2){
            model.addAttribute(MODELATTR_ERRORMSG, "no keywords");
            return REDIRECT_CLIPPING;
        }
        String[] newKeywords = searchwordsInputString.split(";");

        Time sendingtime;
        if(sendingTimeSelectValue.equals("")){
            //no sending time choosen
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_NO_VALID_SENDINGTIME);
            return REDIRECT_CLIPPING;
        }else if(sendingTimeSelectValue.equals("1")){
            sendingtime = Time.valueOf("00:00:00");
        }else if(sendingTimeSelectValue.equals("2")){
            sendingtime = Time.valueOf("00:30:00");
        }else if(sendingTimeSelectValue.equals("3")){
            sendingtime = Time.valueOf("01:00:00");
        }else if(sendingTimeSelectValue.equals("4")){
            sendingtime = Time.valueOf("01:30:00");
        }else if(sendingTimeSelectValue.equals("5")){
            sendingtime = Time.valueOf("02:00:00");
        }else if(sendingTimeSelectValue.equals("6")){
            sendingtime = Time.valueOf("02:30:00");
        }else if(sendingTimeSelectValue.equals("7")){
            sendingtime = Time.valueOf("03:00:00");
        }else if(sendingTimeSelectValue.equals("8")){
            sendingtime = Time.valueOf("03:30:00");
        }else if(sendingTimeSelectValue.equals("9")){
            sendingtime = Time.valueOf("04:00:00");
        }else if(sendingTimeSelectValue.equals("10")){
            sendingtime = Time.valueOf("04:30:00");
        }else if(sendingTimeSelectValue.equals("11")){
            sendingtime = Time.valueOf("05:00:00");
        }else if(sendingTimeSelectValue.equals("12")){
            sendingtime = Time.valueOf("05:30:00");
        }else if(sendingTimeSelectValue.equals("13")){
            sendingtime = Time.valueOf("06:00:00");
        }else if(sendingTimeSelectValue.equals("14")){
            sendingtime = Time.valueOf("06:30:00");
        }else if(sendingTimeSelectValue.equals("15")){
            sendingtime = Time.valueOf("07:00:00");
        }else if(sendingTimeSelectValue.equals("16")){
            sendingtime = Time.valueOf("07:30:00");
        }else if(sendingTimeSelectValue.equals("17")){
            sendingtime = Time.valueOf("08:00:00");
        }else if(sendingTimeSelectValue.equals("18")){
            sendingtime = Time.valueOf("08:30:00");
        }else if(sendingTimeSelectValue.equals("19")){
            sendingtime = Time.valueOf("09:00:00");
        }else if(sendingTimeSelectValue.equals("20")){
            sendingtime = Time.valueOf("09:30:00");
        }else if(sendingTimeSelectValue.equals("21")){
            sendingtime = Time.valueOf("10:00:00");
        }else if(sendingTimeSelectValue.equals("22")){
            sendingtime = Time.valueOf("10:30:00");
        }else if(sendingTimeSelectValue.equals("23")){
            sendingtime = Time.valueOf("11:00:00");
        }else if(sendingTimeSelectValue.equals("24")){
            sendingtime = Time.valueOf("11:30:00");
        }else if(sendingTimeSelectValue.equals("25")){
            sendingtime = Time.valueOf("12:00:00");
        }else if(sendingTimeSelectValue.equals("26")){
            sendingtime = Time.valueOf("12:30:00");
        }else if(sendingTimeSelectValue.equals("27")){
            sendingtime = Time.valueOf("13:00:00");
        }else if(sendingTimeSelectValue.equals("28")){
            sendingtime = Time.valueOf("13:30:00");
        }else if(sendingTimeSelectValue.equals("29")){
            sendingtime = Time.valueOf("14:00:00");
        }else if(sendingTimeSelectValue.equals("30")){
            sendingtime = Time.valueOf("14:30:00");
        }else if(sendingTimeSelectValue.equals("31")){
            sendingtime = Time.valueOf("15:00:00");
        }else if(sendingTimeSelectValue.equals("32")){
            sendingtime = Time.valueOf("15:30:00");
        }else if(sendingTimeSelectValue.equals("33")){
            sendingtime = Time.valueOf("16:00:00");
        }else if(sendingTimeSelectValue.equals("34")){
            sendingtime = Time.valueOf("16:30:00");
        }else if(sendingTimeSelectValue.equals("35")){
            sendingtime = Time.valueOf("17:00:00");
        }else if(sendingTimeSelectValue.equals("36")){
            sendingtime = Time.valueOf("17:30:00");
        }else if(sendingTimeSelectValue.equals("37")){
            sendingtime = Time.valueOf("18:00:00");
        }else if(sendingTimeSelectValue.equals("38")){
            sendingtime = Time.valueOf("18:30:00");
        }else if(sendingTimeSelectValue.equals("39")){
            sendingtime = Time.valueOf("19:00:00");
        }else if(sendingTimeSelectValue.equals("40")){
            sendingtime = Time.valueOf("19:30:00");
        }else if(sendingTimeSelectValue.equals("41")){
            sendingtime = Time.valueOf("20:00:00");
        }else if(sendingTimeSelectValue.equals("42")){
            sendingtime = Time.valueOf("20:30:00");
        }else if(sendingTimeSelectValue.equals("43")){
            sendingtime = Time.valueOf("21:00:00");
        }else if(sendingTimeSelectValue.equals("44")){
            sendingtime = Time.valueOf("21:30:00");
        }else if(sendingTimeSelectValue.equals("45")){
            sendingtime = Time.valueOf("22:00:00");
        }else if(sendingTimeSelectValue.equals("46")){
            sendingtime = Time.valueOf("22:30:00");
        }else if(sendingTimeSelectValue.equals("47")){
            sendingtime = Time.valueOf("23:00:00");
        }else if(sendingTimeSelectValue.equals("48")){
            sendingtime = Time.valueOf("23:30:00");
        }else{
            //no valid option choosen
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_NO_VALID_SENDINGTIME);
            return REDIRECT_CLIPPING;
        }

        if(newProfilename.length() < 1){
            model.addAttribute(MODELATTR_ERRORMSG, "no profilename");
            return REDIRECT_CLIPPING;
        }else if(newInputSources.length < 1){
            model.addAttribute(MODELATTR_ERRORMSG, "no sources choosen");
            return REDIRECT_CLIPPING;
        }else if(newKeywords.length < 1){
            model.addAttribute(MODELATTR_ERRORMSG, "no keywords");
            return REDIRECT_CLIPPING;
        }


        System.out.println("old: " + oldProfilename);
        System.out.println("new: " + newProfilename);
        System.out.println(sourceIdList);
        System.out.println(searchwordsInputString);
        System.out.println(sendingTimeSelectValue);

        User user;

        if ((user = (User) session.getAttribute("user")) == null) {
            return REDIRECT_INDEX;
        } else {
            if (!((User) session.getAttribute("user")).isValidated()) {
                return REDIRECT_VALIDATE;
            }
        }

        DatabaseForView database = new DatabaseForView();
        Profile profile = database.getProfileFromUser(oldProfilename, ((User)session.getAttribute("user")).getUserID());
        if(profile == null){
            //Zu änderndes Profil existiert nicht in der Datebank
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_PROFILE_NOT_EXISTING);
            return CLIPPING;
        }

        //Profil ändern
        profile.setProfilName(newProfilename);

        List<Source> sourcesFromDatabase = null;
        try {
            sourcesFromDatabase = database.getSources();
        } catch (SQLException e) {
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_SQL_EXCEPTION);
            return CLIPPING;
        }
        List<Source> profileSources = new ArrayList<Source>();
        for(Source source: sourcesFromDatabase){
            for(String sourceID: newInputSources){
                if(sourceID.equals(((Integer)source.getSourceID()).toString())){
                    profileSources.add(source);
                    break;
                }
            }
        }
        profile.setSources(profileSources);
        profile.setSendingTime(sendingtime);
        List<String> searchTerms = new ArrayList<String>(Arrays.asList(newKeywords));
        profile.setSearchTerms(searchTerms);

        profile.setArticles(new ArrayList<Article>());
        try {
            database.updateProfileData(profile);
        } catch (SQLException e) {
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_SQL_EXCEPTION);
            return CLIPPING;
        }

        model.addAttribute(MODELATTR_SUCCESSMSG, SUCCESSMSG_PROFILE_CHANGED);

        // schreibe User Action in das Logging-File
        UserActionLogger.logUserAction(user.getCompanyID(), user.getUserMail(), USER_ACTION_PROFILE_UPDATED, "Profil \"" + newProfilename + "\" wurde aktualisiert.");

        return REDIRECT_CLIPPING;
    }

    @RequestMapping(value = "/createProfile", method = RequestMethod.POST)
    public String createProfile(ModelMap model, HttpSession session,
                                @RequestParam(value="profilename") String profilename,
                                @RequestParam(value="sourceIdList") String sourceIdList,
                                @RequestParam(value="sendingTimeSelectValue") String sendingTimeSelectValue,
                                @RequestParam(value="searchwordsInput") String searchwordsInputString) {

        UserActionLogger logger = (UserActionLogger) session.getAttribute("logger");

        /*
        Profileinstellungen speichern:
        -> Profile des Users aus Datenbank holen
        -> Geänderte Schlagwörter in existierenden Profilen in Datenbank updaten
        -> Neu erstellte Profile in Datenbank hinzufügen
        -> Aktiviert/nicht aktivert -Liste in Datenbank updaten
        -> zu Pressespiegel weiterleiten
        */

        User user;
        if ((user = (User) session.getAttribute("user")) == null) {
            return REDIRECT_INDEX;
        } else {
            if (!((User) session.getAttribute("user")).isValidated()) {
                return REDIRECT_VALIDATE;
            }
        }

        String[] inputSources = sourceIdList.split(";");
        searchwordsInputString = searchwordsInputString.replaceAll("undefined", "");
        if(searchwordsInputString.length() < 2){
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_NO_KEYWORDS);
            return REDIRECT_CLIPPING;
        }
        String[] keywords = searchwordsInputString.split(";");

        Time sendingtime;
        if(sendingTimeSelectValue.equals("")){
            //no sending time choosen
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_NO_VALID_SENDINGTIME);
            return REDIRECT_CLIPPING;
        }else if(sendingTimeSelectValue.equals("1")){
             sendingtime = Time.valueOf("00:00:00");
        }else if(sendingTimeSelectValue.equals("2")){
             sendingtime = Time.valueOf("00:30:00");
        }else if(sendingTimeSelectValue.equals("3")){
             sendingtime = Time.valueOf("01:00:00");
        }else if(sendingTimeSelectValue.equals("4")){
             sendingtime = Time.valueOf("01:30:00");
        }else if(sendingTimeSelectValue.equals("5")){
             sendingtime = Time.valueOf("02:00:00");
        }else if(sendingTimeSelectValue.equals("6")){
             sendingtime = Time.valueOf("02:30:00");
        }else if(sendingTimeSelectValue.equals("7")){
             sendingtime = Time.valueOf("03:00:00");
        }else if(sendingTimeSelectValue.equals("8")){
             sendingtime = Time.valueOf("03:30:00");
        }else if(sendingTimeSelectValue.equals("9")){
             sendingtime = Time.valueOf("04:00:00");
        }else if(sendingTimeSelectValue.equals("10")){
             sendingtime = Time.valueOf("04:30:00");
        }else if(sendingTimeSelectValue.equals("11")){
             sendingtime = Time.valueOf("05:00:00");
        }else if(sendingTimeSelectValue.equals("12")){
             sendingtime = Time.valueOf("05:30:00");
        }else if(sendingTimeSelectValue.equals("13")){
             sendingtime = Time.valueOf("06:00:00");
        }else if(sendingTimeSelectValue.equals("14")){
             sendingtime = Time.valueOf("06:30:00");
        }else if(sendingTimeSelectValue.equals("15")){
             sendingtime = Time.valueOf("07:00:00");
        }else if(sendingTimeSelectValue.equals("16")){
             sendingtime = Time.valueOf("07:30:00");
        }else if(sendingTimeSelectValue.equals("17")){
             sendingtime = Time.valueOf("08:00:00");
        }else if(sendingTimeSelectValue.equals("18")){
             sendingtime = Time.valueOf("08:30:00");
        }else if(sendingTimeSelectValue.equals("19")){
             sendingtime = Time.valueOf("09:00:00");
        }else if(sendingTimeSelectValue.equals("20")){
             sendingtime = Time.valueOf("09:30:00");
        }else if(sendingTimeSelectValue.equals("21")){
             sendingtime = Time.valueOf("10:00:00");
        }else if(sendingTimeSelectValue.equals("22")){
             sendingtime = Time.valueOf("10:30:00");
        }else if(sendingTimeSelectValue.equals("23")){
             sendingtime = Time.valueOf("11:00:00");
        }else if(sendingTimeSelectValue.equals("24")){
             sendingtime = Time.valueOf("11:30:00");
        }else if(sendingTimeSelectValue.equals("25")){
             sendingtime = Time.valueOf("12:00:00");
        }else if(sendingTimeSelectValue.equals("26")){
             sendingtime = Time.valueOf("12:30:00");
        }else if(sendingTimeSelectValue.equals("27")){
             sendingtime = Time.valueOf("13:00:00");
        }else if(sendingTimeSelectValue.equals("28")){
             sendingtime = Time.valueOf("13:30:00");
        }else if(sendingTimeSelectValue.equals("29")){
             sendingtime = Time.valueOf("14:00:00");
        }else if(sendingTimeSelectValue.equals("30")){
             sendingtime = Time.valueOf("14:30:00");
        }else if(sendingTimeSelectValue.equals("31")){
             sendingtime = Time.valueOf("15:00:00");
        }else if(sendingTimeSelectValue.equals("32")){
             sendingtime = Time.valueOf("15:30:00");
        }else if(sendingTimeSelectValue.equals("33")){
             sendingtime = Time.valueOf("16:00:00");
        }else if(sendingTimeSelectValue.equals("34")){
             sendingtime = Time.valueOf("16:30:00");
        }else if(sendingTimeSelectValue.equals("35")){
             sendingtime = Time.valueOf("17:00:00");
        }else if(sendingTimeSelectValue.equals("36")){
             sendingtime = Time.valueOf("17:30:00");
        }else if(sendingTimeSelectValue.equals("37")){
             sendingtime = Time.valueOf("18:00:00");
        }else if(sendingTimeSelectValue.equals("38")){
             sendingtime = Time.valueOf("18:30:00");
        }else if(sendingTimeSelectValue.equals("39")){
             sendingtime = Time.valueOf("19:00:00");
        }else if(sendingTimeSelectValue.equals("40")){
             sendingtime = Time.valueOf("19:30:00");
        }else if(sendingTimeSelectValue.equals("41")){
             sendingtime = Time.valueOf("20:00:00");
        }else if(sendingTimeSelectValue.equals("42")){
             sendingtime = Time.valueOf("20:30:00");
        }else if(sendingTimeSelectValue.equals("43")){
             sendingtime = Time.valueOf("21:00:00");
        }else if(sendingTimeSelectValue.equals("44")){
             sendingtime = Time.valueOf("21:30:00");
        }else if(sendingTimeSelectValue.equals("45")){
             sendingtime = Time.valueOf("22:00:00");
        }else if(sendingTimeSelectValue.equals("46")){
             sendingtime = Time.valueOf("22:30:00");
        }else if(sendingTimeSelectValue.equals("47")){
             sendingtime = Time.valueOf("23:00:00");
        }else if(sendingTimeSelectValue.equals("48")){
             sendingtime = Time.valueOf("23:30:00");
        }else{
            //no valid option choosen
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_NO_VALID_SENDINGTIME);
            return REDIRECT_CLIPPING;
        }

        if(profilename.length() < 1){
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_NO_PROFILENAME);
            return REDIRECT_CLIPPING;
        }else if(inputSources.length < 1){
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_NO_SOURCES);
            return REDIRECT_CLIPPING;
        }else if(keywords.length < 1){
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_NO_KEYWORDS);
            return REDIRECT_CLIPPING;
        }

        DatabaseForView database = new DatabaseForView();
        Profile profile = database.getProfileFromUser(profilename, ((User)session.getAttribute("user")).getUserID());
        if(profile != null){
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_PROFILE_ALREADY_EXISTING);
            return REDIRECT_CLIPPING;
        }

        profile = new Profile();
        profile.setProfilName(profilename);
        List<Source> sourcesFromDatabase = null;
        try {
             sourcesFromDatabase = database.getSources();
        } catch (SQLException e) {
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_SQL_EXCEPTION);
            return CLIPPING;
        }
        List<Source> profileSources = new ArrayList<Source>();
        for(Source source: sourcesFromDatabase){
            for(String sourceID: inputSources){
                if(sourceID.equals(((Integer)source.getSourceID()).toString())){
                    profileSources.add(source);
                    break;
                }
            }
        }
        profile.setSources(profileSources);
        profile.setSendingTime(sendingtime);
        List<String> searchTerms = new ArrayList<String>(Arrays.asList(keywords));
        profile.setSearchTerms(searchTerms);
        profile.setArticles(new ArrayList<Article>());
        profile.setActive(true);
        if(!database.addProfile(((User)session.getAttribute("user")).getUserID(), profile)){
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_SQL_EXCEPTION);
            return CLIPPING;
        }

        // schreibe User Action in das Logging-File
        UserActionLogger.logUserAction(user.getCompanyID(), user.getUserMail(), USER_ACTION_PROFILE_UPDATED, "Profil \"" + profilename + "\" wurde erstellt.");

        model.addAttribute(MODELATTR_SUCCESSMSG, SUCCESSMSG_PROFILE_CREATED);

        return REDIRECT_CLIPPING;
    }

    @RequestMapping(value = "/template", method = RequestMethod.GET)
    public String showTemplates(ModelMap model, HttpSession session) {


        /*
        -> zu Templateerstellung weiterleiten
        */

        if (((User) session.getAttribute("user")) == null) {
            return REDIRECT_INDEX;
        } else {
            if (!((User) session.getAttribute("user")).isValidated()) {
                return REDIRECT_VALIDATE;
            } else if (((User) session.getAttribute("user")).getPermits() < COMPANY_ADMIN){
                return REDIRECT_INDEX;
            } else if (((User) session.getAttribute("user")).getPermits() == FULL_ADMIN){
                return REDIRECT_INDEX;
            }
        }

        DatabaseForView database = new DatabaseForView();
        List<Source> sources = null;
        try {
            sources = database.getSources();
        } catch (SQLException e) {
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_SQL_EXCEPTION);
            return TEMPLATE;
        }

        model.addAttribute(MODELATTR_SOURCES, sources);

        List<Template> templates = null;
        try {
            templates = database.getTemplatesFromCompany(((User)session.getAttribute("user")).getCompanyID());
        } catch (SQLException e) {
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_SQL_EXCEPTION);
            return TEMPLATE;
        }

        model.addAttribute(MODELATTR_TEMPLATES, templates);

        List<Company> companies = null;
        try {
            companies = database.getCompanies();
        } catch (SQLException e) {
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_SQL_EXCEPTION);
            return TEMPLATE;
        }

        for(Company company: companies){
            if(company.getCompanyID() == ((User)session.getAttribute("user")).getCompanyID()){
                model.addAttribute(MODELATTR_COMPANYNAME, company.getCompanyName());
                break;
            }
        }

        return TEMPLATE;
    }

    @RequestMapping(value = "/createTemplate", method = RequestMethod.POST)
    public String createTemplate(ModelMap model, HttpSession session,
                                 @RequestParam(value="templatenameInput") String templatename,
                                 @RequestParam(value="sourceIdList") String sourceIdList,
                                 @RequestParam(value="searchwordsInput") String searchwordsInputString) {
        /*
        -> Template erstellen
        */

        User user;
        if ((user = (User) session.getAttribute("user")) == null) {
            return REDIRECT_INDEX;
        } else {
            if (!((User) session.getAttribute("user")).isValidated()) {
                return REDIRECT_VALIDATE;
            }
        }

        String[] inputSources = sourceIdList.split(";");
        searchwordsInputString = searchwordsInputString.replaceAll("undefined", "");
        if(searchwordsInputString.length() < 2){
            model.addAttribute(MODELATTR_ERRORMSG, "no keywords");
            return REDIRECT_TEMPLATE;
        }
        String[] keywords = searchwordsInputString.split(";");

        if(templatename.length() < 1){
            model.addAttribute(MODELATTR_ERRORMSG, "no profilename");
            return REDIRECT_TEMPLATE;
        }else if(inputSources.length < 1){
            model.addAttribute(MODELATTR_ERRORMSG, "no sources choosen");
            return REDIRECT_TEMPLATE;
        }else if(keywords.length < 1){
            model.addAttribute(MODELATTR_ERRORMSG, "no keywords");
            return REDIRECT_TEMPLATE;
        }

        DatabaseForView database = new DatabaseForView();
        List<Template> templates = null;
        try {
            templates = database.getTemplatesFromCompany(((User)session.getAttribute("user")).getCompanyID());
        } catch (SQLException e) {
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_SQL_EXCEPTION);
            return REDIRECT_TEMPLATE;
        }

        if(templates != null) {
            for (Template template : templates) {
                if (template.getName().equals(templatename)) {
                    model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_TEMPLATE_ALREADY_EXISTING);
                    return REDIRECT_TEMPLATE;
                }
            }
        }

        Template template = new Template();
        template.setName(templatename);
        List<Source> sourcesFromDatabase = null;
        try {
            sourcesFromDatabase = database.getSources();
        } catch (SQLException e) {
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_SQL_EXCEPTION);
            return REDIRECT_TEMPLATE;
        }
        List<Source> templateSources = new ArrayList<Source>();
        for(Source source: sourcesFromDatabase){
            for(String sourceID: inputSources){
                if(sourceID.equals(((Integer)source.getSourceID()).toString())){
                    templateSources.add(source);
                    break;
                }
            }
        }
        template.setSources(templateSources);
        List<String> searchTerms = new ArrayList<String>(Arrays.asList(keywords));
        template.setSearchTerms(searchTerms);

        try {
            if(!database.addTemplate(((User)session.getAttribute("user")).getCompanyID(), template)){
                model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_SQL_EXCEPTION);
                return REDIRECT_TEMPLATE;
            }
        } catch (SQLException e) {
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_SQL_EXCEPTION);
            return REDIRECT_TEMPLATE;
        }

        // schreibe User Action in das Logging-File
        UserActionLogger.logUserAction(user.getCompanyID(), user.getUserMail(), USER_ACTION_TEMPLATE_CREATED, "Template " + templatename + " wurde erstellt.");

        model.addAttribute(MODELATTR_SUCCESSMSG, SUCCESSMSG_TEMPLATE_CREATED);

        return REDIRECT_TEMPLATE;
    }

    @RequestMapping(value = "/deleteTemplate", method = RequestMethod.POST)
    public String deleteTemplate(ModelMap model, HttpSession session, @RequestParam(value = "templateIdToDelete") String templateID){

        /**
         * Delete Template
         */

        User user;
        if ((user = (User) session.getAttribute("user")) == null) {
            return REDIRECT_INDEX;
        } else {
            if (!((User) session.getAttribute("user")).isValidated()) {
                return REDIRECT_VALIDATE;
            }
        }

        DatabaseForView database = new DatabaseForView();
        if(!database.removeTemplate(((User)session.getAttribute("user")).getCompanyID(), Integer.parseInt(templateID))){
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_SQL_EXCEPTION);
            return REDIRECT_TEMPLATE;
        }
        model.addAttribute(MODELATTR_SUCCESSMSG, SUCCESSMSG_TEMPLATE_DELETED);

        // schreibe User Action in das Logging-File
        UserActionLogger.logUserAction(user.getCompanyID(), user.getUserMail(), USER_ACTION_TEMPLATE_DELETED, "Template ID " + templateID + " wurde gelöscht.");

        return REDIRECT_TEMPLATE;
    }

    @RequestMapping(value = "/deleteSource", method = RequestMethod.POST)
    public String deleteSource(ModelMap model, HttpSession session, @RequestParam(value = "sourceIdToDelete") String sourceIDString){

        /**
         * Delete Source
         */

        User user;
        if ((user = (User) session.getAttribute("user")) == null) {
            return REDIRECT_INDEX;
        } else {
            if (!((User) session.getAttribute("user")).isValidated()) {
                return REDIRECT_VALIDATE;
            }
        }

        DatabaseForView database = new DatabaseForView();
        try {
            database.removeSource(Integer.parseInt(sourceIDString));
        } catch (SQLException e) {
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_SQL_EXCEPTION);
            return REDIRECT_SUGGESTSOURCE;
        }
        model.addAttribute(MODELATTR_SUCCESSMSG, SUCCESSMSG_SOURCE_DELETED);

        // schreibe User Action in das Logging-File
        UserActionLogger.logUserAction(user.getCompanyID(), user.getUserMail(), USER_ACTION_SOURCE_DELETED, "Source ID " + sourceIDString + " wurde gelöscht.");

        return REDIRECT_SUGGESTSOURCE;
    }

    @RequestMapping(value = "/suggestSource", method = RequestMethod.POST)
    public String suggestSource(ModelMap model, HttpSession session,@RequestParam(value = "quellenName") String quellenName, @RequestParam(value = "quellenLink") String quellenLink, @RequestParam(value = "quellenRSS") String quellenRSS) {
        /*
        -> zu suggestSource weiterleiten
        */

        User user;
        if ((user = (User) session.getAttribute("user")) == null) {
            return REDIRECT_INDEX;
        } else {
            if (!((User) session.getAttribute("user")).isValidated()) {
                return REDIRECT_VALIDATE;
            } else if (((User) session.getAttribute("user")).getPermits() < FULL_ADMIN){
                return REDIRECT_INDEX;
            }
        }

        boolean sourceExisting;
        DatabaseForView database = new DatabaseForView();
        try{
            sourceExisting = database.sourceExists(quellenName, quellenRSS);
        }
        catch (SQLException e)
        {
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_SQL_EXCEPTION);
            return REDIRECT_SUGGESTSOURCE;
        }

        if(sourceExisting){
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_SOURCE_ALREADY_EXISTING);
            return REDIRECT_SUGGESTSOURCE;
        }

        Source source = new Source();
        source.setSourceName(quellenName);
        source.setLinkSite(quellenLink);
        source.setLinkRSS(quellenRSS);

        try{
        if(database.addSource(source)){
            model.addAttribute(MODELATTR_SUCCESSMSG,SUCCESSMSG_SOURCE_CREATED );
            return REDIRECT_SUGGESTSOURCE;
        }}
        catch(Exception e)
        {
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_SQL_EXCEPTION);
            return REDIRECT_SUGGESTSOURCE;
        }

        // schreibe User Action in das Logging-File
        UserActionLogger.logUserAction(user.getCompanyID(), user.getUserMail(), USER_ACTION_SUGGEST_SOURCE, "Quelle \"" + quellenName + "\" wurde zur Prüfung eingereicht.");

        return REDIRECT_SUGGESTSOURCE;
    }
    @RequestMapping(value = "/suggestSource", method = RequestMethod.GET)
    public String suggestSource(ModelMap model, HttpSession session) {


        /*
        -> zu suggestSource weiterleiten
        */

        // schreibe User Action in das Logging-File

        if (((User) session.getAttribute("user")) == null) {
            return REDIRECT_INDEX;
        } else {
            if (!((User) session.getAttribute("user")).isValidated()) {
                return REDIRECT_VALIDATE;
            } else if (((User) session.getAttribute("user")).getPermits() < FULL_ADMIN){
                return REDIRECT_INDEX;
            }
        }

        DatabaseForView database = new DatabaseForView();
        List<Source> sources;
        try {
            sources = database.getSources();
        } catch (SQLException e) {
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_SQL_EXCEPTION);
            return SOURCE;
        }

        model.addAttribute(MODELATTR_SOURCES, sources);

        return SOURCE;
    }

}
