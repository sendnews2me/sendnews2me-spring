package com.sendnews2me.controller;

import com.sendnews2me.service.Constants;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileNotFoundException;

@Controller
@RequestMapping("/download")
public class DownloadController implements Constants{

    private static final String APPLICATION_PDF = "application/pdf";

    @RequestMapping(value="/check/{profileID}/{profileName}", method=RequestMethod.GET)
    public String checkDownload(ModelMap model, HttpServletResponse response, @PathVariable String profileID, @PathVariable String profileName){
        try{
            File file = getFile(profileID, profileName);
        } catch (FileNotFoundException e) {
            model.addAttribute(MODELATTR_ERRORMSG, "Der Pressespiegel wurde noch nicht erstellt.");
            return REDIRECT_SETTINGS;
        }
        return "redirect:/download/" + profileID + "/" + profileName;
    }

    @RequestMapping(value = "/{profileID}/{profileName}", method = RequestMethod.GET, produces = APPLICATION_PDF)
    public @ResponseBody Resource downloadClipping(HttpServletResponse response, @PathVariable String profileID, @PathVariable String profileName) throws FileNotFoundException {
        File file = null;
        try {
            file = getFile(profileID, profileName);
        }catch(FileNotFoundException fe){
        }
        response.setContentType(APPLICATION_PDF);
        response.setHeader("Content-Disposition", "inline; filename=" + file.getName());
        response.setHeader("Content-Length", String.valueOf(file.length()));
        return new FileSystemResource(file);
    }

    private File getFile(String profileID, String profileName) throws FileNotFoundException {
        File file = new File(FILE_PATH + profileID + "/" + profileName + ".pdf");
        if (!file.exists()){
            throw new FileNotFoundException("file with path: " + FILE_PATH + " was not found.");
        }
        return file;
    }
}
