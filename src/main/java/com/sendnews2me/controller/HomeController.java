package com.sendnews2me.controller;

import com.sendnews2me.service.Constants;
import com.sendnews2me.service.DatabaseForView;
import com.sendnews2me.service.DatabaseRepresentation.Article;
import com.sendnews2me.service.DatabaseRepresentation.Company;
import com.sendnews2me.service.DatabaseRepresentation.User;
import com.sendnews2me.service.WebCrawler;
import de.l3s.boilerpipe.BoilerpipeExtractor;
import de.l3s.boilerpipe.BoilerpipeProcessingException;
import de.l3s.boilerpipe.document.TextDocument;
import de.l3s.boilerpipe.extractors.ArticleExtractor;
import de.l3s.boilerpipe.extractors.CommonExtractors;
import de.l3s.boilerpipe.extractors.DefaultExtractor;
import de.l3s.boilerpipe.sax.*;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.commons.io.FileUtils;

@Controller
@RequestMapping("/")
public class HomeController implements Constants{

    @RequestMapping(value = {"/", "/index", "/home"}, method = RequestMethod.GET)
    public String index(ModelMap model, HttpSession session) {

        /*
        Anzeigen der Indexseite
         */

        if (((User) session.getAttribute("user")) != null){
            if (!((User) session.getAttribute("user")).isValidated()){
                return REDIRECT_VALIDATE;
            }
        }

        //Hole vorhandene Firmen aus Datenbank für Dropdown-Liste in Sign-Up
        DatabaseForView database = new DatabaseForView();
        List<Company> companies = new ArrayList<Company>();
        try {
            companies = database.getCompanies();
        } catch (SQLException e) {
            model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_SQL_EXCEPTION);
        }

        model.addAttribute("companies", companies);

        return INDEX;
    }

    @RequestMapping(value = "/validate", method = RequestMethod.GET)
    public String showValidate(ModelMap model, HttpSession session) {

        /**
         * Anzeigen der Validierungsseite
         */

        if (((User) session.getAttribute("user")) == null){
            return REDIRECT_INDEX;
        }

        return VALIDATE;
    }

    @RequestMapping("/image/{imageId}")
    @ResponseBody
    public HttpEntity<byte[]> getPhoto(@PathVariable String imageId) {
        byte[] image = new byte[0];
        try {
            image = FileUtils.readFileToByteArray(new File(PATH_TO_IMAGE_FILES + imageId + ".png"));
        } catch (IOException e) {
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_PNG);
        headers.setContentLength(image.length);
        return new HttpEntity<byte[]>(image, headers);
    }

    @RequestMapping(value="loadMore", method = RequestMethod.POST)
    public void loadMore(HttpServletRequest req, HttpServletResponse res){
        try {
            PrintWriter out = res.getWriter();
            String ajaxdata = req.getParameter("data");
            String profileID = ajaxdata.replaceAll("data=", "").split(";")[0];
            String maxLoadedPubDateString = ajaxdata.replaceAll("data=", "").split(";")[1];
            Timestamp maxLoadedTs = Timestamp.valueOf(maxLoadedPubDateString);

            DatabaseForView database = new DatabaseForView();
            List<Article> articles = null;
            try {
                articles = database.getNext20ArticlesAfterArticleID(Integer.parseInt(profileID), maxLoadedTs);
            } catch (SQLException e) {
                out.print("<p style=\"color:red;\">Sql Exception while loading.</p>");
                e.printStackTrace();
                return;
            }

            Timestamp maxLoadedPubDate;
            if(articles.size() < 1){
                maxLoadedPubDate = maxLoadedTs;
            }else {
                maxLoadedPubDate = articles.get(0).getPubDate();
                for (Article article : articles) {
                    if (article.getPubDate().before(maxLoadedPubDate)) {
                        maxLoadedPubDate = article.getPubDate();
                    }
                }
            }

            out.print("<input id=\"maxLoadedPubDateTemp\" type=\"hidden\" value=\""+maxLoadedPubDate.toString()+"\">");
            for(Article article: articles) {
                out.print("<div class=\"jumbotron jumbotron-fluid\" id=\"article" + article.getArticleID() +"\">\n" +
                        "                        <div class=\"container\">\n" +
                        "                            <h1 class=\"articleheading\" style=\"margin-left: 0; padding-left: 0; display:inline;\">"+article.getTitle()+"</h1><br>\n" +
                        "                            <p style=\"display: inline;\">"+article.getSourceName()+" - </p><p style=\"color:grey; display:inline;\">"+article.getPubDateString()+"</p>\n" +
                        "                            <br>\n" +
                        "                            <br>\n" +
                        "                            <img onerror=\"this.style.display = 'none';\" style=\"display:inline;\" src=\"/image/"+article.getArticleID()+"\"><p style=\"display: inline;\"> "+article.getDescription()+"</p>\n" +
                        "                            <br>\n" +
                        "                            <br>\n" +
                        "                            <a class=\"button\" onclick=\"document.getElementById('modal"+article.getArticleID()+profileID+"').style.display = 'block';\" style=\"margin-right: 5px;\" href=\"javascript:void(0);\">Weiterlesen</a>\n" +
                        "                            <p style=\"display: inline;\">|</p>\n" +
                        "                            <a class=\"button\" style=\"margin-left: 5px;\" target=\"_blank\" href=\""+article.getLink()+"\">Originalartikel</a>\n" +
                        "\n" +
                        "                            <div id=\"modal"+article.getArticleID()+profileID+"\" class=\"modal\">\n" +
                        "                                <!-- Modal content -->\n" +
                        "                                <div class=\"modal-content\">\n" +
                        "                                    <span style=\"display:inline;\" class=\"close\" onclick=\"document.getElementById('modal"+article.getArticleID()+profileID+"').style.display = 'none';\">&times;</span>\n" +
                        "                                    <h4 style=\"display: inline; max-width: 90%;\">"+article.getTitle()+"</h4>\n" +
                        "                                    <p>"+article.getFullText()+"</p>\n" +
                        "                                </div>\n" +
                        "\n" +
                        "                            </div>\n" +
                        "                        </div>\n" +
                        "                    </div>");
            }
            if(articles.size() < 20){
                out.print("<input type=\"hidden\" id=\"endReached"+profileID+">");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/updateCompany", method = RequestMethod.POST)
    public String updateCompany(ModelMap model, HttpSession session, @RequestParam(value = "FirmenName", required = true) String companyName, @RequestParam(value = "companyUpdateHow") String updateFunction){

        //Delete/add company

        //Überprüfe ob User Management angezeigt werden darf
        if (((User) session.getAttribute("user")) == null) {
            //Benutzer nicht eingeloggt
            return REDIRECT_INDEX;
        } else {
            //Benutzer eingeloggt
            if (!((User) session.getAttribute("user")).isValidated()) {
                //Benutzer noch nicht validiert
                return REDIRECT_VALIDATE;
            } else if (((User) session.getAttribute("user")).getPermits() < FULL_ADMIN) {
                //Benutzer validiert, aber besitzt keine Full-Admin Rechte
                return REDIRECT_INDEX;
            }
        }

        DatabaseForView database = new DatabaseForView();
        if(updateFunction.equals("0")){
            //try to delete company
            List<Company> companies;
            try {
                companies = database.getCompanies();
            } catch (SQLException e) {
                model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_SQL_EXCEPTION);
                return REDIRECT_MAIN_USER_MANAGEMENT;
            }
            boolean found = false;
            int companyID = -1;
            for(Company company: companies){
                if(company.getCompanyName().equals(companyName)){
                    found = true;
                    companyID = company.getCompanyID();
                    break;
                }
            }
            if(!found || companyID == -1){
                model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_COMPANY_NOT_EXISTING);
                return REDIRECT_MAIN_USER_MANAGEMENT;
            }else{
                List<User> users;
                try {
                    users = database.getUsersFromCompany(companyID);
                } catch (SQLException e) {
                    model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_SQL_EXCEPTION);
                    return REDIRECT_MAIN_USER_MANAGEMENT;
                }
                if(users == null){
                    //keine Benutzer in Firma vorhanden -> löschen
                    boolean deleted = false;
                    try {
                        deleted = database.removeCompany(companyID);
                    } catch (SQLException e) {
                        model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_SQL_EXCEPTION);
                        return REDIRECT_MAIN_USER_MANAGEMENT;
                    }
                    if(deleted){
                        model.addAttribute(MODELATTR_SUCCESSMSG, "Firma " + companyName + SUCCESSMSG_COMPANY_REMOVED);
                        return REDIRECT_MAIN_USER_MANAGEMENT;
                    }else{
                        model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_COMPANY_NOT_DELETED);
                        return REDIRECT_MAIN_USER_MANAGEMENT;
                    }
                }else{
                    //Benutzer vorhanden -> darf nicht gelöscht werden
                    model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_COMPANY_NOT_DELETABLE);
                }
            }
        }else if(updateFunction.equals("1")){
            //add company
            List<Company> companies;
            try {
                companies = database.getCompanies();
            } catch (SQLException e) {
                model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_SQL_EXCEPTION);
                return REDIRECT_MAIN_USER_MANAGEMENT;
            }
            for(Company company: companies){
                if(company.getCompanyName().toLowerCase().equals(companyName.toLowerCase())){
                    //company already existing
                    model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_COMPANY_ALREADY_EXISTING);
                    return REDIRECT_MAIN_USER_MANAGEMENT;
                }
            }

            if(database.addCompany(companyName)){
                model.addAttribute(MODELATTR_SUCCESSMSG, "Firma " + companyName + SUCCESSMSG_COMPANY_ADDED);
            }else{
                model.addAttribute(MODELATTR_ERRORMSG, ERRORMSG_COMPANY_NOT_ADDED);
            }
        }else{
            //wrong updatefunction
            model.addAttribute(MODELATTR_ERRORMSG, "Da ist etwas schiefgegangen.");
        }

        return REDIRECT_MAIN_USER_MANAGEMENT;
    }
}