package com.sendnews2me.service;


import java.io.*;
import java.nio.file.Paths;

import static java.nio.file.Files.readAllLines;

public class FilesystemForView {
    public String getTextFromFile(String path) throws IOException {
        BufferedReader in = null;
        String returnString = "";
        try {
            in = new BufferedReader(new FileReader(path));
            String line;
            while((line = in.readLine()) != null)
            {
                returnString += line;
            }
        } catch (FileNotFoundException e) {
            throw e;
        } catch (IOException e) {
            throw e;
        } finally{
            try {
                in.close();
            }catch(Exception e){

            }
        }
        return returnString;
    }
}
