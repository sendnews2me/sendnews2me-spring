package com.sendnews2me.service;

import de.l3s.boilerpipe.BoilerpipeExtractor;
import de.l3s.boilerpipe.BoilerpipeProcessingException;
import de.l3s.boilerpipe.document.Image;
import de.l3s.boilerpipe.document.TextDocument;
import de.l3s.boilerpipe.extractors.CommonExtractors;
import de.l3s.boilerpipe.sax.*;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.List;

public class WebCrawler implements Constants {
    private String text = "";
    private String title = "";
    private String[] imgUrls;


    private String baseUrl = "";

    public String crawlAndSave(String url_string, int articleID) throws IOException, SAXException, BoilerpipeProcessingException {

        //GENERATE BASE URL
        baseUrl = url_string.substring(0, url_string.indexOf("/", url_string.indexOf("/", url_string.indexOf("/")+1)+1));

        //GET MAIN TEXT AND TITLE FROM SITE
        URL url = new URL(url_string);

        final HTMLDocument htmlDoc = HTMLFetcher.fetch(url);

        final BoilerpipeExtractor extractor = CommonExtractors.DEFAULT_EXTRACTOR;

        //final HTMLHighlighter hh = HTMLHighlighter.newExtractingInstance();
        //hh.setOutputHighlightOnly(true);

        TextDocument doc = new BoilerpipeSAXInput(htmlDoc.toInputSource()).getTextDocument();
        extractor.process(doc);
        final InputSource is = htmlDoc.toInputSource();
        text = doc.getText(true, false);//hh.process(doc, is);
        title = doc.getTitle();

        //GET IMAGE URLs FROM SITE
        final BoilerpipeExtractor articleExtractor = CommonExtractors.ARTICLE_EXTRACTOR;
        final ImageExtractor ie = ImageExtractor.INSTANCE;

        List<Image> images = ie.process(url, articleExtractor);

        Collections.sort(images);
        imgUrls = new String[images.size()];
        int count = 0;
        while(count < images.size()) {
            imgUrls[count] = images.get(count).getSrc();
            count++;
        }

        //SAVE FULL ARTICLE IN .txt FILE
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(PATH_TO_FULL_ARTICLES + articleID + ".txt"));
            writer.write(text);
        }catch(IOException e){

        }finally {
            try {
                if (writer != null) {
                    writer.close();
                }
            }catch(Exception e){

            }
        }

        return text;
    }

    public String getText() {
        return text;
    }

    public String getTitle() {
        return title;
    }

    public String[] getImgUrls() {
        return imgUrls;
    }

    public String getBaseUrl() {
        return baseUrl;
    }
}
