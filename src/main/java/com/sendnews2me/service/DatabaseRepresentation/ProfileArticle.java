package com.sendnews2me.service.DatabaseRepresentation;

public class ProfileArticle {

    private int profileID;
    private int articleID;

    public ProfileArticle(int profileID, int articleID) {
        this.profileID = profileID;
        this.articleID = articleID;
    }

    public ProfileArticle() {
    }

    public int getProfileID() {
        return profileID;
    }

    public void setProfileID(int profileID) {
        this.profileID = profileID;
    }

    public int getArticleID() {
        return articleID;
    }

    public void setArticleID(int articleID) {
        this.articleID = articleID;
    }
}
