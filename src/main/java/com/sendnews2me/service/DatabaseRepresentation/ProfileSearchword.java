package com.sendnews2me.service.DatabaseRepresentation;

public class ProfileSearchword {
    private int profileID;
    private String searchword;

    public ProfileSearchword(int profileID, String searchword) {
        this.profileID = profileID;
        this.searchword = searchword;
    }

    public ProfileSearchword() {
    }

    public int getProfileID() {
        return profileID;
    }

    public void setProfileID(int profileID) {
        this.profileID = profileID;
    }

    public String getSearchword() {
        return searchword;
    }

    public void setSearchword(String searchword) {
        this.searchword = searchword;
    }
}
