package com.sendnews2me.service.DatabaseRepresentation;

public class Source {
    private int sourceID;
    private String sourceName;
    private String linkSite;
    private String linkRSS;

    public Source() {
    }

    public Source(int sourceID, String sourceName, String linkSite, String linkRSS) {
        this.sourceID = sourceID;
        this.sourceName = sourceName;
        this.linkSite = linkSite;
        this.linkRSS = linkRSS;
    }

    public int getSourceID() {
        return sourceID;
    }

    public void setSourceID(int sourceID) {
        this.sourceID = sourceID;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public String getLinkSite() {
        return linkSite;
    }

    public void setLinkSite(String linkSite) {
        this.linkSite = linkSite;
    }

    public String getLinkRSS() {
        return linkRSS;
    }

    public void setLinkRSS(String linkRSS) {
        this.linkRSS = linkRSS;
    }
}
