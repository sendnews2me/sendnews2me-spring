package com.sendnews2me.service.DatabaseRepresentation;


import java.sql.Timestamp;

public class Article {
    private int articleID;
    private String title;
    private String description;
    private String fullTextTitle;
    private String fullText;
    private java.sql.Timestamp pubDate;
    private String pubDateWorkaround;
    private String pubDateString;
    private String link;
    private int sourceID;
    private String sourceName;

    public Article() {
    }

    public Article(int articleID, String title, String description, String fullTextTitle, String fullText, Timestamp pubDate, String link, int sourceID) {
        this.articleID = articleID;
        this.title = title;
        this.description = description;
        this.fullTextTitle = fullTextTitle;
        this.fullText = fullText;
        this.pubDate = pubDate;
        this.link = link;
        this.sourceID = sourceID;
    }

    public int getArticleID() {
        return articleID;
    }

    public void setArticleID(int articleID) {
        this.articleID = articleID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Timestamp getPubDate() {
        return pubDate;
    }

    public void setPubDate(Timestamp pubDate) {
        this.pubDate = pubDate;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public int getSourceID() {
        return sourceID;
    }

    public void setSourceID(int sourceID) {
        this.sourceID = sourceID;
    }

    public String getFullText() {
        return fullText;
    }

    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    public String getFullTextTitle() {
        return fullTextTitle;
    }

    public void setFullTextTitle(String fullTextTitle) {
        this.fullTextTitle = fullTextTitle;
    }

    public String getPubDateWorkaround() {
        return pubDateWorkaround;
    }

    public void setPubDateWorkaround(String pubDateWorkaround) {
        this.pubDateWorkaround = pubDateWorkaround;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public String getPubDateString() {
        return pubDateString;
    }

    public void setPubDateString(String pubDateString) {
        this.pubDateString = pubDateString;
    }
}
