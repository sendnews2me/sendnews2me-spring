package com.sendnews2me.service.DatabaseRepresentation;

import java.util.List;

public class Template {
    private int templateID;
    private String name;
    private List<Source> sources;
    private List<String> searchTerms;

    public Template(int templateID, String name, List<Source> sources, List<String> searchTerms) {
        this.templateID = templateID;
        this.name = name;
        this.sources = sources;
        this.searchTerms = searchTerms;
    }

    public Template() {
    }

    public int getTemplateID() {
        return templateID;
    }

    public void setTemplateID(int templateID) {
        this.templateID = templateID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Source> getSources() {
        return sources;
    }

    public void setSources(List<Source> sources) {
        this.sources = sources;
    }

    public List<String> getSearchTerms() {
        return searchTerms;
    }

    public void setSearchTerms(List<String> searchTerms) {
        this.searchTerms = searchTerms;
    }
}
