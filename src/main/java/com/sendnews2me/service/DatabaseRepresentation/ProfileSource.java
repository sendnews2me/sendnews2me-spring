package com.sendnews2me.service.DatabaseRepresentation;

public class ProfileSource {
    private int profileID;
    private int sourceID;

    public ProfileSource() {
    }

    public ProfileSource(int profileID, int sourceID) {
        this.profileID = profileID;
        this.sourceID = sourceID;
    }

    public int getProfileID() {
        return profileID;
    }

    public void setProfileID(int profileID) {
        this.profileID = profileID;
    }

    public int getSourceID() {
        return sourceID;
    }

    public void setSourceID(int sourceID) {
        this.sourceID = sourceID;
    }
}
