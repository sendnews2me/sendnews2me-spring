package com.sendnews2me.service.DatabaseRepresentation;

import java.sql.Time;
import java.util.List;

public class Profile {
    private int profilID;
    private String profilName;
    private String shortProfilName;
    private List<Source> sources;
    private List<String> searchTerms;
    private List<Article> articles;
    private Time sendingTime;
    private boolean isActive = false;
    private String maxLoadedPubDate;

    public Profile(int profilID, String profilName, String shortProfilName, List<Source> sources, List<String> searchTerms, List<Article> articles, Time sendingTime, boolean isActive) {
        this.profilID = profilID;
        this.profilName = profilName;
        this.shortProfilName = shortProfilName;
        this.sources = sources;
        this.searchTerms = searchTerms;
        this.articles = articles;
        this.sendingTime = sendingTime;
        this.isActive = isActive;
    }

    public Profile() {
    }

    public int getProfilID() {
        return profilID;
    }

    public void setProfilID(int profilID) {
        this.profilID = profilID;
    }

    public String getProfilName() {
        return profilName;
    }

    public void setProfilName(String profilName) {
        this.profilName = profilName;
    }

    public String getShortProfilName() {
        return shortProfilName;
    }

    public void setShortProfilName(String shortProfilName) {
        this.shortProfilName = shortProfilName;
    }

    public List<Source> getSources() {
        return sources;
    }

    public void setSources(List<Source> sources) {
        this.sources = sources;
    }

    public List<String> getSearchTerms() {
        return searchTerms;
    }

    public void setSearchTerms(List<String> searchTerms) {
        this.searchTerms = searchTerms;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }

    public Time getSendingTime() {
        return sendingTime;
    }

    public void setSendingTime(Time sendingTime) {
        this.sendingTime = sendingTime;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public String getMaxLoadedPubDate() {
        return maxLoadedPubDate;
    }

    public void setMaxLoadedPubDate(String maxLoadedPubDate) {
        this.maxLoadedPubDate = maxLoadedPubDate;
    }
}
