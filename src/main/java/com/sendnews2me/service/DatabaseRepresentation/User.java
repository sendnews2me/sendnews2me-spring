package com.sendnews2me.service.DatabaseRepresentation;

public class User {
    private int userID;
    private String userMail;
    private int companyID;
    private String companyName;
    private boolean validated;
    private int permits;

    public User(int id, String mail, int companyID, boolean validated, int permits){
        userID = id;
        userMail = mail;
        this.validated = validated;
        this.companyID = companyID;
        this.permits = permits;
    }

    public User(){}

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getUserMail() {
        return userMail;
    }

    public void setUserMail(String userMail) {
        this.userMail = userMail;
    }

    public int getCompanyID() {
        return companyID;
    }

    public void setCompanyID(int companyID) {
        this.companyID = companyID;
    }

    public boolean isValidated() {
        return validated;
    }

    public void setValidated(boolean validated) {
        this.validated = validated;
    }

    public int getPermits() {
        return permits;
    }

    public void setPermits(int permits) {
        this.permits = permits;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
}
