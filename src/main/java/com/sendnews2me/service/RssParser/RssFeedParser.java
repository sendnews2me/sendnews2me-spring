package com.sendnews2me.service.RssParser;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.XMLEvent;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class RssFeedParser {
    private static final String TITLE = "title";
    private static final String DESCRIPTION = "description";
    private static final String CHANNEL = "channel";
    private static final String LANGUAGE = "language";
    private static final String COPYRIGHT = "copyright";
    private static final String LINK = "link";
    private static final String AUTHOR = "author";
    private static final String ITEM = "item";
    private static final String PUB_DATE = "pubDate";
    private static final String GUID = "guid";

    private final URL url;
    private final Instant timeBefore;

    public RssFeedParser(String feedUrl, Instant timeBefore) {
        try {
            this.url = new URL(feedUrl);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
        this.timeBefore = timeBefore;
    }

    public Feed readFeed() {
        Feed feed = null;
        try {
            boolean isFeedHeader = true;
            // Set header values intial to the empty string
            String description = "";
            String title = "";
            String link = "";
            String language = "";
            String copyright = "";
            String author = "";
            Date pubdate = null;
            String guid = "";

            DateFormat format = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z", Locale.ENGLISH);

            // First create a new XMLInputFactory
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            inputFactory.setProperty("javax.xml.stream.isCoalescing", true);
            // Setup a new eventReader
            InputStream in = read();
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);
            // read the XML document
            while (eventReader.hasNext()) {
                XMLEvent event = eventReader.nextEvent();
                if (event.isStartElement()) {
                    String localPart = event.asStartElement().getName()
                            .getLocalPart();
                    if (ITEM.equals(localPart)) {
                        if (isFeedHeader) {
                            isFeedHeader = false;
                            feed = new Feed(title, link, description, language,
                                    copyright, pubdate);
                        }
                        event = eventReader.nextEvent();

                    } else if (TITLE.equals(localPart)) {
                        title = getCharacterData(event, eventReader);

                    } else if (DESCRIPTION.equals(localPart)) {
                        description = getCharacterData(event, eventReader);
                        description = description.replaceAll("\n", " ");

                    } else if (LINK.equals(localPart)) {
                        link = getCharacterData(event, eventReader);

                    } else if (GUID.equals(localPart)) {
                        guid = getCharacterData(event, eventReader);

                    } else if (LANGUAGE.equals(localPart)) {
                        language = getCharacterData(event, eventReader);

                    } else if (AUTHOR.equals(localPart)) {
                        author = getCharacterData(event, eventReader);

                    } else if (PUB_DATE.equals(localPart)) {
                        pubdate = format.parse(getCharacterData(event, eventReader));

                    } else if (COPYRIGHT.equals(localPart)) {
                        copyright = getCharacterData(event, eventReader);

                    }
                } else if (event.isEndElement()) {
                    if (event.asEndElement().getName().getLocalPart().equals(ITEM)) {
                        FeedMessage message = new FeedMessage();
                        message.setAuthor(author);
                        message.setDescription(description);
                        message.setGuid(guid);
                        message.setLink(link);
                        message.setTitle(title);
                        message.setPubDate(pubdate);

                        /*Calendar cal = Calendar.getInstance();
                        cal.setTime(new Date());
                        cal.set(Calendar.HOUR, cal.get(Calendar.HOUR) - hoursBefore);
                        java.util.Date timeBefore = cal.getTime();*/

                        Instant timePubDate = pubdate.toInstant();
                        Instant now = Instant.now();

                        System.out.println("PubDate: " + Date.from(timePubDate).toString() + "  - MaxTimeBefore: " + Date.from(timeBefore).toString());

                        if(timeBefore.isBefore(timePubDate) && !timePubDate.isAfter(now) && !message.getTitle().contains("*** BILDplus Inhalt ***")) {
                            System.out.println("true\r\n");
                            //Instant timeWorkaround = message.getPubDate().toInstant();
                            //timeWorkaround = timeWorkaround.plus(Duration.ofHours(2));
                            //message.setPubDate(Date.from(timeWorkaround));
                            feed.getMessages().add(message);
                        }

                        event = eventReader.nextEvent();
                    }
                }
            }
        } catch (XMLStreamException e) {
            throw new RuntimeException(e);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
        return feed;
    }

    private String getCharacterData(XMLEvent event, XMLEventReader eventReader)
            throws XMLStreamException {
        String result = "";
        event = eventReader.nextEvent();
        if (event instanceof Characters) {
            result = event.asCharacters().getData();
        }
        return result;
    }

    private InputStream read() {
        try {
            return url.openStream();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
