package com.sendnews2me.service;

import com.sendnews2me.service.Constants;

import com.sun.xml.internal.bind.v2.runtime.reflect.opt.Const;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.FileAppender;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Log the user activities
 */
public class UserActionLogger {

    private static Logger logger;
    private static FileAppender<ILoggingEvent> appender;

    static {

        logger = (Logger) LoggerFactory.getLogger("allCompanyLogger");

        appender  = new FileAppender<ILoggingEvent>();
        LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
        PatternLayoutEncoder ple = new PatternLayoutEncoder();

        ple.setPattern("%msg%n");
        ple.setContext(context);
        ple.start();

        // konfiguriere den File-Appender
        appender.setFile(Constants.PATH_TO_LOGGING_FILES);
        appender.setEncoder(ple);
        appender.setContext(context);

        appender.start();

        logger.addAppender(appender);
    }

    public static void logUserAction(int companyId, String userMail, String action, String message) {
        logger.info(createMessage(companyId, userMail, action, message));
    }

    private static String createMessage(int companyId, String userMail, String action, String message) {
        StringBuilder msg = new StringBuilder();

        msg.append(companyId);
        msg.append(';');
        msg.append(new SimpleDateFormat("dd.MM.yy kk:mm:ss").format(new Date()).toString());
        msg.append(';');
        msg.append(userMail + ';');
        msg.append(message);

        return msg.toString();
    }
}