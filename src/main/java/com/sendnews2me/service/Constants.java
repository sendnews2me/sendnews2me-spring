package com.sendnews2me.service;

public interface Constants {
    //PATHS     TODO: for final build, change path to ubuntu system path
    public static final String PATH_TO_FULL_ARTICLES = /*"C:\\SendNews2Me Test\\fullArticles\\"*/ "/opt/fullArticles/";
    public static final String PATH_TO_LOGGING_FILES = /*"C:\\SendNews2Me Test\\logging\\"*/ "/opt/logging/allCompanyLoggingFile.log";
    public static final String PATH_TO_IMAGE_FILES = /*"C:\\SendNews2Me Test\\images\\"*/ "/opt/fullArticles/images/";
    public static final String FILE_PATH = /*"C:\\Users\\fpreuschoff\\IdeaProjects\\sendnews2me-spring\\target\\"*/ "/opt/tomcat/webapps/ROOT/WEB-INF/classes/";

    //PERMITS
    public static final int USER = 1;
    public static final int COMPANY_ADMIN = 2;
    public static final int FULL_ADMIN = 3;

    //OTHER
    public static final int INTERVAL_FOR_UPDATING_ARTICLES = 2; //in Stunden; *** WICHTIG ***: in Bash-Script muss die
    //Intervallzeit separat angepasst werden
    public static final String MODELATTR_ERRORMSG = "errorMsg";
    public static final String MODELATTR_SUCCESSMSG = "successMsg";
    public static final String MODELATTR_PROFILES = "profiles";
    public static final String MODELATTR_SOURCES = "sources";
    public static final String MODELATTR_TEMPLATES = "templates";
    public static final String MODELATTR_COMPANYNAME = "companyname";
    public static final String MODELATTR_ALL_USERS = "allUsers";


    //MESSAGES
    public static final String ERRORMSG_SQL_EXCEPTION = "Es gibt Probleme auf die Daten zuzugreifen. Bitte kontaktieren Sie einen Administrator.";
    public static final String ERRORMSG_USERMAIL_OR_PASSWORD = "E-Mail oder Passwort ist falsch. Bitte &uuml;berpr&uuml;fen Sie Ihre Eingaben.";
    public static final String ERRORMSG_EMPTY_USER_PW_FIELD = "Benutzername und Passwort bitte ausfuellen.";
    public static final String ERRORMSG_EMPTY_USER_FIELD = "Benutzername bitte ausfuellen.";

    public static final String ERRORMSG_EMPTY_USER_PW_COMPANY_FIELD = "Firma, Benutzername und Passwort bitte ausf&uuml;llen.";
    public static final String ERRORMSG_USER_ALREADY_EXISTING = "Benutzer/E-Mail existiert bereits.";
    public static final String ERRORMSG_USERMAIL_NOT_EXISTING = "Benutzer/E-Mail ist uns nicht bekannt.";
    public static final String ERRORMSG_DIFFERENT_PASSWORDS = "Passwoerter stimmen nicht &uuml;berein.";
    public static final String ERRORMSG_PROFILE_ALREADY_EXISTING = "Profil ist bereits vorhanden.";
    public static final String ERRORMSG_PROFILE_NOT_EXISTING = "Profil existiert nicht";
    public static final String ERRORMSG_NO_PROFILES_EXISTING = "Keine Profile vorhanden";
    public static final String ERRORMSG_NO_KEYWORDS = "Keine Suchbegriffe eingegeben";
    public static final String ERRORMSG_NO_PROFILENAME = "Kein Profilname eingegeben";
    public static final String ERRORMSG_NO_SOURCES = "Keine Quelle ausgewaehlt";
    public static final String ERRORMSG_NO_TEXT_ENTERED = "Keinen Text eingegeben.";
    public static final String ERRORMSG_COMPANY_NOT_ADDED = "Firma konnte nicht hinzugefuegt werden.";
    public static final String ERRORMSG_COMPANY_NOT_EXISTING = "Firma existiert nicht. Achten Sie auf Groß-/Kleinschreibung.";
    public static final String ERRORMSG_COMPANY_NOT_DELETABLE = "Es sind bereits Benutzer mit dieser Firma registriert! Loeschen Sie bitte zuerst alle Benutzer dieser Firma.";
    public static final String ERRORMSG_COMPANY_NOT_DELETED = "Firma konnte nicht geloescht werden.";
    public static final String ERRORMSG_USER_NOT_DELETED = " konnte nicht geloescht werden.";
    public static final String ERRORMSG_COMPANY_ALREADY_EXISTING = "Firma existiert bereits.";


    public static final String ERRORMSG_SOURCE_ALREADY_EXISTING = "Quelle ist bereits vorhanden.";
    public static final String ERRORMSG_TEMPLATE_ALREADY_EXISTING = "Template ist bereits vorhanden.";

    public static final String ERRORMSG_NO_PW_ENTERED = "Es wurde kein Passwort eingegeben";
    public static final String ERRORMSG_PW_CHANGE_FAILED = "Passwort konnte nicht geaendert werden.";
    public static final String ERRORMSG_LOGGER_FILE = "Logging-Datei konnte nicht geoeffnet werden.";


    public static final String SUCCESSMSG_ACCOUNT_CREATED = "Account erfolgreich erstellt.";
    public static final String SUCCESSMSG_LOGGED_IN = ", erfolgreich angemeldet.";
    public static final String SUCCESSMSG_LOGGED_OUT = "Abgemeldet";
    public static final String SUCCESSMSG_PW_SEND = "Passwort zurueckgesetzt";
    public static final String SUCCESSMSG_SOURCE_CREATED = "Quelle hinzugefuegt";
    public static final String SUCCESSMSG_TEMPLATE_CREATED = "Template hinzugefuegt";
    public static final String SUCCESSMSG_TEMPLATE_DELETED = "Template geloescht";
    public static final String SUCCESSMSG_PROFILE_CREATED = "Profil hinzugefuegt";
    public static final String SUCCESSMSG_PROFILE_CHANGED = "Profil aktualisiert";
    public static final String SUCCESSMSG_PROFILENAME_UPDATED = "Profilname geaendert";
    public static final String SUCCESSMSG_PROFILESTATUS_DISABLED = "E-Mail Zusendung deaktiviert fuer Profil ";
    public static final String SUCCESSMSG_PROFILESTATUS_ENABLED = "E-Mail Zusendung aktiviert fuer Profil ";
    public static final String SUCCESSMSG_PROFILE_DELETED = " geloescht";
    public static final String SUCCESSMSG_PW_CHANGED = "Das Passwort wurde erfolgreich geaendert.";
    public static final String SUCCESSMSG_COMPANY_PERMITS_GIVEN = " wurden Firmen-Admin Rechte zugeteilt.";
    public static final String SUCCESSMSG_COMPANY_PERMITS_NOT_GIVEN = " wurden Firmen-Admin Rechte entzogen.";
    public static final String SUCCESSMSG_USER_VALIDATED = " wurde validiert. Der Benutzer hat nun Zugriff auf den Dienst.";
    public static final String SUCCESSMSG_USER_INVALIDATED = " wurde invalidiert. Der Benutzer hat nun keinen Zugriff mehr auf den Dienst.";
    public static final String SUCCESSMSG_MAIL_SENT = "Eine E-Mail an alle Systemadministratoren wurde versendet.";
    public static final String SUCCESSMSG_COMPANY_ADDED = " wurde hinzugefuegt.";
    public static final String SUCCESSMSG_COMPANY_REMOVED = " wurde geloescht";
    public static final String SUCCESSMSG_USER_DELETED = " wurde geloescht";
    public static final String SUCCESSMSG_SOURCE_DELETED = "Quelle geloescht";



    public static final String ERRORMSG_NO_VALID_SENDINGTIME = "E-Mail Sendezeit bitte waehlen.";

    //REDIRECTS
    public static final String REDIRECT_INDEX = "redirect:/";
    public static final String REDIRECT_INDEX_OPEN_SIGNIN = "redirect:/index?act=nuf";
    public static final String REDIRECT_INDEX_OPEN_SIGNUP = "redirect:/index?act=reg";
    public static final String REDIRECT_USER_MANAGEMENT = "redirect:/account/manageUsers";
    public static final String REDIRECT_CLIPPING = "redirect:/clipping";
    public static final String REDIRECT_SUGGESTSOURCE = "redirect:/clipping/suggestSource";
    public static final String REDIRECT_TEMPLATE = "redirect:/clipping/template";
    public static final String REDIRECT_VALIDATE = "redirect:/validate";
    public static final String REDIRECT_SETTINGS = "redirect:/account/settings";
    public static final String REDIRECT_MAIN_USER_MANAGEMENT = "redirect:/account/mainManageUsers";

    //VIEWNAMES
    public static final String INDEX = "index";
    public static final String VALIDATE = "validate";
    public static final String CLIPPING = "pressclipping";
    public static final String ACC_SETTINGS = "accountSettings";
    public static final String PROFILE = "profile";
    public static final String USER_MANAGEMENT = "userManagement";
    public static final String MAIN_USER_MANAGEMENT = "mainUserManagement";
    public static final String TEST = "test";
    public static final String TEMPLATE = "createTemplate";
    public static final String SOURCE = "suggestSource";
    public static final String LOGGING = "logging";

    // Bezeichnung der User Actions der Logging Klasse
    public static final String USER_ACTION_ACCOUNT_DELETED = "Accountlöschun";
    public static final String USER_ACTION_ACCOUNT_SETTINGS_CHANGED = "Accountänderung";
    public static final String USER_ACTION_LOGIN = "Login";
    public static final String USER_ACTION_LOGOUT = "Logout";
    public static final String USER_ACTION_PROFILE_CHANGED = "Profiländerung";
    public static final String USER_ACTION_PROFILE_CREATED = "Profilerstellung";
    public static final String USER_ACTION_PROFILE_UPDATED = "Profilupdate";
    public static final String USER_ACTION_PW_CHANGED = "Passwortänderung";
    public static final String USER_ACTION_REGISTRATION = "Registrierung";
    public static final String USER_ACTION_SOURCE_CREATED = "Quellerstellung";
    public static final String USER_ACTION_TEMPLATE_CREATED = "Templateerstellung";
    public static final String USER_ACTION_TEMPLATE_DELETED = "Templatelöschung";
    public static final String USER_ACTION_UPDATED_USER_PERMISSION = "Rechteänderung";
    public static final String USER_ACTION_SUGGEST_SOURCE = "Quellenvorschlag";
    public static final String USER_ACTION_SOURCE_DELETED = "Quellenlöschung";

    // Messages der Logging-Einträge
    public static final String LOGGER_NO_PW_ENTERED = "Kein neues Passwort eingegeben.";
    public static final String LOGGER_PW_CHANGED = "Passwort geaendert.";
    public static final String LOGGER_PW_CHANGE_FAILED = "Passwortaenderung fehlgeschlagen.";

}
