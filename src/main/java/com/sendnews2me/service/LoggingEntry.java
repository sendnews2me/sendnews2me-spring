package com.sendnews2me.service;

public class LoggingEntry
{
    private String date;
    private String userMail;
    private String message;

    public LoggingEntry(String date, String userMail, String message) {
        this.date = date;
        this.userMail = userMail;
        this.message = message;
    }

    public String getDate()
    {
        return date;
    }

    public String getMessage()
    {
        return message;
    }

    public String getUserMail()
    {
        return userMail;
    }
}
