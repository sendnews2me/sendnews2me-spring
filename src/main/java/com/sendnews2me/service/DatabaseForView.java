package com.sendnews2me.service;

import com.sendnews2me.service.DatabaseRepresentation.*;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class DatabaseForView implements IDatabase, Constants{
    private Connection conn = null;
    private ResultSet rs = null;

    public User getUser(String userMail) throws SQLException {

        try {
            //Treiber laden
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Verbindung aufbauen
            conn = DriverManager.getConnection(connectionUrl + database + encoding, user, pass);

            //Sql Statement vorbereiten
            PreparedStatement ps = conn.prepareStatement("SELECT * FROM User WHERE Email=?");
            ps.setString(1, userMail);

            //Sql Query ausführen
            rs = ps.executeQuery();

            //Sql Antwort auswerten
            User user = null;
            if(rs.next()){
                user = new User();
                user.setUserID(rs.getInt("UserID"));
                user.setUserMail(rs.getString("Email"));
                user.setCompanyID(rs.getInt("CompanyID"));
                user.setValidated(rs.getBoolean("Validated"));
                user.setPermits(rs.getInt("Permits"));
            }

            return user;

        } catch (Exception e) {
            throw new SQLException(e.getMessage());
        } finally {
            if (conn != null)
            {
                try { conn.close(); } catch (SQLException ignore) {}
            }
        }
    }

    public String getUserPassword(String userMail) throws SQLException {

        try {
            //Treiber laden
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Verbindung aufbauen
            conn = DriverManager.getConnection(connectionUrl + database + encoding, user, pass);

            //Sql Statement vorbereiten
            PreparedStatement ps = conn.prepareStatement("SELECT Passwort FROM User WHERE Email=?");
            ps.setString(1, userMail);

            //Sql Query ausführen
            rs = ps.executeQuery();

            //Sql Antwort auswerten
            String password = null;
            while(rs.next()){
                password = rs.getString("Passwort");
            }

            return password;

        } catch (Exception e) {
            return null;
        } finally {
            if (conn != null)
            {
                try { conn.close(); } catch (SQLException ignore) {}
            }
        }
    }

    public String getUserAuthentication(String userID) {
        //not implemented
        return null;
    }

    @Override
    public List<Profile> getUserProfiles(int userID) throws Exception {
        try {
            //Treiber laden
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Verbindung aufbauen
            conn = DriverManager.getConnection(connectionUrl + database + encoding, user, pass);

            //Sql Statement vorbereiten
            PreparedStatement ps = conn.prepareStatement("SELECT SourceID,SourceName FROM Sources");

            //Sql Query ausführen
            rs = ps.executeQuery();

            //Sql Antwort auswerten
            List<Source> sourcesForGettingProfiles = new ArrayList<Source>();
            while(rs.next()){
                Source source = new Source();
                source.setSourceID(rs.getInt("SourceID"));
                source.setSourceName(rs.getString("SourceName"));
                sourcesForGettingProfiles.add(source);
            }

            //Sql Statement vorbereiten
            ps = conn.prepareStatement("SELECT * FROM Profile WHERE UserID=?;");
            ps.setInt(1, userID);

            //Sql Query ausführen
            rs = ps.executeQuery();

            //Sql Antwort auswerten
            List<Profile> profiles = null;
            boolean firstLoop = true;
            while(rs.next()){
                if(firstLoop){firstLoop = false; profiles = new ArrayList<Profile>();}
                Profile profile = new Profile();
                profile.setProfilID(rs.getInt("ProfileID"));
                profile.setProfilName(rs.getString("Profilename"));
                if(profile.getProfilName().length() > 9){
                    profile.setShortProfilName(profile.getProfilName().substring(0,8) + "...");
                }else{
                    profile.setShortProfilName(profile.getProfilName());
                }
                profile.setSendingTime(Time.valueOf(rs.getString("Sendingtime")));
                profile.setActive(rs.getBoolean("Enabled"));
                profiles.add(profile);
            }
            if(profiles == null){
                return profiles;
            }

            List<Profile> profilesToAdd = new ArrayList<Profile>();
            for(Profile profile: profiles){
                //Sql Statement vorbereiten
                ps = conn.prepareStatement("SELECT * FROM ProfileSource WHERE ProfileID=?;");
                ps.setInt(1, profile.getProfilID());

                //Sql Query ausführen
                rs = ps.executeQuery();

                //Sql Antwort auswerten
                List<Integer> sourceIDs = null;
                firstLoop = true;
                while(rs.next()){
                    if(firstLoop){firstLoop = false; sourceIDs = new ArrayList<Integer>();}
                    sourceIDs.add(rs.getInt("SourceID"));
                }

                if(sourceIDs != null) {
                    List<Source> sources = new ArrayList<Source>();
                    for (Integer sourceID : sourceIDs) {
                        //Sql Statement vorbereiten
                        ps = conn.prepareStatement("SELECT * FROM Sources WHERE SourceID=?;");
                        ps.setInt(1, sourceID);

                        //Sql Query ausführen
                        rs = ps.executeQuery();

                        //Sql Antwort auswerten
                        if (rs.next()) {
                            Source source = new Source();
                            source.setSourceID(rs.getInt("SourceID"));
                            source.setSourceName(rs.getString("SourceName"));
                            source.setLinkSite(rs.getString("LinkSite"));
                            source.setLinkRSS(rs.getString("LinkRSS"));
                            sources.add(source);
                            System.out.println(profile.getProfilName() +" source added: " + source.getSourceName());
                        }
                    }
                    System.out.println(profile.getProfilName() + " sources set");
                    profile.setSources(sources);
                }else{
                    System.out.println(profile.getProfilName() + " null");
                    profile.setSources(null);
                }

                //Sql Statement vorbereiten
                ps = conn.prepareStatement("SELECT * FROM ProfileSearchWord WHERE ProfileID=?;");
                ps.setInt(1, profile.getProfilID());

                //Sql Query ausführen
                rs = ps.executeQuery();

                //Sql Antwort auswerten
                List<String> searchwords = null;
                firstLoop = true;
                while(rs.next()){
                    if(firstLoop){firstLoop = false; searchwords = new ArrayList<String>();}
                    searchwords.add(rs.getString("SearchWord"));
                }
                if(searchwords!=null) {
                    profile.setSearchTerms(searchwords);
                }else{
                    profile.setSearchTerms(null);
                }

                //Sql Statement vorbereiten
                ps = conn.prepareStatement("SELECT * FROM ProfileArticle WHERE ProfileID=?");
                ps.setInt(1, profile.getProfilID());

                //Sql Query ausführen
                rs = ps.executeQuery();

                //Sql Antwort auswerten
                List<Integer> articleIDs = null;
                firstLoop = true;
                while(rs.next()){
                    if(firstLoop){firstLoop = false; articleIDs = new ArrayList<Integer>();}
                    articleIDs.add(rs.getInt("ArticleID"));
                }
                if (firstLoop){
                   profile.setArticles(null);
                   profilesToAdd.add(profile);
                   continue;
                }

                //get Articles from Database
                String articleIDsqlString = "";
                for(Integer articleID : articleIDs) {
                    articleIDsqlString += "ArticleID=" + articleID + " OR ";
                }
                articleIDsqlString = articleIDsqlString.substring(0, articleIDsqlString.length()-4);

                //Sql Statement vorbereiten
                ps = conn.prepareStatement("SELECT * FROM Article WHERE " + articleIDsqlString + " ORDER BY PubDate DESC LIMIT 20;");

                //Sql Query ausführen
                rs = ps.executeQuery();

                //Sql Antwort auswerten
                List<Article> articles = new ArrayList<Article>();
                Timestamp maxLoadedPubDate = null;
                boolean firstLooop = true;
                while(rs.next()){
                    if(firstLooop){firstLooop=false; maxLoadedPubDate = Timestamp.valueOf(rs.getString("PubDate"));}
                    Article article = new Article();
                    article.setArticleID(rs.getInt("ArticleID"));
                    article.setTitle(rs.getString("Title").replaceAll("\"", "&quot;").replaceAll("'", "&#39;"));
                    article.setDescription(rs.getString("Description"));
                    FilesystemForView fs = new FilesystemForView();
                    try {
                        article.setFullText(fs.getTextFromFile(PATH_TO_FULL_ARTICLES + article.getArticleID() + ".txt").replaceAll("\"", "&quot;").replaceAll("'", "&#39;"));
                    }catch(Exception e){
                        article.setFullText("exception: " + e.getMessage());
                    }
                    article.setFullText(article.getFullText().replaceAll("\n", "<br>"));
                    article.setPubDate(Timestamp.valueOf(rs.getString("PubDate")));
                    article.setPubDateString(new SimpleDateFormat("dd/MM/yyyy HH:mm").format(article.getPubDate()));
                    article.setLink(rs.getString("Link"));
                    article.setSourceID(rs.getInt("SourceID"));

                    if(maxLoadedPubDate != null) {
                        if (article.getPubDate().before(maxLoadedPubDate)) {
                            maxLoadedPubDate = article.getPubDate();
                        }
                    }

                    for(int v=0; v < sourcesForGettingProfiles.size(); v++){
                        if(article.getSourceID() == sourcesForGettingProfiles.get(v).getSourceID()){
                            article.setSourceName(sourcesForGettingProfiles.get(v).getSourceName());
                        }
                    }
                    articles.add(article);
                }
                profile.setArticles(articles);
                if(maxLoadedPubDate != null) {
                    profile.setMaxLoadedPubDate(maxLoadedPubDate.toString());
                }

                profilesToAdd.add(profile);
            }
            profiles.clear();
            for(Profile addProfile: profilesToAdd){
                profiles.add(addProfile);
            }

            return profiles;

        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            if (conn != null)
            {
                try { conn.close(); } catch (SQLException ignore) {}
            }
        }
    }

    @Override
    public Profile getProfileFromUser(String profilename, int userID) {
        try {
            //Treiber laden
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Verbindung aufbauen
            conn = DriverManager.getConnection(connectionUrl + database + encoding, user, pass);

            //Sql Statement vorbereiten
            PreparedStatement ps = conn.prepareStatement("SELECT * FROM Profile WHERE UserID=? AND Profilename=?;");
            ps.setInt(1, userID);
            ps.setString(2, profilename);

            //Sql Query ausführen
            rs = ps.executeQuery();

            //Sql Antwort auswerten
            Profile profile = null;
            if(rs.next()){
                profile = new Profile();
                profile.setProfilID(rs.getInt("ProfileID"));
                profile.setProfilName(rs.getString("Profilename"));
                profile.setSendingTime(Time.valueOf(rs.getString("Sendingtime")));
                profile.setActive(rs.getBoolean("Enabled"));
            }

            return profile;

        } catch (Exception e) {
            return null;
        } finally {
            if (conn != null)
            {
                try { conn.close(); } catch (SQLException ignore) {}
            }
        }
    }

    public List<User> getUsersFromCompany(int companyID) throws SQLException {
        try {
            //Treiber laden
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Verbindung aufbauen
            conn = DriverManager.getConnection(connectionUrl + database + encoding, user, pass);

            //Sql Statement vorbereiten
            PreparedStatement ps = conn.prepareStatement("SELECT * FROM User WHERE CompanyID=? ORDER BY Validated DESC, Permits DESC");
            ps.setInt(1, companyID);

            //Sql Query ausführen
            rs = ps.executeQuery();

            //Sql Antwort auswerten
            List<User> users = new ArrayList<User>();
            while(rs.next()){
                User user = new User();
                user.setUserID(rs.getInt("UserID"));
                user.setUserMail(rs.getString("Email"));
                user.setCompanyID(rs.getInt("CompanyID"));
                user.setValidated(rs.getBoolean("Validated"));
                user.setPermits(rs.getInt("Permits"));
                users.add(user);
            }

            if(users.size() < 1){
                users = null;
            }

            return users;

        } catch (Exception e) {
            throw new SQLException(e.getMessage());
        } finally {
            if (conn != null)
            {
                try { conn.close(); } catch (SQLException ignore) {}
            }
        }
    }

    public List<String> getProfileSearchWords(int profileID) {

        List<String> profileSearchwords = new ArrayList<String>();

        try {
            //Treiber laden
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Verbindung aufbauen
            conn = DriverManager.getConnection(connectionUrl + database + encoding, user, pass);

            //Sql Statement vorbereiten
            PreparedStatement ps = conn.prepareStatement("SELECT SearchWord FROM ProfileSearchWord WHERE ProfileID=?");
            ps.setInt(1, profileID);

            //Sql Query ausführen
            rs = ps.executeQuery();

            // füge searchword der Liste hinzu
            while(rs.next()) {
                profileSearchwords.add(rs.getString("SearchWord"));
            }

        } catch (Exception e) {
            return null;
        } finally {
            if (conn != null)
            {
                try { conn.close(); } catch (SQLException ignore) {}
            }
        }

        return profileSearchwords;
    }

    @Override
    public List<ProfileSearchword> getAllSearchwords() throws SQLException {
        try {
            //Treiber laden
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Verbindung aufbauen
            conn = DriverManager.getConnection(connectionUrl + database + encoding, user, pass);

            //Sql Statement vorbereiten
            PreparedStatement ps = conn.prepareStatement("SELECT * FROM ProfileSearchWord");

            //Sql Query ausführen
            rs = ps.executeQuery();

            //Sql Antwort auswerten
            List<ProfileSearchword> searchwords = new ArrayList<ProfileSearchword>();
            while(rs.next()){
                ProfileSearchword searchword = new ProfileSearchword();
                searchword.setProfileID(rs.getInt("ProfileID"));
                searchword.setSearchword(rs.getString("SearchWord"));
                searchwords.add(searchword);
            }

            return searchwords;

        } catch (Exception e) {
            throw new SQLException(e.getMessage());
        } finally {
            if (conn != null)
            {
                try { conn.close(); } catch (SQLException ignore) {}
            }
        }
    }

    public List<Company> getCompanies() throws SQLException {
        try {
            //Treiber laden
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Verbindung aufbauen
            conn = DriverManager.getConnection(connectionUrl + database + encoding, user, pass);

            //Sql Statement vorbereiten
            PreparedStatement ps = conn.prepareStatement("SELECT * FROM Companies");

            //Sql Query ausführen
            rs = ps.executeQuery();

            //Sql Antwort auswerten
            List<Company> companies = new ArrayList<Company>();
            while(rs.next()){
                Company company = new Company();
                company.setCompanyID(rs.getInt("CompanyID"));
                company.setCompanyName(rs.getString("CompanyName"));
                if(company.getCompanyID() != 0) {
                    companies.add(company);
                }
            }

            return companies;

        } catch (Exception e) {
            return null;
        } finally {
            if (conn != null)
            {
                try { conn.close(); } catch (SQLException ignore) {}
            }
        }
    }


    @Override
    public List<Source> getSources() throws SQLException {
        try {
            //Treiber laden
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Verbindung aufbauen
            conn = DriverManager.getConnection(connectionUrl + database + encoding, user, pass);

            //Sql Statement vorbereiten
            PreparedStatement ps = conn.prepareStatement("SELECT SourceID,SourceName,LinkSite,LinkRSS FROM Sources");

            //Sql Query ausführen
            rs = ps.executeQuery();

            //Sql Antwort auswerten
            List<Source> sources = new ArrayList<Source>();
            while(rs.next()){
                Source source = new Source();
                source.setSourceID(rs.getInt("SourceID"));
                source.setSourceName(rs.getString("SourceName"));
                source.setLinkSite(rs.getString("LinkSite"));
                source.setLinkRSS(rs.getString("LinkRSS"));
                sources.add(source);
            }

            return sources;

        } catch (Exception e) {
            throw new SQLException(e.getMessage());
        } finally {
            if (conn != null)
            {
                try { conn.close(); } catch (SQLException ignore) {}
            }
        }
    }

    @Override
    public List<Article> getArticlesFromLastInterval(int sourceID) throws SQLException {
        /*
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.HOUR, -INTERVAL_FOR_UPDATING_ARTICLES);
        java.util.Date lastInterval = cal.getTime();*/

        Instant compareTime = Instant.now().minus(Duration.ofHours(INTERVAL_FOR_UPDATING_ARTICLES));
        System.out.println(new java.sql.Timestamp(Date.from(compareTime).getTime()).toString());


        try {
            //Treiber laden
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Verbindung aufbauen
            conn = DriverManager.getConnection(connectionUrl + database + encoding, user, pass);

            //Sql Statement vorbereiten
            PreparedStatement ps = conn.prepareStatement("SELECT * FROM Article WHERE PubDate>=? AND SourceID=?");
            ps.setString(1, new java.sql.Timestamp(Date.from(compareTime).getTime()).toString());
            ps.setInt(2, sourceID);

            //Sql Query ausführen
            rs = ps.executeQuery();

            //Sql Antwort auswerten
            List<Article> relevantArticles = new ArrayList<Article>();
            while(rs.next()){
                Article article = new Article();
                article.setArticleID(rs.getInt("ArticleID"));
                article.setTitle(rs.getString("Title"));
                article.setDescription(rs.getString("Description"));
                article.setPubDate(Timestamp.valueOf(rs.getString("PubDate")));
                article.setLink(rs.getString("Link"));
                article.setSourceID(rs.getInt("SourceID"));
                relevantArticles.add(article);
            }

            return relevantArticles;

        } catch (Exception e) {
            throw new SQLException(e.getMessage());
        } finally {
            if (conn != null)
            {
                try { conn.close(); } catch (SQLException ignore) {}
            }
        }
    }

    @Override
    public List<Template> getTemplatesFromCompany(int companyID) throws SQLException {
        try {
            //Treiber ladend
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Verbindung aufbauen
            conn = DriverManager.getConnection(connectionUrl + database + encoding, user, pass);

            //Sql Statement vorbereiten
            PreparedStatement ps = conn.prepareStatement("SELECT * FROM Templates WHERE CompanyID=?;");
            ps.setInt(1, companyID);

            //Sql Query ausführen
            rs = ps.executeQuery();

            //Sql Antwort auswerten
            List<Template> templates = new ArrayList<Template>();
            while(rs.next()){
                Template template = new Template();
                template.setTemplateID(rs.getInt("TemplateID"));
                template.setName(rs.getString("Templatename"));
                templates.add(template);
            }

            for(Template template: templates){
                //Sql Statement vorbereiten
                ps = conn.prepareStatement("SELECT * FROM TemplateSource WHERE TemplateID=?;");
                ps.setInt(1, template.getTemplateID());

                //Sql Query ausführen
                rs = ps.executeQuery();

                List<Integer> sourceIDs = null;
                boolean firstLoop = true;
                while(rs.next()){
                    if(firstLoop){firstLoop = false; sourceIDs = new ArrayList<Integer>();}
                    sourceIDs.add(rs.getInt("SourceID"));
                }

                if(sourceIDs != null) {
                    List<Source> sources = new ArrayList<Source>();
                    for (Integer sourceID : sourceIDs) {
                        //Sql Statement vorbereiten
                        ps = conn.prepareStatement("SELECT * FROM Sources WHERE SourceID=?;");
                        ps.setInt(1, sourceID);

                        //Sql Query ausführen
                        rs = ps.executeQuery();

                        //Sql Antwort auswerten
                        if (rs.next()) {
                            Source source = new Source();
                            source.setSourceID(rs.getInt("SourceID"));
                            source.setSourceName(rs.getString("SourceName"));
                            source.setLinkSite(rs.getString("LinkSite"));
                            source.setLinkRSS(rs.getString("LinkRSS"));
                            sources.add(source);
                        }
                    }
                    template.setSources(sources);
                }else{
                    template.setSources(null);
                }

                //Sql Statement vorbereiten
                ps = conn.prepareStatement("SELECT * FROM TemplateSearchword WHERE TemplateID=?;");
                ps.setInt(1, template.getTemplateID());

                //Sql Query ausführen
                rs = ps.executeQuery();

                //Sql Antwort auswerten
                List<String> searchwords = null;
                firstLoop = true;
                while(rs.next()){
                    if(firstLoop){firstLoop = false; searchwords = new ArrayList<String>();}
                    searchwords.add(rs.getString("Searchword"));
                }
                if(searchwords!=null) {
                    template.setSearchTerms(searchwords);
                }else{
                    template.setSearchTerms(null);
                }
            }

            return templates;

        } catch (Exception e) {
            throw new SQLException(e.getMessage());
        } finally {
            if (conn != null)
            {
                try { conn.close(); } catch (SQLException ignore) {}
            }
        }
    }

    @Override
    public String getProfilenameFromProfileID(int profileID) throws SQLException {
        try {
            //Treiber ladend
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Verbindung aufbauen
            conn = DriverManager.getConnection(connectionUrl + database + encoding, user, pass);

            //Sql Statement vorbereiten
            PreparedStatement ps = conn.prepareStatement("SELECT Profilename FROM Profile WHERE ProfileID=?;");
            ps.setInt(1, profileID);

            //Sql Query ausführen
            rs = ps.executeQuery();

            //Sql Antwort auswerten
            String profilename = null;
            if(rs.next()){
                profilename = rs.getString("Profilename");
            }
            
            return profilename;

        } catch (Exception e) {
            throw new SQLException(e.getMessage());
        } finally {
            if (conn != null)
            {
                try { conn.close(); } catch (SQLException ignore) {}
            }
        }
    }

    @Override
    public List<Article> getNext20ArticlesAfterArticleID(int profileID, Timestamp maxLoadedPubDate) throws SQLException {
        try {
            //Treiber laden
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Verbindung aufbauen
            conn = DriverManager.getConnection(connectionUrl + database + encoding, user, pass);

            //Sql Statement vorbereiten
            PreparedStatement ps = conn.prepareStatement("SELECT SourceID,SourceName FROM Sources");

            //Sql Query ausführen
            rs = ps.executeQuery();

            //Sql Antwort auswerten
            List<Source> sourcesForGettingProfiles = new ArrayList<Source>();
            while(rs.next()){
                Source source = new Source();
                source.setSourceID(rs.getInt("SourceID"));
                source.setSourceName(rs.getString("SourceName"));
                sourcesForGettingProfiles.add(source);
            }

            //Sql Statement vorbereiten
            ps = conn.prepareStatement("SELECT * FROM ProfileArticle WHERE ProfileID=?;");
            ps.setInt(1, profileID);

            //Sql Query ausführen
            rs = ps.executeQuery();

            List<Integer> articleIDs = new ArrayList<Integer>();
            while(rs.next()){
                articleIDs.add(rs.getInt("ArticleID"));
            }

            //get Articles from Database
            String articleIDsqlString = "";
            for(Integer articleID : articleIDs) {
                articleIDsqlString += "ArticleID=" + articleID + " OR ";
            }
            articleIDsqlString = articleIDsqlString.substring(0, articleIDsqlString.length()-4);

            //Sql Statement vorbereiten
            ps = conn.prepareStatement("SELECT * FROM Article WHERE (" + articleIDsqlString + ") AND PubDate<? ORDER BY PubDate DESC LIMIT 20;");
            ps.setString(1, maxLoadedPubDate.toString());

            //Sql Query ausführen
            rs = ps.executeQuery();

            //Sql Antwort auswerten
            List<Article> articles = new ArrayList<Article>();
            while(rs.next()){
                Article article = new Article();
                article.setArticleID(rs.getInt("ArticleID"));
                article.setTitle(rs.getString("Title"));
                article.setDescription(rs.getString("Description"));
                FilesystemForView fs = new FilesystemForView();
                try {
                    article.setFullText(fs.getTextFromFile(PATH_TO_FULL_ARTICLES + article.getArticleID() + ".txt"));
                }catch(Exception e){
                    article.setFullText("exception: " + e.getMessage());
                }
                article.setPubDate(Timestamp.valueOf(rs.getString("PubDate")));
                article.setPubDateString(new SimpleDateFormat("dd/MM/yyyy HH:mm").format(article.getPubDate()));
                article.setLink(rs.getString("Link"));
                article.setSourceID(rs.getInt("SourceID"));

                for(int v=0; v < sourcesForGettingProfiles.size(); v++){
                    if(article.getSourceID() == sourcesForGettingProfiles.get(v).getSourceID()){
                        article.setSourceName(sourcesForGettingProfiles.get(v).getSourceName());
                    }
                }
                articles.add(article);
            }

            return articles;
        } catch (Exception e) {
            throw new SQLException(e.getMessage());
        } finally {
            if (conn != null)
            {
                try { conn.close(); } catch (SQLException ignore) {}
            }
        }    }

    @Override
    public List<String> getSystemAdminMails() throws SQLException {
        try {
            //Treiber laden
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Verbindung aufbauen
            conn = DriverManager.getConnection(connectionUrl + database + encoding, user, pass);

            //Sql Statement vorbereiten
            PreparedStatement ps = conn.prepareStatement("SELECT Email FROM User WHERE Permits>=?;");
            ps.setInt(1,3);

            //Sql Query ausführen
            rs = ps.executeQuery();

            //Sql Antwort auswerten
            List<String> adminMails = new ArrayList<String>();
            while(rs.next()){
                adminMails.add(rs.getString("Email"));
            }

            return adminMails;

        } catch (Exception e) {
            throw new SQLException(e.getMessage());
        } finally {
            if (conn != null)
            {
                try { conn.close(); } catch (SQLException ignore) {}
            }
        }
    }

    @Override
    public List<Profile> getProfilesFromUserWithoutArticles(int userID) throws SQLException {
        try {
            //Treiber laden
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Verbindung aufbauen
            conn = DriverManager.getConnection(connectionUrl + database + encoding, user, pass);

            //Sql Statement vorbereiten
            PreparedStatement ps = conn.prepareStatement("SELECT ProfileID, Profilename FROM Profile WHERE UserID=?;");
            ps.setInt(1,userID);

            //Sql Query ausführen
            rs = ps.executeQuery();

            //Sql Antwort auswerten
            List<Profile> profiles = new ArrayList<Profile>();
            while(rs.next()){
                Profile profile = new Profile();
                profile.setProfilName(rs.getString("Profilename"));
                profile.setProfilID(rs.getInt("ProfileID"));
                profiles.add(profile);
            }

            return profiles;

        } catch (Exception e) {
            throw new SQLException(e.getMessage());
        } finally {
            if (conn != null)
            {
                try { conn.close(); } catch (SQLException ignore) {}
            }
        }
    }

    @Override
    public List<ProfileSource> getAllProfileSources() throws SQLException {
        try {
            //Treiber laden
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Verbindung aufbauen
            conn = DriverManager.getConnection(connectionUrl + database + encoding, user, pass);

            //Sql Statement vorbereiten
            PreparedStatement ps = conn.prepareStatement("SELECT * FROM ProfileSource");

            //Sql Query ausführen
            rs = ps.executeQuery();

            //Sql Antwort auswerten
            List<ProfileSource> sourceConnection = new ArrayList<ProfileSource>();
            while(rs.next()){
                ProfileSource profileSource = new ProfileSource();
                profileSource.setProfileID(rs.getInt("ProfileID"));
                profileSource.setSourceID(rs.getInt("SourceID"));
                sourceConnection.add(profileSource);
            }

            return sourceConnection;

        } catch (Exception e) {
            throw new SQLException(e.getMessage());
        } finally {
            if (conn != null)
            {
                try { conn.close(); } catch (SQLException ignore) {}
            }
        }
    }

    @Override
    public boolean updateUserPermit(String userMail, int permits) throws SQLException {
        if(permits != USER && permits != COMPANY_ADMIN && permits != FULL_ADMIN){
            return false;
        }

        try {
            //Treiber laden
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Verbindung aufbauen
            conn = DriverManager.getConnection(connectionUrl + database + encoding, user, pass);

            //Sql Statement vorbereiten
            PreparedStatement ps = conn.prepareStatement("UPDATE User SET Permits=? WHERE Email=?;");
            ps.setInt(1, permits);
            ps.setString(2, userMail);

            //Sql Query ausführen
            ps.execute();

            return true;
        } catch (Exception e) {
            throw new SQLException(e.getMessage());
        } finally {
            if (conn != null)
            {
                try { conn.close(); } catch (SQLException ignore) {}
            }
        }
    }

    @Override
    public boolean updateUserValidation(String userMail, boolean validated) throws SQLException {
        try {
            //Treiber laden
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Verbindung aufbauen
            conn = DriverManager.getConnection(connectionUrl + database + encoding, user, pass);

            //Sql Statement vorbereiten
            PreparedStatement ps = conn.prepareStatement("UPDATE User SET Validated=? WHERE Email=?;");
            ps.setBoolean(1, validated);
            ps.setString(2, userMail);

            //Sql Query ausführen
            ps.execute();

            return true;
        } catch (Exception e) {
            throw new SQLException(e.getMessage());
        } finally {
            if (conn != null)
            {
                try { conn.close(); } catch (SQLException ignore) {}
            }
        }
    }

    @Override
    public boolean updateProfileData(Profile profile) throws SQLException {
        if(profile==null){
            //Ungültige Instanz von Profil übergeben
            return false;
        }

        try {
            //Treiber laden
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Verbindung aufbauen
            conn = DriverManager.getConnection(connectionUrl + database + encoding, user, pass);

            //Sql Update Statement vorbereiten
            PreparedStatement ps = conn.prepareStatement("UPDATE Profile SET Profilename=?, Sendingtime=? WHERE ProfileID=?;");
            ps.setString(1, profile.getProfilName());
            ps.setString(2, profile.getSendingTime().toString());
            ps.setInt(3, profile.getProfilID());

            //Profile-Table update - Statement ausführen
            ps.execute();

            //Sql Delete Statement vorbereiten
            ps = conn.prepareStatement("DELETE FROM ProfileSource WHERE ProfileID=?;");
            ps.setInt(1, profile.getProfilID());

            //Sql Statement ausführen
            ps.execute();

            //Für jede SourceID eine Verknüpfung zur ProfileID herstellen
            for(Source source: profile.getSources()) {
                //Sql Insert Statement vorbereiten
                ps = conn.prepareStatement("INSERT INTO ProfileSource VALUES (?,?);");
                ps.setInt(1, profile.getProfilID());
                ps.setInt(2, source.getSourceID());

                //Sql Statement ausführen
                ps.execute();
            }

            //Sql Delete Statement vorbereiten
            ps = conn.prepareStatement("DELETE FROM ProfileSearchWord WHERE ProfileID=?;");
            ps.setInt(1, profile.getProfilID());

            //Sql Statement ausführen
            ps.execute();

            //Für jedes Schlagwort eine Verknüpfung zur ProfileID herstellen
            for(String searchterm: profile.getSearchTerms()) {
                //Sql Insert Statement vorbereiten
                ps = conn.prepareStatement("INSERT INTO ProfileSearchWord VALUES (?,?);");
                ps.setInt(1, profile.getProfilID());
                ps.setString(2, searchterm);

                //Sql Statement ausführen
                ps.execute();
            }

            //Hinzufügen von neuem Profil war erfolgreich
            return true;

        }catch(Exception e) {
            //Fehler beim Hinzufügen
            return false;
        } finally {
            if (conn != null)
            {
                try { conn.close(); } catch (SQLException ignore) {}
            }
        }
    }

    @Override
    public boolean updateProfileStatus(int profileID, boolean activated) throws SQLException {
        try {
            //Treiber laden
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Verbindung aufbauen
            conn = DriverManager.getConnection(connectionUrl + database + encoding, user, pass);

            //Sql Statement vorbereiten
            PreparedStatement ps = conn.prepareStatement("UPDATE Profile SET Enabled=? WHERE ProfileID=?");
            ps.setBoolean(1, activated);
            ps.setInt(2, profileID);

            //Sql Query ausführen
            ps.execute();

            return true;

        } catch (Exception e) {
            throw new SQLException(e.getMessage());
        } finally {
            if (conn != null)
            {
                try { conn.close(); } catch (SQLException ignore) {}
            }
        }
    }

    @Override
    public boolean updateProfilename(int profileID, String newProfilename) throws SQLException {
        try {
            //Treiber laden
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Verbindung aufbauen
            conn = DriverManager.getConnection(connectionUrl + database + encoding, user, pass);

            //Sql Statement vorbereiten
            PreparedStatement ps = conn.prepareStatement("UPDATE Profile SET Profilename=? WHERE ProfileID=?");
            ps.setString(1, newProfilename);
            ps.setInt(2, profileID);

            //Sql Query ausführen
            ps.execute();

            return true;

        } catch (Exception e) {
            throw new SQLException(e.getMessage());
        } finally {
            if (conn != null)
            {
                try { conn.close(); } catch (SQLException ignore) {}
            }
        }
    }

    @Override
    public boolean updateArticleDescription(int articleID, String articleDescription) throws SQLException {
        try {
            //Treiber laden
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Verbindung aufbauen
            conn = DriverManager.getConnection(connectionUrl + database + encoding, user, pass);

            //Sql Statement vorbereiten
            PreparedStatement ps = conn.prepareStatement("UPDATE Article SET Description=? WHERE ArticleID=?");
            ps.setString(1, articleDescription);
            ps.setInt(2, articleID);

            //Sql Query ausführen
            ps.execute();

            return true;

        } catch (Exception e) {
            throw new SQLException(e.getMessage());
        } finally {
            if (conn != null)
            {
                try { conn.close(); } catch (SQLException ignore) {}
            }
        }
    }

    public boolean addSearchWords(List<String> searchterms, int profileId) {
        try {
            //Treiber laden
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Verbindung aufbauen
            conn = DriverManager.getConnection(connectionUrl + database + encoding, user, pass);

            StringBuilder databaseQuery = new StringBuilder();
            databaseQuery.append("INSERT INTO ProfileSearchWord VALUES ");

            boolean appendComma = true;
            // baue für jeden searchterm den String der Form "(profileId, searchterm)" auf
            for (String searchterm : searchterms) {
                if(appendComma)
                {
                    databaseQuery.append(", ");
                    appendComma = false;
                }
                databaseQuery.append(" (" + profileId + ", " + searchterm + ")");
            }
            databaseQuery.append(";");

            //Sql Statement vorbereiten
            PreparedStatement ps = conn.prepareStatement(databaseQuery.toString());

            //Sql Query ausführen
            rs = ps.executeQuery();

        } catch (Exception e) {
            return false;
        } finally {
            if (conn != null)
            {
                try { conn.close(); } catch (SQLException ignore) {}
            }
        }

        return true;

    }

    @Override
    public Integer addArticle(Article article) throws SQLException {
        try {
            //Treiber laden
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Verbindung aufbauen
            conn = DriverManager.getConnection(connectionUrl + database + encoding, user, pass);

            //Sql Statement vorbereiten
            PreparedStatement ps = conn.prepareStatement("INSERT INTO Article(Title, Description, PubDate, Link, SourceID) VALUES (?,?,?,?,?)");
            ps.setString(1, article.getTitle());
            ps.setString(2, article.getDescription());
            ps.setString(3, article.getPubDateWorkaround());
            ps.setString(4, article.getLink());
            ps.setInt(5, article.getSourceID());

            //Sql Query ausführen
            ps.execute();

            ps = conn.prepareStatement("SELECT ArticleID FROM Article WHERE Title=? AND PubDate=?");
            ps.setString(1, article.getTitle());
            ps.setString(2, article.getPubDateWorkaround());

            rs = ps.executeQuery();

            Integer articleID = null;
            if(rs.next()){
                articleID = rs.getInt("ArticleID");
            }

            return articleID;

        } catch (Exception e) {
            throw new SQLException(e.getMessage());
        } finally {
            if (conn != null)
            {
                try { conn.close(); } catch (SQLException ignore) {}
            }
        }
    }

    public boolean updateUser(String userMail, String password) throws SQLException {

        try {

            //Treiber laden
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Verbindung aufbauen
            conn = DriverManager.getConnection(connectionUrl + database + encoding, user, pass);

            //Sql Statement vorbereiten
            PreparedStatement ps = conn.prepareStatement("UPDATE User SET Passwort=? WHERE Email=?;");
            ps.setString(1, password);
            ps.setString(2, userMail);

            //Sql Query ausführen
            ps.execute();

            return true;
        } catch (Exception e) {
            throw new SQLException(e.getMessage());
        } finally {
            if (conn != null)
            {
                try { conn.close(); } catch (SQLException ignore) {}
            }
        }

    }

    public boolean addUser(User newUser, String password) {

        if(newUser==null || password.length() < 1){
            //Ungültige Instanz von User übergeben
            //              oder
            //Passwort String 0 Zeichen lang
            return false;
        }

        try {
            //Treiber laden
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Verbindung aufbauen
            conn = DriverManager.getConnection(connectionUrl + database + encoding, user, pass);

            //Sql Insert Statement vorbereiten
            PreparedStatement ps = conn.prepareStatement("INSERT INTO User(Email, Passwort, CompanyID, Validated, Permits) VALUES (?,?,?,?,?)");
            ps.setString(1, newUser.getUserMail());
            ps.setString(2, password);
            ps.setInt(3, newUser.getCompanyID());
            ps.setBoolean(4, newUser.isValidated());
            ps.setInt(5, newUser.getPermits());

            //Sql Statement ausführen
            ps.execute();

            //Hinzufügen von neuem User war erfolgreich
            return true;

        }catch(Exception e) {
            //Fehler beim Hinzufügen
            return false;
        } finally {
            if (conn != null)
            {
                try { conn.close(); } catch (SQLException ignore) {}
            }
        }
    }

    public boolean addProfile(int userID, Profile newProfile) {
        if(newProfile==null){
            //Ungültige Instanz von Profil übergeben
            return false;
        }

        try {
            //Treiber laden
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Verbindung aufbauen
            conn = DriverManager.getConnection(connectionUrl + database + encoding, user, pass);

            

            //Sql Insert Statement vorbereiten
            PreparedStatement ps = conn.prepareStatement("INSERT INTO Profile(Profilename, UserID, Sendingtime, Enabled) VALUES (?,?,?,?);");
            ps.setString(1, newProfile.getProfilName());
            ps.setInt(2, userID);
            ps.setString(3, newProfile.getSendingTime().toString());
            ps.setBoolean(4, newProfile.isActive());

            //Sql Statement ausführen
            ps.execute();

            //Sql Insert Statement vorbereiten
            ps = conn.prepareStatement("SELECT ProfileID FROM Profile WHERE Profilename=? AND UserID=?;");
            ps.setString(1, newProfile.getProfilName());
            ps.setInt(2, userID);

            //Sql Statement ausführen
            rs = ps.executeQuery();

            int profileID;
            if(rs.next()){
                profileID = rs.getInt("ProfileID");
            }else{
                return false;
            }

            //Für jede SourceID eine Verknüpfung zur ProfileID herstellen
            for(Source source: newProfile.getSources()) {
                //Sql Insert Statement vorbereiten
                ps = conn.prepareStatement("INSERT INTO ProfileSource VALUES (?,?);");
                ps.setInt(1, profileID);
                ps.setInt(2, source.getSourceID());

                //Sql Statement ausführen
                ps.execute();
            }

            //Für jedes Schlagwort eine Verknüpfung zur ProfileID herstellen
            for(String searchterm: newProfile.getSearchTerms()) {
                //Sql Insert Statement vorbereiten
                ps = conn.prepareStatement("INSERT INTO ProfileSearchWord VALUES (?,?);");
                ps.setInt(1, profileID);
                ps.setString(2, searchterm);

                System.out.println("ohshit   " + searchterm);

                //Sql Statement ausführen
                ps.execute();
            }

            //Hinzufügen von neuem Profil war erfolgreich
            return true;

        }catch(Exception e) {
            //Fehler beim Hinzufügen
            return false;
        } finally {
            if (conn != null)
            {
                try { conn.close(); } catch (SQLException ignore) {}
            }
        }
    }

    @Override
    public boolean addProfileArticleConnections(List<ProfileArticle> connections) throws SQLException {
        try {
            //Treiber laden
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Verbindung aufbauen
            conn = DriverManager.getConnection(connectionUrl + database + encoding, user, pass);

            //Sql Statement vorbereiten
            String sqlStatement = "INSERT INTO ProfileArticle VALUES ";
            for(int i=0; i<connections.size(); i++){
                if(i<connections.size()-1) {
                    sqlStatement += "(?,?),";
                }else{
                    sqlStatement += "(?,?);";
                }
            }

            PreparedStatement ps = conn.prepareStatement(sqlStatement);
            int count = 1;
            for(ProfileArticle connection: connections) {
                ps.setInt(count, connection.getProfileID());
                ps.setInt(count+1, connection.getArticleID());
                count += 2;
            }

            //Sql Query ausführen
            ps.execute();

            return true;

        } catch (Exception e) {
            throw new SQLException(e.getMessage());
        } finally {
            if (conn != null)
            {
                try { conn.close(); } catch (SQLException ignore) {}
            }
        }
    }

    @Override
    public boolean addTemplate(int companyID, Template newTemplate) throws SQLException {
        if(newTemplate==null){
            //Ungültige Instanz von Template übergeben
            return false;
        }

        try {
            //Treiber laden
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Verbindung aufbauen
            conn = DriverManager.getConnection(connectionUrl + database + encoding, user, pass);

            //Sql Insert Statement vorbereiten
            PreparedStatement ps = conn.prepareStatement("INSERT INTO Templates(CompanyID, Templatename) VALUES (?,?);");
            ps.setInt(1, companyID);
            ps.setString(2, newTemplate.getName());

            //Sql Statement ausführen
            ps.execute();

            //Sql Insert Statement vorbereiten
            ps = conn.prepareStatement("SELECT TemplateID FROM Templates WHERE Templatename=? AND CompanyID=?;");
            ps.setString(1, newTemplate.getName());
            ps.setInt(2, companyID);

            //Sql Statement ausführen
            rs = ps.executeQuery();

            int templateID;
            if(rs.next()){
                templateID = rs.getInt("TemplateID");
            }else{
                return false;
            }

            //Für jede SourceID eine Verknüpfung zur ProfileID herstellen
            for(Source source: newTemplate.getSources()) {
                //Sql Insert Statement vorbereiten
                ps = conn.prepareStatement("INSERT INTO TemplateSource VALUES (?,?);");
                ps.setInt(1, templateID);
                ps.setInt(2, source.getSourceID());

                //Sql Statement ausführen
                ps.execute();
            }

            //Für jedes Schlagwort eine Verknüpfung zur ProfileID herstellen
            for(String searchterm: newTemplate.getSearchTerms()) {
                //Sql Insert Statement vorbereiten
                ps = conn.prepareStatement("INSERT INTO TemplateSearchword VALUES (?,?);");
                ps.setInt(1, templateID);
                ps.setString(2, searchterm);

                //Sql Statement ausführen
                ps.execute();
            }

            //Hinzufügen von neuem Profil war erfolgreich
            return true;

        }catch(Exception e) {
            //Fehler beim Hinzufügen
            throw new SQLException(e.getMessage());
        } finally {
            if (conn != null)
            {
                try { conn.close(); } catch (SQLException ignore) {}
            }
        }
    }

    public boolean removeUser(String userID) {

        int rowsAffected = 0;

        try {
            //Treiber laden
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Verbindung aufbauen
            conn = DriverManager.getConnection(connectionUrl + database + encoding, user, pass);

            //Sql Statement vorbereiten
            PreparedStatement ps = conn.prepareStatement("DELETE FROM User WHERE Email=?");
            ps.setString(1, userID);

            //Sql Query ausführen
            rowsAffected = ps.executeUpdate();

            return rowsAffected == 1;

        } catch (Exception e) {
            return false;
        } finally {
            if (conn != null)
            {
                try { conn.close(); } catch (SQLException ignore) {}
            }
        }
    }

    public boolean removeProfile(int profileID) {

        int rowsAffected = 0;

        try {
            //Treiber laden
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Verbindung aufbauen
            conn = DriverManager.getConnection(connectionUrl + database + encoding, user, pass);

            //Sql Statement vorbereiten
            //PreparedStatement ps = conn.prepareStatement("DELETE Profile FROM Profile INNER JOIN User ON Profile.UserID = User.UserID WHERE Email=? AND ProfileID =?");
            //ps.setString(1, userID);
            //ps.setInt(2, profileID);
            PreparedStatement ps = conn.prepareStatement("DELETE FROM Profile WHERE ProfileID=?;");
            ps.setInt(1, profileID);

            //Sql Query ausführen
            rowsAffected = ps.executeUpdate();

            if(rowsAffected == 1)
            {
                return true;
            }

            return false;

        } catch (Exception e) {
            return false;
        } finally {
            if (conn != null)
            {
                try { conn.close(); } catch (SQLException ignore) {}
            }
        }
    }

    @Override
    public boolean removeTemplate(int companyID, int templateID) {

        int rowsAffected = 0;

        try {
            //Treiber laden
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Verbindung aufbauen
            conn = DriverManager.getConnection(connectionUrl + database + encoding, user, pass);

            //Sql Statement vorbereiten
            PreparedStatement ps = conn.prepareStatement("DELETE FROM Templates WHERE TemplateID=? AND CompanyID=?");
            ps.setInt(1, templateID);
            ps.setInt(2, companyID);

            //Sql Query ausführen
            rowsAffected = ps.executeUpdate();

            if(rowsAffected == 1)
            {
                return true;
            }
            return false;

        } catch (Exception e) {
            return false;
        } finally {
            if (conn != null)
            {
                try { conn.close(); } catch (SQLException ignore) {}
            }
        }
    }

    @Override
    public boolean removeArticle(int articleID) throws SQLException {
        int rowsAffected = 0;

        try {
            //Treiber laden
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Verbindung aufbauen
            conn = DriverManager.getConnection(connectionUrl + database + encoding, user, pass);

            //Sql Statement vorbereiten
            PreparedStatement ps = conn.prepareStatement("DELETE FROM Article WHERE ArticleID=?");
            ps.setInt(1, articleID);

            //Sql Query ausführen
            rowsAffected = ps.executeUpdate();

            return rowsAffected == 1;

        } catch (Exception e) {
            throw new SQLException(e.getMessage());
        } finally {
            if (conn != null)
            {
                try { conn.close(); } catch (SQLException ignore) {}
            }
        }
    }

    @Override
    public boolean removeCompany(int companyID) throws SQLException {
        int rowsAffected = 0;

        try {
            //Treiber laden
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Verbindung aufbauen
            conn = DriverManager.getConnection(connectionUrl + database + encoding, user, pass);

            //Sql Statement vorbereiten
            PreparedStatement ps = conn.prepareStatement("DELETE FROM Companies WHERE CompanyID=?");
            ps.setInt(1, companyID);

            //Sql Query ausführen
            rowsAffected = ps.executeUpdate();

            return rowsAffected == 1;

        } catch (Exception e) {
            throw new SQLException(e.getMessage());
        } finally {
            if (conn != null)
            {
                try { conn.close(); } catch (SQLException ignore) {}
            }
        }
    }

    @Override
    public boolean removeSource(int sourceID) throws SQLException {
        int rowsAffected = 0;

        try {
            //Treiber laden
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Verbindung aufbauen
            conn = DriverManager.getConnection(connectionUrl + database + encoding, user, pass);

            //Sql Statement vorbereiten
            PreparedStatement ps = conn.prepareStatement("DELETE FROM Sources WHERE SourceID=?");
            ps.setInt(1, sourceID);

            //Sql Query ausführen
            rowsAffected = ps.executeUpdate();

            return rowsAffected == 1;

        } catch (Exception e) {
            throw new SQLException(e.getMessage());
        } finally {
            if (conn != null)
            {
                try { conn.close(); } catch (SQLException ignore) {}
            }
        }
    }

    @Override
    public boolean sourceExists(String sourceName, String quellenRSS) throws SQLException {
        try {
            //Treiber laden
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Verbindung aufbauen
            conn = DriverManager.getConnection(connectionUrl + database + encoding, user, pass);

            //Sql Statement vorbereiten
            PreparedStatement ps = conn.prepareStatement("SELECT SourceID FROM Sources WHERE SourceName=? OR LinkRSS=?");
            ps.setString(1, sourceName);
            ps.setString(2, quellenRSS);

            //Sql Query ausführen
            rs = ps.executeQuery();

            //Sql Antwort auswerten

            if(rs.next())
            {
                return true;
            }
            return false;


        } catch (Exception e) {
            throw new SQLException(e.getMessage());
        } finally {
            if (conn != null)
            {
                try { conn.close(); } catch (SQLException ignore) {}
            }
        }
    }

    @Override
    public boolean addSource(Source source) throws SQLException {
        try {
            //Treiber laden
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Verbindung aufbauen
            conn = DriverManager.getConnection(connectionUrl + database + encoding, user, pass);

            //Sql Statement vorbereiten
            PreparedStatement ps = conn.prepareStatement("INSERT INTO Sources(SourceName, LinkSite, LinkRSS) VALUES (?,?,?)");
            ps.setString(1, source.getSourceName());
            ps.setString(2, source.getLinkSite());
            ps.setString(3, source.getLinkRSS());


            //Sql Query ausführen
            ps.execute();

            ps = conn.prepareStatement("SELECT SourceID FROM Sources WHERE SourceName=? ");
            ps.setString(1, source.getSourceName());

            ps.executeQuery();

            return true;



        } catch (Exception e) {
            throw new SQLException(e.getMessage());
        } finally {
            if (conn != null)
            {
                try { conn.close(); } catch (SQLException ignore) {}
            }
        }
    }

    @Override
    public boolean addCompany(String companyName) {
        if(companyName.length() < 1){
            //Name String 0 Zeichen lang
            return false;
        }

        try {
            //Treiber laden
            Class.forName("com.mysql.cj.jdbc.Driver");

            //Verbindung aufbauen
            conn = DriverManager.getConnection(connectionUrl + database + encoding, user, pass);

            //Sql Insert Statement vorbereiten
            PreparedStatement ps = conn.prepareStatement("INSERT INTO Companies(CompanyName) VALUES (?);");
            ps.setString(1, companyName);

            //Sql Statement ausführen
            ps.execute();

            //Hinzufügen von neuem User war erfolgreich
            return true;

        }catch(Exception e) {
            //Fehler beim Hinzufügen
            return false;
        } finally {
            if (conn != null)
            {
                try { conn.close(); } catch (SQLException ignore) {}
            }
        }
    }
}
