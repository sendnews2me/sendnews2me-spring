package com.sendnews2me.service;

import com.sendnews2me.service.DatabaseRepresentation.*;
import org.springframework.jdbc.support.xml.SqlXmlFeatureNotImplementedException;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

public interface IDatabase {
    public String user = "root";
    public String pass = "Goldyg123";
    public String connectionUrl = "jdbc:mysql://46.101.234.140/";
    public String database = "DatabaseComplete";
    public String encoding = "?useUnicode=true&characterEncoding=utf-8&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";

    //public ResultSet sendSqlStatement(String sql);


    public User getUser(String userMail) throws SQLException;
    public String getUserPassword(String userMail) throws SQLException;
    public String getUserAuthentication(String userID);
    public List<Profile> getUserProfiles(int userID) throws Exception;
    public Profile getProfileFromUser(String profilename, int userID);
    public List<User> getUsersFromCompany(int companyID) throws SQLException;
    public List<String> getProfileSearchWords(int profileID);
    public List<ProfileSearchword> getAllSearchwords() throws SQLException;
    public List<Company> getCompanies() throws SQLException;
    public List<Source> getSources() throws SQLException;
    public List<Article> getArticlesFromLastInterval(int sourceID) throws SQLException;
    public List<Template> getTemplatesFromCompany(int companyID) throws SQLException;
    public String getProfilenameFromProfileID(int profileID) throws SQLException;
    public List<Article> getNext20ArticlesAfterArticleID(int profileID, Timestamp maxLoadedPubDate) throws SQLException;
    public List<String> getSystemAdminMails() throws SQLException;
    public List<Profile> getProfilesFromUserWithoutArticles(int userID) throws SQLException;
    public List<ProfileSource> getAllProfileSources() throws SQLException;

    public boolean addSearchWords(List<String> searchterms, int profileId);
    public Integer addArticle(Article article) throws SQLException;

    public boolean updateUser(String userMail, String Password) throws SQLException;
    public boolean updateUserPermit(String userMail, int permits) throws SQLException;
    public boolean updateUserValidation(String userMail, boolean validated) throws SQLException;
    public boolean updateProfileData(Profile profile) throws SQLException;
    public boolean updateProfileStatus(int profileID, boolean activated) throws SQLException;
    public boolean updateProfilename(int profileID, String newProfilename) throws SQLException;
    public boolean updateArticleDescription(int articleID, String articleDescription) throws SQLException;

    public boolean addUser(User newUser, String password);
    public boolean addProfile(int userID, Profile newProfile);
    public boolean addProfileArticleConnections(List<ProfileArticle> connections) throws SQLException;
    public boolean addTemplate(int companyID, Template newTemplate) throws SQLException;
    public boolean addSource(Source newSource) throws SQLException;
    public boolean addCompany(String companyName);

    public boolean removeUser(String userMail);
    public boolean removeProfile(int profileID);
    public boolean removeTemplate(int companyID, int templateID);
    public boolean removeArticle(int articleID) throws SQLException;
    public boolean removeCompany(int companyID) throws SQLException;
    public boolean removeSource(int sourceID) throws SQLException;
    public boolean sourceExists(String sourceName, String quellenRSS) throws SQLException;
}
