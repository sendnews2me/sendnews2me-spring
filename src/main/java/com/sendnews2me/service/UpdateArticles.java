package com.sendnews2me.service;

import com.sendnews2me.service.Constants;
import com.sendnews2me.service.DatabaseForView;
import com.sendnews2me.service.DatabaseRepresentation.*;
import com.sendnews2me.service.RssParser.Feed;
import com.sendnews2me.service.RssParser.FeedMessage;
import com.sendnews2me.service.RssParser.RssFeedParser;
import com.sendnews2me.service.WebCrawler;
import de.l3s.boilerpipe.BoilerpipeProcessingException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.scheduling.annotation.Scheduled;
import org.xml.sax.SAXException;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UpdateArticles implements Constants{
    //Fixed Rate = 7.200.000 Milliseconds -> every 2 hours
    //TODO: uncomment @Scheduled tag when building final version
    //@Scheduled(fixedRate = 7200000, initialDelay = 100000)
    public void updateArticles() { //TODO: wenn Bilder in RSS Feeds vorhanden -> herunterladen und lokal speichern
        System.out.println("********************* com.sendnews2me.service.UpdateArticles (Intervallzeitraum: " + INTERVAL_FOR_UPDATING_ARTICLES + " Stunden): *******************");
        System.out.println("Reading Sources from Database...");
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date)); //2016/11/16 12:08:43
        Instant timeBefore = Instant.now().minus(Duration.ofHours(INTERVAL_FOR_UPDATING_ARTICLES));

        //Aus datenbank Sources mit Rss URLs laden
        DatabaseForView database = new DatabaseForView();
        List<Source> sources;
        try {
            sources = database.getSources();
            System.out.println("Sources: ");
            for (Source source: sources) {
                System.out.println("- " + source.getSourceName());
            }
        } catch (SQLException e) {
            System.out.println("sql exception: " + e.getMessage());
            return;
        }

        List<ProfileSource> sourceConnections;
        try {
            sourceConnections = database.getAllProfileSources();
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }

        for(int i=0; i<sources.size(); i++) {
            System.out.println("\nParsing RSS From Source: " + sources.get(i).getSourceName());

            // Jede RSS Url parsen
            RssFeedParser parser = new RssFeedParser(sources.get(i).getLinkRSS(), timeBefore);
            Feed feed = parser.readFeed();
            feed.setSourceID(sources.get(i).getSourceID());

            for (FeedMessage message: feed.getMessages()) {
                System.out.println("Feed-Message: " + message.getTitle());
            }
            System.out.println("\n");

            System.out.println("Relevante Artikel aus Datenbank: ");
            //Relevante Artikel zur Überprüfung, welche Artikel in Datenbank müssen, aus Datenbank holen
            List<Article> relevantArticles;
            try {
                relevantArticles = database.getArticlesFromLastInterval(sources.get(i).getSourceID());
                for (Article article: relevantArticles) {
                    System.out.println("Datenbank-Artikel: " + article.getTitle());
                }
            } catch (SQLException e) {
                System.out.println("sql exception: " + e.getMessage());
                return;
            }

            System.out.println("\n");
            System.out.println("check if existing...");
            // Überprüfen, welche Artikel von den geparsten RSS-Seiten in die Datenbank eingetragen werden müssen
            List<Article> newArticles = new ArrayList<Article>();
            /* Für jeden Artikel aus dem geparsten RSS Feed überprüfen, ob er mit einem der relevanten Artikel aus der
               Datenbank übereinstimmt */
            for(int k=0; k < feed.getMessages().size(); k++){
                if(feed.getMessages().get(k).getTitle().length() > 120){ feed.getMessages().get(k).setTitle(feed.getMessages().get(k).getTitle().substring(0,120)); }
                boolean alreadyExisting = false;
                for(int p=0; p < relevantArticles.size(); p++){
                    if(relevantArticles.get(p).getTitle().equals(feed.getMessages().get(k).getTitle())){
                        alreadyExisting = true;
                        System.out.println("Artikel existiert schon: " + relevantArticles.get(p).getTitle());
                    }
                }
                if(!alreadyExisting){
                    Article newArticle = new Article();
                    newArticle.setSourceID(feed.getSourceID());
                    newArticle.setTitle(feed.getMessages().get(k).getTitle());
                    newArticle.setDescription(feed.getMessages().get(k).getDescription());
                    newArticle.setLink(feed.getMessages().get(k).getLink());
                    newArticle.setPubDateWorkaround(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(feed.getMessages().get(k).getPubDate()));
                    newArticles.add(newArticle);
                    System.out.println("Artikel neu erstellen: " + newArticle.getTitle());
                }
            }

            //die neuen Artikel in Datenbank eintragen
            for(int g=0; g<newArticles.size(); g++){
                try {
                    if(newArticles.get(g).getTitle().length() > 120){ newArticles.get(g).setTitle(newArticles.get(g).getTitle().substring(0,119)); }
                    if(newArticles.get(g).getDescription().length() > 700){ newArticles.get(g).setDescription(newArticles.get(g).getDescription().substring(0,699)); }
                    if(newArticles.get(g).getLink().length() > 300){ newArticles.remove(g);}
                    Integer articleID = database.addArticle(newArticles.get(g));
                    if(articleID != null) {
                        System.out.println("Artikel in Datenbank hinzugefügt: " + newArticles.get(g).getTitle());
                        newArticles.get(g).setArticleID(articleID);
                    }else{
                        return;
                    }
                } catch (SQLException e) {
                    System.out.println("sql exception: " + e.getMessage());
                    return;
                }
            }

            // auf neue Artikel WebCrawler losschicken
            List<Article> articlesToDelete = new ArrayList<Article>();
            System.out.println("Starting WebCrawler for new Articles: ");
            WebCrawler crawler = new WebCrawler();
            for(int z=0; z < newArticles.size(); z++) {
                System.out.println("Crawling from \"" + newArticles.get(z).getLink() + "\"...");
                try {
                    String fullText = crawler.crawlAndSave(newArticles.get(z).getLink(), newArticles.get(z).getArticleID());
                    newArticles.get(z).setFullText(fullText);
                    newArticles.get(z).setFullTextTitle(crawler.getTitle());
                    System.out.println("Saved as \"" + newArticles.get(z).getArticleID() + ".txt\"");
                } catch (IOException e) {
                    System.out.println("Crawler Exception: " + e.getMessage());
                    articlesToDelete.add(newArticles.get(z));
                    newArticles.remove(z);
                } catch (SAXException e) {
                    System.out.println("Crawler Exception: " + e.getMessage());
                    articlesToDelete.add(newArticles.get(z));
                    newArticles.remove(z);
                } catch (BoilerpipeProcessingException e) {
                    System.out.println("Crawler Exception: " + e.getMessage());
                    articlesToDelete.add(newArticles.get(z));
                    newArticles.remove(z);
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            //aktualisierte Artikel User-Profilen zuordnen (Schlagwörter)!
            List<ProfileArticle> newProfileArticleConnections = new ArrayList<ProfileArticle>();

            //Schlagwörter aus Datenbank holen
            List<ProfileSearchword> searchwords = null;
            try {
                searchwords = database.getAllSearchwords();
            } catch (SQLException e) {
                System.out.println("SQL Exception while getting Searchwords");
                return;
            }

            //Neue Artikel nach Schlagwörtern durchsuchen
            for(Article article: newArticles){
                boolean connected = false;
                System.out.println("checking article: " + article.getArticleID() + " - " + article.getTitle());
                for(ProfileSearchword searchword: searchwords){
                    boolean profileActivatedForSource = false;
                    for(ProfileSource profileSource: sourceConnections){
                        if(profileSource.getProfileID() == searchword.getProfileID()){
                            if(profileSource.getSourceID() == sources.get(i).getSourceID()){
                                profileActivatedForSource = true;
                            }
                        }
                    }
                    if(!profileActivatedForSource){
                        System.out.println(sources.get(i).getSourceName() + "NOT activated for Profile " + searchword.getProfileID());
                        continue;
                    }
                    System.out.println(sources.get(i).getSourceName() + "activated for Profile " + searchword.getProfileID());
                    System.out.println("searchword   " + searchword.getProfileID() + "-" + searchword.getSearchword());
                    if (    article.getTitle().contains(searchword.getSearchword()) ||
                            article.getDescription().contains(searchword.getSearchword()) ||
                            article.getFullText().contains(searchword.getSearchword())) {
                        boolean alreadyConnected = false;
                        for(ProfileArticle con: newProfileArticleConnections){
                            if(searchword.getProfileID() == con.getProfileID() && article.getArticleID() == con.getArticleID()){
                                System.out.println("profile already connected:  " + searchword.getProfileID());
                                alreadyConnected = true;
                            }
                        }
                        if(!alreadyConnected) {
                            //download image from news site
                            if(article.getDescription().contains("<img")) {

                                String imgString = null;
                                try {
                                    imgString = article.getDescription().substring(article.getDescription().indexOf("<img"), article.getDescription().indexOf(">", article.getDescription().indexOf("<img")) + 1);
                                    System.out.println("imgString: "+ imgString);
                                    System.out.println("articledesc: " + article.getDescription());
                                    String result1 = article.getDescription().replaceAll(imgString, "");
                                    imgString = imgString.substring(imgString.indexOf("src=")+5, imgString.indexOf("\"", imgString.indexOf("src=")+5));
                                    System.out.println("imgUrl: " + imgString);
                                    System.out.println("1." + result1);
                                    if (result1.contains("<br")) {
                                        String brString = result1.substring(result1.indexOf("<br"), result1.indexOf(">", result1.indexOf("<br")) + 1);
                                        System.out.println("2." + result1);
                                        result1 = result1.replaceAll(brString, "");
                                    }
                                    System.out.println("3." +result1);
                                    result1 = result1.trim();
                                    article.setDescription(result1);
                                    System.out.println("4.result1: " + result1);
                                    try {
                                        database.updateArticleDescription(article.getArticleID(), article.getDescription());
                                    } catch (SQLException e) {
                                        e.printStackTrace();
                                    }
                                }catch(Exception e){
                                    imgString = null;
                                }

                                if(imgString != null) {
                                    BufferedImage image = null;
                                    try {
                                        URL url = new URL(imgString);
                                        image = ImageIO.read(url);

                                        ImageIO.write(image, "png", new File(PATH_TO_IMAGE_FILES + article.getArticleID() + ".png"));

                                    } catch (IOException e) {
                                        e.printStackTrace();
                                        System.out.println("error while downloading image");
                                    }
                                }
                            }

                            ProfileArticle connection = new ProfileArticle(searchword.getProfileID(), article.getArticleID());
                            newProfileArticleConnections.add(connection);
                            System.out.println("Article " + article.getArticleID() + "-" + article.getTitle() + " connected with " + searchword.getProfileID());
                            connected = true;
                        }

                    }
                }
                if(!connected){
                    //Artikel wird von keinem Suchbegriff gematcht -> kann aus Datenbank entfernt werden
                    articlesToDelete.add(article);
                    System.out.println("Deleting Article " + article.getArticleID() + "-" + article.getTitle());
                }
            }

            //Zuordnungen zwischen Artikel und Profilen in Datenbank einfügen
            if(newProfileArticleConnections.size() > 0) {
                try {
                    database.addProfileArticleConnections(newProfileArticleConnections);
                } catch (SQLException e) {
                    System.out.println("SQLException while adding ProfileArticleConnections " + e.getMessage());
                }
            }

            //nicht verknüpfte Artikel aus Datenbank wieder löschen
            for(Article article: articlesToDelete){
                try {
                    System.out.println("deleting article: " + article.getArticleID() + " - " + article.getTitle());
                    database.removeArticle(article.getArticleID());

                    //remove full article .txt file
                    File file = new File(PATH_TO_FULL_ARTICLES + article.getArticleID() + ".txt");
                    if(file.delete()){
                        System.out.println(file.getName() + " is deleted!");
                    }else{
                        System.out.println("******************** Delete operation is failed.**************");
                    }
                } catch (Exception e) {
                    System.out.println("Exception while deleting articles");
                }
            }

            System.out.println("\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
        }

        System.out.println("--- ALL DONE ---");
    }
}
