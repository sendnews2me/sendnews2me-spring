var maxLoadedArticleId;

function showArticlesFromProfileID(profileID){
    maxLoadedArticleId = 0;
    if(profileID == null){
        document.getElementById('noProfileChoosen').style.display = 'block';
        return;
    }else{
        document.getElementById('noProfileChoosen').style.display = 'none';
    }

    var articleLists = document.getElementsByClassName("articleList");
    var profileListTd1 = document.getElementsByClassName("first");
    var profileListTd2 = document.getElementsByClassName("second");
    var profileListTd3 = document.getElementsByClassName("third");
    for(var i=0; i < articleLists.length; i++){
        articleLists[i].style.display = 'none';
    }
    for(var k=0; k < profileListTd1.length; k++){
        //Reset all background-colors in profile list
        profileListTd1[k].style.backgroundColor = '#343642';
        profileListTd2[k].style.backgroundColor = '#343642';
        profileListTd3[k].style.backgroundColor = '#343642';
    }
    //show article list with right profileID
    document.getElementById(profileID + "_articleList").style.display = 'block';
    //set background-color of profileTd with right profileID
    document.getElementById(profileID + "_profileList1").style.backgroundColor = '#5a5d72';
    document.getElementById(profileID + "_profileList2").style.backgroundColor = '#5a5d72';
    document.getElementById(profileID + "_profileList3").style.backgroundColor = '#5a5d72';

    window.scroll(0,0);
}

function showRenameField(profileID){
    document.getElementById(profileID + '_name').style.display = 'none';
    document.getElementById(profileID + '_inputName').style.display = 'inline';
}

function deleteProfile(confirmValue, profileID, profileName){
    if(confirmValue) {
        document.getElementById(profileName).value = 'delete';
        document.getElementById('updateProfileForm_' + profileID).submit();
    }
}

function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
}

function loadMoreArticles(profilID, maxLoadedPubDateFromProfile){
    document.getElementById("loadMore" + profilID).innerText = "Laden...";
    document.getElementById("loadMore" + profilID).disabled = true;

    try {
        document.getElementById("endReached" + profilID).remove();
    }catch(e){}

    var ajaxdata =  "data=" + profilID + ";" + maxLoadedPubDateFromProfile;


    $.ajax({
        type: "post",
        url: "loadMore" ,
        data: ajaxdata,
        success : function(successData) {
            document.getElementById("articles" + profilID).innerHTML += successData;
            document.getElementById("maxLoadedPubDate_" + profilID).value = document.getElementById("maxLoadedPubDateTemp").value;
            document.getElementById("maxLoadedPubDateTemp").remove();
            if(document.getElementById("endReached" + profilID) != null){
                document.getElementById("loadMore" + profilID).innerText = "Keine weiteren Artikel";
            }else {
                document.getElementById("loadMore" + profilID).innerText = "Weitere Artikel laden";
                document.getElementById("loadMore" + profilID).disabled = false;
            }

        }
    });
}

function resetColoredSearchwords(articleID, profileID, fullText, title){
    document.getElementById("title" + articleID + "_" + profileID).innerHTML = title;
    var fullTextParagraph = document.getElementById("fullText" + articleID + "_" + profileID);
    fullTextParagraph.innerHTML = fullText;
}

function coloredSearchwords(articleID, profileID, searchwords){
    var fullText = document.getElementById("fullText" + articleID + "_" + profileID);
    var title = document.getElementById("title" + articleID + "_" + profileID);
    for(var i=0; i<searchwords.length; i++) {
        fullText.innerHTML = fullText.innerHTML.replace(new RegExp(searchwords[i], 'g'), "<p style=\"background:yellow; display:inline;\">" + searchwords[i] + "</p>");
        title.innerHTML = title.innerHTML.replace(new RegExp(searchwords[i], 'g'), "<p style=\"background:yellow; display:inline;\">" + searchwords[i] + "</p>");
    }
}