//Variables
var overlay = $("#overlay"),
    fab = $(".fab"),
    cancel = $("#cancel");

var overlayChange = $("#overlayChange"),
    fabChange = $(".fabChange"),
    cancelChange = $("#cancelChange");

//fab click
fab.on('click', openFAB);
overlay.on('click', closeFAB);
cancel.on('click', closeFAB);

//fab click
fabChange.on('click', openFABchange);
overlayChange.on('click', closeFABchange);
cancelChange.on('click', closeFABchange);

function openFAB(event) {
    if (event) event.preventDefault();
    fab.addClass('active');
    overlay.addClass('dark-overlay');
    fab.scroll(0,0);
}

function closeFAB(event) {
    if (event) {
        event.preventDefault();
        event.stopImmediatePropagation();
    }

    fab.removeClass('active');
    overlay.removeClass('dark-overlay');

}

function openFABchange(event) {
    if (event) event.preventDefault();
    fabChange.addClass('active');
    overlayChange.addClass('dark-overlay');
    fab[0].style.display = 'none';
    fabChange.scroll(0,0);

}

function closeFABchange(event) {
    if (event) {
        event.preventDefault();
        event.stopImmediatePropagation();
    }

    fabChange.removeClass('active');
    overlayChange.removeClass('dark-overlay');
    fab[0].style.display = 'block';
}


var searchwords = [];

function addNewSearchword(focus){
    var inputField = document.getElementById("text2");
    if(inputField.value.length < 1){
        //inputField is empty
        inputField.focus();
        document.getElementById("searchwordErrorMsg").innerHTML = "Eingabefeld darf nicht leer sein.";
        setTimeout(function(){
            document.getElementById("searchwordErrorMsg").innerHTML = '';
        }, 2000);
        return;
    }

    //check if searchword already existing
    for(var i=0; i<searchwords.length; i++){
        if(searchwords[i] === inputField.value){
            //searchword already existing
            inputField.focus();
            document.getElementById("searchwordErrorMsg").innerHTML = "Schlagwort exisitiert bereits in diesem Profil.";
            setTimeout(function(){
                document.getElementById("searchwordErrorMsg").innerHTML = '';
            }, 2000);
            return;
        }
    }

    //searchword not existing, add it
    searchwords.push(inputField.value);
    var ul = document.getElementsByClassName("searchwords")[0];
    var li = document.createElement("li");
    li.appendChild(document.createTextNode(inputField.value));
    li.setAttribute("id", inputField.value);
    var span = document.createElement("span");
    span.appendChild(document.createTextNode("×"));
    span.setAttribute("class", "deleteSearchword");
    li.appendChild(span);
    ul.appendChild(li);
    inputField.value = '';
    if(focus === true) {
        inputField.focus();
    }
}

function createInputForSubmitting(){
    var searchwordsString;
    for(var k=0; k<searchwords.length; k++){
        if(k < searchwords.length-1) {
            searchwordsString += searchwords[k] + ";";
        }else{
            searchwordsString += searchwords[k];
        }
    }
    document.getElementById("searchwordsInput").value = searchwordsString;

    document.getElementById("sendingTimeSelectValue").value = $("select[name='emailSendingTimeSelect'] option:selected").index();

    var sourceIDs = "";
    var checkboxes = document.getElementsByClassName("newsCheckbox");
    for(var m=0; m<checkboxes.length; m++){
        if(checkboxes[m].checked){
            sourceIDs += checkboxes[m].id + ";";
        }
    }
    document.getElementById("sourceIdList").value = sourceIDs;

    var form = document.getElementById("createProfileForm");
    form.submit();
}

function createInputForSubmittingTemplate(){
    var searchwordsString;
    for(var k=0; k<searchwords.length; k++){
        if(k < searchwords.length-1) {
            searchwordsString += searchwords[k] + ";";
        }else{
            searchwordsString += searchwords[k];
        }
    }
    document.getElementById("searchwordsInput").value = searchwordsString;

    var sourceIDs = "";
    var checkboxes = document.getElementsByClassName("newsCheckbox");
    for(var m=0; m<checkboxes.length; m++){
        if(checkboxes[m].checked){
            sourceIDs += checkboxes[m].id + ";";
        }
    }
    document.getElementById("sourceIdList").value = sourceIDs;

    var form = document.getElementById("createTemplateForm");
    form.submit();
}




var searchwordsChange = [];

function addNewSearchwordChange(){
    var inputField = document.getElementById("text2Change");
    if(inputField.value.length < 1){
        //inputField is empty
        inputField.focus();
        document.getElementById("searchwordErrorMsgChange").innerHTML = "Eingabefeld darf nicht leer sein.";
        setTimeout(function(){
            document.getElementById("searchwordErrorMsgChange").innerHTML = '';
        }, 2000);
        return;
    }

    //check if searchword already existing
    for(var i=0; i<searchwordsChange.length; i++){
        if(searchwordsChange[i] === inputField.value){
            //searchword already existing
            inputField.focus();
            document.getElementById("searchwordErrorMsgChange").innerHTML = "Schlagwort exisitiert bereits in diesem Profil.";
            setTimeout(function(){
                document.getElementById("searchwordErrorMsgChange").innerHTML = '';
            }, 2000);
            return;
        }
    }

    //searchword not existing, add it
    searchwordsChange.push(inputField.value);
    var ul = document.getElementsByClassName("searchwordsChange")[0];
    var li = document.createElement("li");
    li.appendChild(document.createTextNode(inputField.value));
    li.setAttribute("id", inputField.value);
    var span = document.createElement("span");
    span.appendChild(document.createTextNode("×"));
    span.setAttribute("class", "deleteSearchwordChange");
    li.appendChild(span);
    ul.appendChild(li);
    inputField.value = '';
    inputField.focus();
}

function createSearchwordInputChange(){
    var searchwordsString;
    for(var k=0; k<searchwordsChange.length; k++){
        if(k < searchwordsChange.length-1) {
            searchwordsString += searchwordsChange[k] + ";";
        }else{
            searchwordsString += searchwordsChange[k];
        }
    }
    document.getElementById("searchwordsInputChange").value = searchwordsString;

    /*var select = document.getElementById('sendingTimeSelectValue');
    var sendingTimeValue = select.options[select.selectedIndex].value;*/

    document.getElementById("sendingTimeSelectValueChange").value = $("select[name='emailSendingTimeSelectChange'] option:selected").index();

    var sourceIDs = "";
    var checkboxes = document.getElementsByClassName("newsCheckboxChange");
    for(var m=0; m<checkboxes.length; m++){
        if(checkboxes[m].checked){
            sourceIDs += checkboxes[m].id + ";";
        }
    }
    document.getElementById("sourceIdListChange").value = sourceIDs;

    var form = document.getElementById("createProfileFormChange");
    form.submit();
}

function showChangeProfile(profileID, profileName, sources, searchterms, sendingtime){
    //delete all li tags in ul list
    document.getElementsByClassName("searchwordsChange")[0].innerHTML = '';
    searchwordsChange = [];
    //uncheck all checkboxes
    var checkboxes = document.getElementsByClassName("newsCheckboxChange");
    for(var m=0; m < checkboxes.length; m++){
        checkboxes[m].checked = false;
    }

    document.getElementById("oldProfilenameChange").value = profileName;
    document.getElementById("profilenameChange").value = profileName;

    for(var k=0; k<searchterms.length; k++){
        document.getElementById("text2Change").value = searchterms[k];
        addNewSearchwordChange();
    }
    eventlistenersChange();

    for(var f=0; f < sources.length; f++){
        console.log(sources[f].toString());
        document.getElementById(sources[f]+"Change").checked = true;
    }

    if(sendingtime === "00:00:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 1;
    }else if(sendingtime === "00:30:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 2;
    }else if(sendingtime === "01:00:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 3;
    }else if(sendingtime === "01:30:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 4;
    }else if(sendingtime === "02:00:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 5;
    }else if(sendingtime === "02:30:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 6;
    }else if(sendingtime === "03:00:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 7;
    }else if(sendingtime === "03:30:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 8;
    }else if(sendingtime === "04:00:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 9;
    }else if(sendingtime === "04:30:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 10;
    }else if(sendingtime === "05:00:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 11;
    }else if(sendingtime === "05:30:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 12;
    }else if(sendingtime === "06:00:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 13;
    }else if(sendingtime === "06:30:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 14;
    }else if(sendingtime === "07:00:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 15;
    }else if(sendingtime === "07:30:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 16;
    }else if(sendingtime === "08:00:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 17;
    }else if(sendingtime === "08:30:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 18;
    }else if(sendingtime === "09:00:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 19;
    }else if(sendingtime === "09:30:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 20;
    }else if(sendingtime === "10:00:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 21;
    }else if(sendingtime === "10:30:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 22;
    }else if(sendingtime === "11:00:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 23;
    }else if(sendingtime === "11:30:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 24;
    }else if(sendingtime === "12:00:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 25;
    }else if(sendingtime === "12:30:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 26;
    }else if(sendingtime === "13:00:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 27;
    }else if(sendingtime === "13:30:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 28;
    }else if(sendingtime === "14:00:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 29;
    }else if(sendingtime === "14:30:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 30;
    }else if(sendingtime === "15:00:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 31;
    }else if(sendingtime === "15:30:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 32;
    }else if(sendingtime === "16:00:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 33;
    }else if(sendingtime === "16:30:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 34;
    }else if(sendingtime === "17:00:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 35;
    }else if(sendingtime === "17:30:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 36;
    }else if(sendingtime === "18:00:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 37;
    }else if(sendingtime === "18:30:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 38;
    }else if(sendingtime === "19:00:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 39;
    }else if(sendingtime === "19:30:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 40;
    }else if(sendingtime === "20:00:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 41;
    }else if(sendingtime === "20:30:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 42;
    }else if(sendingtime === "21:00:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 43;
    }else if(sendingtime === "21:30:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 44;
    }else if(sendingtime === "22:00:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 45;
    }else if(sendingtime === "22:30:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 46;
    }else if(sendingtime === "23:00:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 47;
    }else if(sendingtime === "23:30:00"){
        document.getElementById("emailSendingTimeSelectChange").value = 48;
    }

}


function fillWithTemplateValues(templateID, templateName, sourceIDs, searchterms){
    //delete all li tags in ul list
    document.getElementsByClassName("searchwords")[0].innerHTML = '';
    searchwords = [];
    //uncheck all checkboxes
    var checkboxes = document.getElementsByClassName("newsCheckbox");
    for(var m=0; m < checkboxes.length; m++){
        checkboxes[m].checked = false;
    }

    document.getElementById("text1").value = templateName;

    for(var k=0; k<searchterms.length; k++){
        document.getElementById("text2").value = searchterms[k];
        addNewSearchword(false);
    }
    eventlisteners();

    for(var f=0; f < sourceIDs.length; f++){
        document.getElementById(sourceIDs[f]).checked = true;
    }
}

function deleteAllSearchwords(){
    //delete all li tags in ul list
    document.getElementsByClassName("searchwords")[0].innerHTML = '';
    searchwords = [];
}