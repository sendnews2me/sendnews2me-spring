function toggleDisplayCreateTemplate(){
    var createTemplateDiv = document.getElementById("createTemplateDiv");
    if(createTemplateDiv.style.display === 'none') {
        createTemplateDiv.style.display = 'block';
        document.getElementById("dropdownTriangle").innerHTML = "▲";
    }else{
        createTemplateDiv.style.display = 'none';
        document.getElementById("dropdownTriangle").innerHTML = "▼";
    }
}

function deleteTemplate(confirmValue, templateID){
    if(confirmValue) {
        document.getElementById('formTemplate_' + templateID).submit();
    }
}