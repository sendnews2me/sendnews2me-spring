<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: L
  Date: 26.04.2018
  Time: 16:45
  To change this template use File | Settings | File Templates.

  Description:
  Neues Template erstellen
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Templateverwaltung</title>

    <link rel="stylesheet" href="../../resources/css/style_1.css">
    <link rel="stylesheet" href="../../resources/css/floatingbtn.css">
    <link rel="stylesheet" href="../../resources/css/stickysidebar.css">
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

    <!--<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

    <!-- RESPONSIVE NAVBAR -->
    <c:if test="${sessionScope.user == null}">
        <%-- User ist noch nicht eingeloggt: Gast Navigationsleiste --%>
        <link rel="stylesheet" href="../../resources/css/navbarStyle_StandardUser.css">
    </c:if>
    <c:if test="${sessionScope.user != null && sessionScope.permits == 1}">
        <%-- Normaler User ist eingeloggt: User Navigationsleiste --%>
        <link rel="stylesheet" href="../../resources/css/navbarStyle_StandardUser.css">
    </c:if>
    <c:if test="${sessionScope.user != null && sessionScope.permits == 2}">
        <%-- Firmen-Admin ist eingeloggt: Firmen-Admin Navigationsleiste --%>
        <link rel="stylesheet" href="../../resources/css/navbarStyle_Admin.css">
    </c:if>
    <c:if test="${sessionScope.user != null && sessionScope.permits >= 3}">
        <%-- Firmen-Admin ist eingeloggt: Firmen-Admin Navigationsleiste --%>
        <link rel="stylesheet" href="../../resources/css/navbarStyle_Admin.css">
    </c:if>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="../../resources/js/index.js"></script>

</head>
<body onload="load('${param.errorMsg}', '${param.successMsg}');">

<header role="banner" class="header">
    <div id="cd-logo"><a class="cd-logo" href="/"><img src="../../resources/media/Logo_white.png" height="40px"
                                                       alt="Logo"></a></div>

    <nav class="main-nav">
        <c:if test="${sessionScope.user == null}">
            <%-- User ist noch nicht eingeloggt: Gast Navigationsleiste --%>
            <ul>
                <li><a class="cd-signin" style="color: white">Anmelden</a></li>
                <li><a class="cd-signup" style="color: white">Registrieren</a></li>
            </ul>
        </c:if>
        <c:if test="${sessionScope.user != null && sessionScope.permits == 1}">
            <%-- Normaler User ist eingeloggt: User Navigationsleiste --%>
            <ul>
                <li><a class="cd-navbutton" href="/clipping">Pressespiegel</a></li>
                <li><a  ></a></li>
                <li><a class="cd-navbutton"href="/account/settings">Einstellungen</a></li>
                <li><a class="cd-logout" href="/account/logout">Abmelden</a></li>
            </ul>
        </c:if>
        <c:if test="${sessionScope.user != null && sessionScope.permits == 2}">
            <%-- Firmen-Admin ist eingeloggt: Firmen-Admin Navigationsleiste --%>
            <ul>
                <li><a class="cd-navbutton" href="/clipping">Pressespiegel</a></li>
                <!--<li><a class="cd-navbutton" href="/clipping/suggestSource">Quellverwaltung</a></li>-->
                <li><a class="cd-navbutton" href="/clipping/template">Templates</a></li>
                <li><a class="cd-navbutton" href="/account/logging">Logdateien</a></li>
                <li><a class="cd-navbutton" href="/account/manageUsers">Mitarbeiter</a></li>
                <li><a  ></a></li>
                <li><a class="cd-navbutton"href="/account/settings" <%--style="background: #7d7d7d; border: #3E5D89"--%>>Einstellungen</a></li>
                <li><a class="cd-logout" href="/account/logout">Abmelden</a></li>
            </ul>
        </c:if>
        <c:if test="${sessionScope.user != null && sessionScope.permits >= 3}">
            <%-- Firmen-Admin ist eingeloggt: Firmen-Admin Navigationsleiste --%>
            <ul>
                <li><a class="cd-navbutton" href="/clipping">Pressespiegel</a></li>
                <li><a class="cd-navbutton" href="/clipping/suggestSource">Quellen</a></li>
                <!--<li><a class="cd-navbutton" href="/clipping/template">Templateverwaltung</a></li>-->
                <li><a class="cd-navbutton" href="/account/logging">Logdateien</a></li>
                <li><a class="cd-navbutton" href="/account/mainManageUsers">Nutzer</a></li>
                <li><a  ></a></li>
                <li><a class="cd-navbutton"href="/account/settings" <%--style="background: #7d7d7d; border: #3E5D89"--%>>Einstellungen</a></li>
                <li><a class="cd-logout" href="/account/logout">Abmelden</a></li>
            </ul>
        </c:if>
    </nav>
</header>

<div class="topLine" style="cursor: pointer;" onclick="toggleDisplayCreateTemplate();"><p>Neues Template erstellen</p><p id="dropdownTriangle" style="display: inline; float:right; margin-right: 20px; margin-top: 5px;">&#9660;</p></div>
<div class="middleDiv" id="createTemplateDiv" style="display: none;">
<form id="createTemplateForm" name="createTemplateForm" action="/clipping/createTemplate" method="post">
                <input style="margin: 20px; width: 95%;" type="text" id="templatenameInput" name="templatenameInput" placeholder="Templatename" />

                <div class="container">
                    <table class="table table-borderless">
                        <tbody>
                        <c:forEach var="source" items="${sources}">
                            <tr>
                                <td>
                                    <label style="padding-left: 50px; font-size: 18px; height: 30px;" class="checkboxContainer">${source.sourceName}
                                        <input class="newsCheckbox" id="${source.sourceID}" style="width: 100%; height: 30px; left: 0; z-index: 2;" type="checkbox">
                                        <span class="checkmark"></span>
                                    </label>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                    <input type="hidden" id="sourceIdList" name="sourceIdList" value="">
                </div>
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    <input style="display:inline; width: 60%; margin-left: 20px;" type="text" id="text2" placeholder="Schlagwort" />
                    <button onclick="addNewSearchword(true); eventlisteners();" type="button" style="display:inline; width:35%; border-radius: 0 !important;">Hinzufügen</button>
                    <!--<label class="mdl-textfield__label" for="text2" ></label>-->
                </div>
                <p id="searchwordErrorMsg" style="color: red; margin-left: 20px;"></p>

            <ul class="searchwords" style="margin-left: 20px;">
            </ul>

            <input type="hidden" name="searchwordsInput" id="searchwordsInput" value="">

            <script>
                function eventlisteners() {
                    var closebtns = document.getElementsByClassName("deleteSearchword");
                    var i;

                    for (i = 0; i < closebtns.length; i++) {
                        closebtns[i].addEventListener("click", function () {
                            var id = this.parentElement.innerHTML.substring(0,this.parentElement.innerText.length-2);
                            var li = document.getElementById(id);
                            if(li != null){
                                li.remove();
                            }

                            var index = searchwords.indexOf(id);
                            if (index > -1) {
                                searchwords.splice(index, 1);
                            }
                        });
                    }
                }
            </script>
    <button type="reset" onclick="deleteAllSearchwords();" class="buttonDistance">Reset</button>
    <button type="submit" onclick="createInputForSubmittingTemplate();" class="buttonDistance">Erstellen</button>
</form>
</div>


<div class="topLine" style="margin-top: 50px; height: 50px;"><p>Templates von ${companyname}:</p></div>
<table style="color: white; width: 90%; margin: 0 5%;" class="table table-hover">
    <tbody> <!-- Show all templates from the same company -->
    <c:forEach var="template" items="${templates}">
        <form id="formTemplate_${template.templateID}" name="formTemplates" action="/clipping/deleteTemplate" method="post">
            <tr>
                <td style="color: white; width: 90%; font-size: 23px;"><input type="hidden" id="templateName" name="templateIdToDelete"
                                                 value="${template.templateID}">${template.name}</td>
                <!--<td>
                    <button type="button" onclick="//TODO: ändern popup">Ändern</button>
                </td>-->
                <td style="color: white; width: 10%;">
                    <img class="validationimg" onclick="deleteTemplate(confirm('Template \'' + '${template.name}' + '\' wirklich löschen?'), ${template.templateID});" src="../../resources/media/roteskreuz.png"
                                               style="width:30px; margin-top: 10px; cursor: pointer;">
                </td>
            </tr>
        </form>
    </c:forEach>
    </tbody>
</table>

<br>
<br>


<script>
    function load(errorMsgText, successMsgText) {
        var nanobar = new Nanobar();
        nanobar.go(30);
        var successMsg = getUrlParameter('successMsg');
        if(successMsg && successMsg != null){
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };

            toastr.success(successMsgText);
        }

        var errorMsg = getUrlParameter('errorMsg');
        if(errorMsg && errorMsg != null){
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };

            toastr.error('Error: \r\n' + errorMsgText);
        }
        window.history.pushState("", "", '/clipping/template');
        nanobar.go(100);
    }
</script>

<script src="../../resources/js/nanobar.js"></script>
<script src="../../resources/js/floatingbtn.js"></script>
<script src="../../resources/js/template.js"></script>
<script src="../../resources/js/clipping.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.2/js/toastr.min.js"></script>


</body>
</html>
