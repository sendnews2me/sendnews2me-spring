<%--
  Description:
  Diese View ziegt die Indexseite an, welche angezeigt wird, wenn man nicht angemeldet ist.
  Es werden die Buttons angezeigt für:
  -Anmeldung
  -Registrierung

${sessionScope.user.userMail}
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SendNews2.Me</title>

    <link rel="stylesheet" href="../../resources/css/style_1.css">
    <link rel="stylesheet" href="../../resources/css/floatingbtn.css">
    <link rel="stylesheet" href="../../resources/css/stickysidebar.css">
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

    <!-- RESPONSIVE NAVBAR -->
    <c:if test="${sessionScope.user == null}">
        <%-- User ist noch nicht eingeloggt: Gast Navigationsleiste --%>
        <link rel="stylesheet" href="../../resources/css/navbarStyle_StandardUser.css">
    </c:if>
    <c:if test="${sessionScope.user != null && sessionScope.permits == 1}">
        <%-- Normaler User ist eingeloggt: User Navigationsleiste --%>
        <link rel="stylesheet" href="../../resources/css/navbarStyle_StandardUser.css">
    </c:if>
    <c:if test="${sessionScope.user != null && sessionScope.permits == 2}">
        <%-- Firmen-Admin ist eingeloggt: Firmen-Admin Navigationsleiste --%>
        <link rel="stylesheet" href="../../resources/css/navbarStyle_Admin.css">
    </c:if>
    <c:if test="${sessionScope.user != null && sessionScope.permits >= 3}">
        <%-- Firmen-Admin ist eingeloggt: Firmen-Admin Navigationsleiste --%>
        <link rel="stylesheet" href="../../resources/css/navbarStyle_Admin.css">
    </c:if>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>


</head>
<body onload="doOnload('${param.errorMsg}', '${param.successMsg}')">
<header role="banner" class="header">
    <div id="cd-logo"><a class="cd-logo" href="/"><img src="../../resources/media/Logo_white.png" height="40px"
                                                       alt="Logo"></a></div>

    <nav class="main-nav">
        <c:if test="${sessionScope.user == null}">
            <%-- User ist noch nicht eingeloggt: Gast Navigationsleiste --%>
            <ul>
                <li><a class="cd-signin" style="color: white">Anmelden</a></li>
                <li><a class="cd-signup" style="color: white">Registrieren</a></li>
            </ul>
        </c:if>
        <c:if test="${sessionScope.user != null && sessionScope.permits == 1}">
            <%-- Normaler User ist eingeloggt: User Navigationsleiste --%>
            <ul>
                <li><a class="cd-navbutton" href="/clipping">Pressespiegel</a></li>
                <li><a  ></a></li>
                <li><a class="cd-navbutton"href="/account/settings">Einstellungen</a></li>
                <li><a class="cd-logout" href="/account/logout">Abmelden</a></li>
            </ul>
        </c:if>
        <c:if test="${sessionScope.user != null && sessionScope.permits == 2}">
            <%-- Firmen-Admin ist eingeloggt: Firmen-Admin Navigationsleiste --%>
        <ul>
            <li><a class="cd-navbutton" href="/clipping">Pressespiegel</a></li>
            <li><a class="cd-navbutton" href="/clipping/template">Templates</a></li>
            <li><a class="cd-navbutton" href="/account/logging">Logdateien</a></li>
            <li><a class="cd-navbutton" href="/account/manageUsers">Mitarbeiter</a></li>
            <li><a  ></a></li>
            <li><a class="cd-navbutton"href="/account/settings">Einstellungen</a></li>
            <li><a class="cd-logout" href="/account/logout">Abmelden</a></li>
        </ul>
        </c:if>
        <c:if test="${sessionScope.user != null && sessionScope.permits >= 3}">
            <%-- Main-Admin ist eingeloggt: Main-Admin Navigationsleiste --%>
            <ul>
                <li><a class="cd-navbutton" href="/clipping">Pressespiegel</a></li>
                <li><a class="cd-navbutton" href="/clipping/suggestSource">Quellen</a></li>
                <li><a class="cd-navbutton" href="/account/logging">Logdateien</a></li>
                <li><a class="cd-navbutton" href="/account/mainManageUsers">Nutzer</a></li>
                <li><a  ></a></li>
                <li><a class="cd-navbutton"href="/account/settings">Einstellungen</a></li>
                <li><a class="cd-logout" href="/account/logout">Abmelden</a></li>
            </ul>
        </c:if>
    </nav>
</header>


<div class="cd-user-modal">
    <div class="cd-user-modal-container">
        <ul class="cd-switcher">
            <li><a>Anmelden</a></li>
            <li><a>Account erstellen</a></li>
        </ul>

        <div id="cd-login"> <!-- log in Anzeige -->
            <form class="cd-form" action="/account/login" method="post">

                <%--<c:if test="${param.act eq 'nuf'}">
                    <p class="fieldset" style="color:red;">
                            ${param.errorMsg}
                    </p>
                </c:if>--%>

                <p class="fieldset">
                    <input class="full-width has-padding has-border" id="signin-email" type="email" name="signin-email"
                           placeholder="E-Mail">
                </p>

                <p class="fieldset">
                    <input class="full-width has-padding has-border" id="signin-password" autocomplete="off"
                           type="password"
                           name="signin-password" placeholder="Passwort">
                    <a class="hide-password">Zeigen</a>
                </p>

                <!--<p class="fieldset">
                    <input type="checkbox" id="remember-me" checked>
                    <label for="remember-me">Remember me</label>
                </p>-->

                <p class="fieldset">
                    <input class="full-width" type="submit" name="submit" value="Anmelden">
                </p>
            </form>

            <p class="cd-form-bottom-message"><a style="color: #FFF; text-decoration: underline; cursor: pointer;">Passwort
                vergessen?</a></p>
        </div> <!-- cd-login -->

        <div id="cd-signup"> <!-- sign up Anzeige -->
            <form class="cd-form" name="registerForm" action="/account/register" method="post">

                <%--<c:if test="${param.act eq 'reg'}">
                    <p class="fieldset" style="color:red;">
                            ${param.errorMsg}
                    </p>
                </c:if>--%>

                <div class="select-wrapper">
                    <select id="companySelector">
                        <option style="align-items: center;" selected>Firma ausw&auml;hlen...</option>
                        <c:forEach var="company" items="${companies}">
                            <option style="align-content: center">${company.companyName}</option>
                        </c:forEach>
                    </select>
                    <input type="hidden" id="selectedCompany" name="selectedCompany" value="0">
                </div>

                <script>
                    //Script: sobald Dropdown Item geändert wird der Wert von input(type=hidden) auf diesen Wert gesetzt
                    var selectmenu = document.getElementById("companySelector");
                    selectmenu.onchange = function () {
                        var chosenoption = this.options[this.selectedIndex];
                        var chosenindex = this.options.selectedIndex;
                        if (chosenindex === 0) {
                            document.registerForm.selectedCompany.value = 0;
                        } else {
                            document.registerForm.selectedCompany.value = chosenoption.text;
                        }
                    }
                </script>

                <p class="fieldset">
                    <input class="full-width has-padding has-border" id="signup-email" name="signup-email" type="email"
                           placeholder="E-Mail">
                </p>

                <p class="fieldset">
                    <input class="full-width has-padding has-border" id="signup-password_1" autocomplete="off"
                           name="signup-password_1"
                           type="password" placeholder="Passwort">
                    <a class="hide-password">Zeigen</a>
                </p>

                <p class="fieldset">
                    <input class="full-width has-padding has-border" id="signup-password_2" autocomplete="off"
                           name="signup-password_2"
                           type="password" placeholder="Passwort wiederholen">
                    <a class="hide-password">Zeigen</a>
                </p>

                <!--<p class="fieldset">
                    <input type="checkbox" id="accept-terms">
                    <label for="accept-terms">I agree to the <a href="#0">Terms</a></label>
                </p>-->

                <p class="fieldset">
                    <input class="full-width has-padding" type="submit" value="Account erstellen">
                </p>
            </form>

            <!-- <a href="#0" class="cd-close-form">Close</a> -->
        </div> <!-- cd-signup -->

        <div id="cd-reset-password"> <!-- Passwort zurücksetzten -->
            <p class="cd-form-message">Passwort vergessen? Bitte geben Sie Ihre E-Mail Adresse ein. Es wird Ihnen ein
                neues Passwort zugesendet!</p>

            <form class="cd-form" name="resetPW" action="/account/resetPassword" method="post">
                <p class="fieldset">
                    <input class="full-width has-padding has-border" id="reset-email" name="reset-email" type="email"
                           placeholder="E-Mail">
                </p>


                <p class="fieldset">
                    <input class="full-width has-padding" type="submit" value="Passwort zuruecksetzen">
                </p>
            </form>

            <p class="cd-form-bottom-message"><a style="color: #FFF; text-decoration: underline; cursor: pointer;">Zurueck
                zur Anmeldung</a></p>
        </div> <!-- cd-reset-password -->
    </div> <!-- cd-user-modal-container -->
</div> <!-- cd-user-modal -->

<div id="centerMessage">

    <h1 style="color: red;">${errorMsg}</h1>
    <h2 style="color: green;">${successMsg}</h2>
    <c:if test="${sessionScope.user == null}">
        <p class="sendNewsIntroB"><span style="font-style: italic">SendNews2.Me</span> ist der einfache Weg personalisierte Nachrichten zu erhalten. Nach Kategorien und Interessen sortierte und zusammengestellte Artikel werden dem Nutzer direkt in einem einfachen Format zugestellt und bieten eine schnelle &Uuml;bersicht des t&auml;glichen Geschehens.

            Um Zugang zu <span style="font-style: italic">SendNews2.Me</span> zu erhalten, registrieren Sie sich durch den Button rechts oben, oder melden Sie sich an.</p>
    </c:if>

    <c:if test="${sessionScope.user != null && sessionScope.permits == 1}">
        <p class="sendNewsIntroB">Sie können sich jetzt durch <span style="font-style: italic">SendNews2.Me</span> Ihre <a href="/clipping/">personalisierten Nachrichten</a> zukommen lassen.</p>
    </c:if>

    <c:if test="${sessionScope.user != null && sessionScope.permits >= 2}">
        <p class="sendNewsIntroB">Sie können sich jetzt durch <span style="font-style: italic">SendNews2.Me</span> Ihre <a href="/clipping/">personalisierten Nachrichten</a> zukommen lassen.</p>
    </c:if>

    <c:if test="${sessionScope.user == null}">
        <br>
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            </ol>
            <div style="border-radius: 10px; border: 1px solid #BFBFBF; -webkit-box-shadow: 0 0 5px 2px #aaaaaa;
    -moz-box-shadow: 0 0 5px 2px #aaaaaa;
    box-shadow: 0 0 5px 2px #aaaaaa;" class="carousel-inner">
                <div class="carousel-item active">
                    <div class="grad1">
                    <img class="d-block w-100" src="../../resources/media/carousel_clipping.png" alt="Pressespiegel">
                    </div>
                    <div class="carousel-caption d-none d-md-block">
                        <h5>Pressespiegel</h5>
                        <p>Automatisch erstellte Pressespiegel</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="grad1">
                    <img class="d-block w-100" src="../../resources/media/carousel_profiles.png" alt="Profile">
                    </div>
                    <div class="carousel-caption d-none d-md-block">
                        <h5>Profile</h5>
                        <p>Definieren Sie ihre eigenen Profile</p>
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <br>
    </c:if>
    <p class="sendNewsIntroS">Zur Kontaktaufnahme benutzen Sie folgende E-Mail Adresse: <a style="color: black; font-style: italic;" href="mailto:sendnewstwome@gmail.com">sendnewstwome@gmail.com</a></p>
</div>

<script>
    function doOnload(errorMsgText, successMsgText) {
        var nanobar = new Nanobar();
        nanobar.go(30);
        var act = getUrlParameter('act');
        if (act === 'nuf') {
            $('.cd-user-modal').addClass('is-visible');
            login_selected();
        } else if (act === 'reg') {
            $('.cd-user-modal').addClass('is-visible');
            signup_selected();
        }

        var successMsg = getUrlParameter('successMsg');
        if(successMsg && successMsg != null){
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };

            toastr.success(successMsgText);
        }

        var errorMsg = getUrlParameter('errorMsg');
        if(errorMsg && errorMsg != null){
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-top-full-width",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "10000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };

            toastr.error('Error: \r\n' + errorMsgText);
        }
        window.history.pushState("", "", '/');
        nanobar.go(100);
    }
</script>

<!--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>


<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script src="../../resources/js/index.js"></script>
<script src="../../resources/js/nanobar.js"></script>
<script src="../../resources/js/clipping.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.2/js/toastr.min.js"></script>

</body>

</html>
