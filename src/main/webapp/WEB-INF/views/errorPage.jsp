<%--
Description:
  Diese View stellt die Validierungsseite dar und gibt Nachrichten an den Nutzer, falls dieser noch nicht freigeschalten wurde.

    //TODO: Validierungsmessage schöner machen

--%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Error</title>
    <link rel="stylesheet" href="../../resources/css/style_1.css">
    <link rel="stylesheet" href="../../resources/css/floatingbtn.css">
    <link rel="stylesheet" href="../../resources/css/stickysidebar.css">
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

    <!-- RESPONSIVE NAVBAR -->
    <c:if test="${sessionScope.user == null}">
        <%-- User ist noch nicht eingeloggt: Gast Navigationsleiste --%>
        <link rel="stylesheet" href="../../resources/css/navbarStyle_StandardUser.css">
    </c:if>
    <c:if test="${sessionScope.user != null && sessionScope.permits == 1}">
        <%-- Normaler User ist eingeloggt: User Navigationsleiste --%>
        <link rel="stylesheet" href="../../resources/css/navbarStyle_StandardUser.css">
    </c:if>
    <c:if test="${sessionScope.user != null && sessionScope.permits == 2}">
        <%-- Firmen-Admin ist eingeloggt: Firmen-Admin Navigationsleiste --%>
        <link rel="stylesheet" href="../../resources/css/navbarStyle_Admin.css">
    </c:if>
    <c:if test="${sessionScope.user != null && sessionScope.permits >= 3}">
        <%-- Firmen-Admin ist eingeloggt: Firmen-Admin Navigationsleiste --%>
        <link rel="stylesheet" href="../../resources/css/navbarStyle_Admin.css">
    </c:if>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>
</head>

<body onload="load('${param.errorMsg}', '${param.successMsg}');">

<header role="banner">
    <div id="cd-logo"><a class="cd-logo" href="/"><img src="../../resources/media/Logo_white.png" height="40"
                                                       alt="Logo"></a></div>

    <nav class="main-nav">
        <c:if test="${sessionScope.user == null}">
            <%-- User ist noch nicht eingeloggt: Gast Navigationsleiste --%>
            <ul>
                <li><a class="cd-signin" style="color: white">Anmelden</a></li>
                <li><a class="cd-signup" style="color: white">Registrieren</a></li>
            </ul>
        </c:if>
        <c:if test="${sessionScope.user != null && sessionScope.permits == 1}">
            <%-- Normaler User ist eingeloggt: User Navigationsleiste --%>
            <ul>
                <li><a class="cd-navbutton" href="/clipping">Pressespiegel</a></li>
                <li><a  ></a></li>
                <li><a class="cd-navbutton"href="/account/settings">Einstellungen</a></li>
                <li><a class="cd-logout" href="/account/logout">Abmelden</a></li>
            </ul>
        </c:if>
        <c:if test="${sessionScope.user != null && sessionScope.permits == 2}">
            <%-- Firmen-Admin ist eingeloggt: Firmen-Admin Navigationsleiste --%>
            <ul>
                <li><a class="cd-navbutton" href="/clipping">Pressespiegel</a></li>
                <!--<li><a class="cd-navbutton" href="/clipping/suggestSource">Quellverwaltung</a></li>-->
                <li><a class="cd-navbutton" href="/clipping/template">Templates</a></li>
                <li><a class="cd-navbutton" href="/account/logging">Logdateien</a></li>
                <li><a class="cd-navbutton" href="/account/manageUsers">Mitarbeiter</a></li>
                <li><a  ></a></li>
                <li><a class="cd-navbutton"href="/account/settings" <%--style="background: #7d7d7d; border: #3E5D89"--%>>Einstellungen</a></li>
                <li><a class="cd-logout" href="/account/logout">Abmelden</a></li>
            </ul>
        </c:if>
        <c:if test="${sessionScope.user != null && sessionScope.permits >= 3}">
            <%-- Firmen-Admin ist eingeloggt: Firmen-Admin Navigationsleiste --%>
            <ul>
                <li><a class="cd-navbutton" href="/clipping">Pressespiegel</a></li>
                <li><a class="cd-navbutton" href="/clipping/suggestSource">Quellen</a></li>
                <!--<li><a class="cd-navbutton" href="/clipping/template">Templateverwaltung</a></li>-->
                <li><a class="cd-navbutton" href="/account/logging">Logdateien</a></li>
                <li><a class="cd-navbutton" href="/account/mainManageUsers">Nutzer</a></li>
                <li><a  ></a></li>
                <li><a class="cd-navbutton"href="/account/settings" <%--style="background: #7d7d7d; border: #3E5D89"--%>>Einstellungen</a></li>
                <li><a class="cd-logout" href="/account/logout">Abmelden</a></li>
            </ul>
        </c:if>
    </nav>
</header>

<div id="centerMessage">

<p class="sendNewsIntroB">${errorMsg}</p>
    <p class="sendNewsIntroB"><a href="/">Zurück zur Startseite</a></p>
</div>

<script>
    function load(errorMsgText, successMsgText) {
        var nanobar = new Nanobar();
        nanobar.go(30);
        var successMsg = getUrlParameter('successMsg');
        if(successMsg && successMsg != null){
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };

            toastr.success(successMsgText);
        }

        var errorMsg = getUrlParameter('errorMsg');
        if(errorMsg && errorMsg != null){
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };

            toastr.error('Error: \r\n' + errorMsgText);
        }
        window.history.pushState("", "", '/error');
        nanobar.go(100);
    }
</script>

<script src="../../resources/js/nanobar.js"></script>

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script src="../../resources/js/index.js"></script>
<script src="../../resources/js/clipping.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.2/js/toastr.min.js"></script>

</body>

</html>

