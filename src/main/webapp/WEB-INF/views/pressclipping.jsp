<%--
  Description:
  Diese View stellt den Pressespiegel aus dem Archiv dar.

--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Pressespiegel</title>

    <link rel="stylesheet" href="../../resources/css/style_1.css">
    <link rel="stylesheet" href="../../resources/css/clipping.css">
    <link rel="stylesheet" href="../../resources/css/floatingbtn.css">
    <link rel="stylesheet" href="../../resources/css/stickysidebar.css">
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">


    <!-- RESPONSIVE NAVBAR -->
    <c:if test="${sessionScope.user == null}">
        <%-- User ist noch nicht eingeloggt: Gast Navigationsleiste --%>
        <link rel="stylesheet" href="../../resources/css/navbarStyle_StandardUser.css">
    </c:if>
    <c:if test="${sessionScope.user != null && sessionScope.permits == 1}">
        <%-- Normaler User ist eingeloggt: User Navigationsleiste --%>
        <link rel="stylesheet" href="../../resources/css/navbarStyle_StandardUser.css">
    </c:if>
    <c:if test="${sessionScope.user != null && sessionScope.permits == 2}">
        <%-- Firmen-Admin ist eingeloggt: Firmen-Admin Navigationsleiste --%>
        <link rel="stylesheet" href="../../resources/css/navbarStyle_Admin.css">
    </c:if>
    <c:if test="${sessionScope.user != null && sessionScope.permits >= 3}">
        <%-- Firmen-Admin ist eingeloggt: Firmen-Admin Navigationsleiste --%>
        <link rel="stylesheet" href="../../resources/css/navbarStyle_Admin.css">
    </c:if>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <script type="javascript" src="../../resources/js/clipping.js"></script>
    <link rel="script" href="../../resources/js/clipping.js">

    <script src="<c:url value="/resources/js/clipping.js" />"></script>


</head>
<body onload="load('${profiles.get(0).profilID}', '${param.errorMsg}', '${param.successMsg}');">
<header role="banner" class="header">
    <div id="cd-logo"><a class="cd-logo" href="/"><img src="../../resources/media/Logo_white.png" height="40px"
                                                       alt="Logo"></a></div>

    <nav class="main-nav">
        <c:if test="${sessionScope.user == null}">
            <%-- User ist noch nicht eingeloggt: Gast Navigationsleiste --%>
            <ul>
                <li><a class="cd-signin" style="color: white">Anmelden</a></li>
                <li><a class="cd-signup" style="color: white">Registrieren</a></li>
            </ul>
        </c:if>
        <c:if test="${sessionScope.user != null && sessionScope.permits == 1}">
            <%-- Normaler User ist eingeloggt: User Navigationsleiste --%>
            <ul>
                <li><a class="cd-navbutton" href="/clipping">Pressespiegel</a></li>
                <li><a  ></a></li>
                <li><a class="cd-navbutton"href="/account/settings">Einstellungen</a></li>
                <li><a class="cd-logout" href="/account/logout">Abmelden</a></li>
            </ul>
        </c:if>
        <c:if test="${sessionScope.user != null && sessionScope.permits == 2}">
            <%-- Firmen-Admin ist eingeloggt: Firmen-Admin Navigationsleiste --%>
            <ul>
                <li><a class="cd-navbutton" href="/clipping">Pressespiegel</a></li>
                <!--<li><a class="cd-navbutton" href="/clipping/suggestSource">Quellverwaltung</a></li>-->
                <li><a class="cd-navbutton" href="/clipping/template">Templates</a></li>
                <li><a class="cd-navbutton" href="/account/logging">Logdateien</a></li>
                <li><a class="cd-navbutton" href="/account/manageUsers">Mitarbeiter</a></li>
                <li><a  ></a></li>
                <li><a class="cd-navbutton"href="/account/settings" <%--style="background: #7d7d7d; border: #3E5D89"--%>>Einstellungen</a></li>
                <li><a class="cd-logout" href="/account/logout">Abmelden</a></li>
            </ul>
        </c:if>
        <c:if test="${sessionScope.user != null && sessionScope.permits >= 3}">
            <%-- Firmen-Admin ist eingeloggt: Firmen-Admin Navigationsleiste --%>
            <ul>
                <li><a class="cd-navbutton" href="/clipping">Pressespiegel</a></li>
                <li><a class="cd-navbutton" href="/clipping/suggestSource">Quellen</a></li>
                <!--<li><a class="cd-navbutton" href="/clipping/template">Templateverwaltung</a></li>-->
                <li><a class="cd-navbutton" href="/account/logging">Logdateien</a></li>
                <li><a class="cd-navbutton" href="/account/mainManageUsers">Nutzer</a></li>
                <li><a  ></a></li>
                <li><a class="cd-navbutton"href="/account/settings" <%--style="background: #7d7d7d; border: #3E5D89"--%>>Einstellungen</a></li>
                <li><a class="cd-logout" href="/account/logout">Abmelden</a></li>
            </ul>
        </c:if>
    </nav>
    <div class="pageHeaderLine"></div>
</header>

<!-- Sidebar Profile -->
<div class="sidenav">
    <div class="pageHeaderLineSidenav"></div>
    <div style="margin: 0; background-color: #3E5D89;">
        <h4 style="color: white; text-shadow: 0.5px 0.5px 1px #5a5a5a; margin-left: 20px; padding-top:10px; padding-bottom: 20px; margin-bottom: 0;">Profile:</h4>
    </div>

    <c:if test="${profiles.size() == 0 || profiles == null}">
    <table class="table-borderless" style="margin-top: 15px;">
    </c:if>
    <c:if test="${profiles.size() != 0 && profiles != null}">
    <table class="table-borderless" style="margin-top: 0;">
    </c:if>
        <tbody>
            <c:if test="${profiles.size() == 0 || profiles == null}">
                <tr><td><h6 id="noProfilesExisting" style="max-width: 110px; margin: 20px; display: inline; vertical-align: calc(70%); color: white; text-shadow: 0.5px 0.5px 1px #5a5a5a;">Keine Profile vorhanden</h6></td></tr>
            </c:if>
            <c:forEach var="profile" items="${profiles}" varStatus="loop">
                <form id="updateProfileForm_${profile.profilID}" action="/clipping/updateProfile" method="post">
                    <input type="hidden" id="${profile.profilName}" name="updateFunction" value="">
                    <input type="hidden" name="profilID" value="${profile.profilID}">
                    <tr>
                        <c:if test="${loop.index == 0}">
                            <tr>
                                <td class="first bottomline"><div class="sidenavProfileSeparator"></div></td>
                                <td class="second"><div class="sidenavProfileSeparator"></div></td>
                                <td class="third"><div class="sidenavProfileSeparator"></div></td>
                            </tr>
                        </c:if>
                        <td class="first" id="${profile.profilID}_profileList1">
                            <c:if test="${profile.active == true}">
                                <label class="switch" style="display: inline-block; margin-top: 10px;">
                                    <input onclick="document.getElementById('${profile.profilName}').value='deactivate'; this.form.submit();" name="toggle" type="checkbox" checked>
                                    <span class="blueslider round"></span>
                                </label>
                            </c:if>
                            <c:if test="${profile.active == false}">
                                <label class="switch" style="display: inline-block; margin-top: 10px;">
                                    <input onclick="document.getElementById('${profile.profilName}').value='activate'; this.form.submit();" name="toggle" type="checkbox">
                                    <span class="blueslider round"></span>
                                </label>
                            </c:if>
                        </td>
                        <td class="second" id="${profile.profilID}_profileList2" onclick="showArticlesFromProfileID(${profile.profilID});">
                            <c:if test="${profile.active == true}">
                                <h5 id="${profile.profilID}_name" style="max-width: 110px; display: inline; vertical-align: calc(70%); color: white; text-shadow: 0.5px 0.5px 1px #5a5a5a; cursor: pointer;">${profile.shortProfilName}</h5>
                            </c:if>
                            <c:if test="${profile.active == false}">
                                <h5 id="${profile.profilID}_name" style="max-width: 110px; display: inline; vertical-align: calc(70%); color: white; text-shadow: 0.5px 0.5px 1px #5a5a5a; cursor: pointer;">${profile.shortProfilName}</h5>
                            </c:if>
                            <input id="${profile.profilID}_inputName" type="text" name="newProfilename" style="width: 110px; height: 45px; vertical-align: calc(85%); display: none; overflow-x: auto;" value="${profile.profilName}" placeholder="Profilname">
                        </td>
                        <td class="third" id="${profile.profilID}_profileList3">
                            <div class="btn-group">
                                <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img src="../../resources/media/threeDots_showMore_Icon.png" width="50px">
                                </button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <button onclick="prepareChangeProfile('${profile.profilID}'); openFABchange();" class="dropdown-item" type="button">Ändern</button>
                                    <button onclick="document.getElementById('${profile.profilName}').value='rename'; showRenameField(${profile.profilID});" class="dropdown-item" type="button">Umbenennen</button>
                                    <div class="dropdown-divider"></div>
                                    <button onclick="deleteProfile(confirm('Profil \'' + '${profile.profilName}' + '\' wirklich löschen? \r\n\r\nAlle in diesem Profil gespeicherten \r\nVerknüpfungen zu archivierten Artikeln \r\nwerden unwiderruflich gelöscht!'), ${profile.profilID}, '${profile.profilName}');" class="dropdown-item" type="button">Löschen</button>
                                </div>
                                <script>
                                    function prepareChangeProfile(profileID){
                                        <c:forEach var="profileIterator" items="${profiles}">
                                            if('${profileIterator.profilID}' === profileID){

                                                var searchterms = [];
                                                <c:forEach var="searchterm" items="${profileIterator.searchTerms}">
                                                searchterms.push('${searchterm}');
                                                </c:forEach>

                                                console.log('test');

                                                var sourceIDs = [];
                                                <c:forEach var="source" items="${profileIterator.sources}">
                                                console.log("yey");
                                                sourceIDs.push('${source.sourceID}');
                                                </c:forEach>

                                                showChangeProfile('${profileIterator.profilID}', '${profileIterator.profilName}', sourceIDs, searchterms, '${profileIterator.sendingTime}');
                                            }
                                        </c:forEach>
                                    }
                                </script>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="first bottomline"><div class="sidenavProfileSeparator"></div></td>
                        <td class="second"><div class="sidenavProfileSeparator"></div></td>
                        <td class="third"><div class="sidenavProfileSeparator"></div></td>
                    </tr>
                </form>
            </c:forEach>
        </tbody>
    </table>
</div>

<!-- Page content -->
<div class="main">
    <h4 style="color:red;">${errorMsg}</h4>
    <!-- Add all page content inside this div if you want the side nav to push page content to the right (not used if you only want the sidenav to sit on top of the page -->
    <c:forEach var="profile" items="${profiles}">
        <div class="articleList" id="${profile.profilID}_articleList" style="display: none;">
            <div id="articles${profile.profilID}">
                <input id="maxLoadedPubDate_${profile.profilID}" type="hidden" value="${profile.maxLoadedPubDate}">
            <c:if test="${profile.articles.size() > 0}">
                <c:forEach var="article" items="${profile.articles}">
                    <div class="jumbotron jumbotron-fluid" id="article${article.articleID}">
                        <div class="container">
                            <h1 class="articleheading" style="margin-left: 0; padding-left: 0; display:inline;">${article.title}</h1><br>
                            <p style="display: inline;">${article.sourceName} - </p><p style="color:grey; display:inline;">${article.pubDateString}</p>
                            <br>
                            <br>
                            <img onerror="this.style.display = 'none';" style="display:inline;" src="/image/${article.articleID}">
                            <br>
                            <p style="display: inline;"> ${article.description}</p>
                            <br>
                            <br>
                            <a class="button" onclick="document.getElementById('modal${article.articleID}${profile.profilID}').style.display = 'block';" style="margin-right: 5px;" href="javascript:void(0);">Weiterlesen</a>
                            <p style="display: inline;">|</p>
                            <a class="button" style="margin-left: 5px;" target="_blank" href="${article.link}">Originalartikel</a>

                            <div id="modal${article.articleID}${profile.profilID}" class="modal">
                                <!-- Modal content -->
                                <div class="modal-content">
                                    <input style="display:none;" class="coloredSearchwordsCheckbox" id="checkboxColorSearchwords${article.articleID}_${profile.profilID}" type="checkbox">
                                    <img width="25" style="position:fixed;right:16.5%;margin-top:5px;" id="bucket${article.articleID}_${profile.profilID}" src="../../resources/media/coloredBucket.png" onclick="prepareColorSearchwords${article.articleID}${profile.profilID}('${article.articleID}', '${profile.profilID}', '${article.fullText}', '${article.title}');">
                                    <span style="display:inline;" class="close" onclick="document.getElementById('modal${article.articleID}${profile.profilID}').style.display = 'none';">&times;</span>
                                    <h4 id="title${article.articleID}_${profile.profilID}" style="display: inline; max-width: 90%;">${article.title}</h4>
                                    <p id="fullText${article.articleID}_${profile.profilID}" style="width: 91%; text-align: justify;">${article.fullText}</p>
                                    <script>
                                        function prepareColorSearchwords${article.articleID}${profile.profilID}(articleID, profileID, fullText, title){
                                            var checkbox = document.getElementById("checkboxColorSearchwords" + articleID + "_" + profileID);
                                            if(checkbox.checked){
                                                checkbox.checked = false;
                                                document.getElementById("bucket" + articleID + "_" + profileID).src = "../../resources/media/coloredBucket.png";
                                                resetColoredSearchwords(articleID, profileID, fullText, title);
                                            }else{
                                                checkbox.checked = true;
                                                document.getElementById("bucket" + articleID + "_" + profileID).src = "../../resources/media/uncoloredBucket.png";
                                                var searchwords = [];
                                                <c:forEach var="searchword" items="${profile.searchTerms}">
                                                    searchwords.push('${searchword}');
                                                </c:forEach>
                                                coloredSearchwords(articleID, profileID, searchwords);
                                            }
                                        }
                                    </script>
                                </div>

                            </div>
                        </div>
                    </div>
                </c:forEach>
            </c:if>
            <c:if test="${profile.articles.size() == 0 || profile.articles == null}">
                <h4 style="color: white;">Keine Artikel in diesem Profil vorhanden.</h4>
            </c:if>
            </div>

            <c:if test="${profile.articles.size() >= 20}">
                <button class="btn btn-outline-secondary loadMore" id="loadMore${profile.profilID}" onclick="prepareLoadMoreArticles(${profile.profilID});" style="border-radius: 2px !important; position: absolute; left: 50%; width: 200px;">Weitere Artikel laden</button>
                <script>
                    function prepareLoadMoreArticles(profilID){
                        var articleList = document.getElementById(profilID + "_articleList");
                        var maxLoadedPubDateString = document.getElementById("maxLoadedPubDate_" + profilID).value;

                        loadMoreArticles(profilID, maxLoadedPubDateString);
                    }
                </script>
            </c:if>

        </div>
    </c:forEach>
    <c:if test="${profiles.size() == 0 || profiles == null}">
        <h4 style="color: white;">Erstellen Sie mit dem Plus Button ein neues Profil.</h4>
    </c:if>
    <c:if test="${profiles.size() != 0 && profiles != null}">
        <h4 id="noProfileChoosen" style="color: white;">Wählen Sie ein Profil aus der Liste auf der linken Seite.</h4>
    </c:if>

    <div class="row">
        <div class="col-12 text-center">

        </div>
    </div>
</div>

<!-- Floating Button: Profil erstellen -->
    <div id="overlay" onclick="closeFAB();"></div>
        <div class="fab">
            <i class="material-icons fab-icon"><img src="../../resources/media/plus_Icon.png"></i>
            <!--<img src="../../resources/media/threeDots_showMore_Icon.png">-->
            <form class='cntt-wrapper' id="createProfileForm" name="createProfileForm" action="/clipping/createProfile" method="post">
                <div id="fab-hdr">
                    <h3>Neues Profil erstellen</h3>
                    <div class="form-group" style="margin-left: 20px; margin-right: 30px;">
                        <select class="custom-select" name="templateSelect" id="templateSelect">
                            <option value="0">Template wählen...</option>
                            <c:forEach var="template" items="${templates}">
                                <option value="${template.templateID}">${template.name}</option>
                            </c:forEach>
                        </select>
                        <script>
                            function prepareTemplateFilling(){

                                var selectedTemplateID = $("select[name='templateSelect'] option:selected").val();
                                document.getElementById("text1").value = selectedTemplateID;
                                <c:forEach var="templateIterator" items="${templates}">
                                if('${templateIterator.templateID}' === selectedTemplateID){

                                    var searchterms = [];
                                    <c:forEach var="searchterm" items="${templateIterator.searchTerms}">
                                    searchterms.push('${searchterm}');
                                    </c:forEach>

                                    var sourceIDs = [];
                                    <c:forEach var="source" items="${templateIterator.sources}">
                                    sourceIDs.push('${source.sourceID}');
                                    </c:forEach>

                                    fillWithTemplateValues('${templateIterator.templateID}', '${templateIterator.name}', sourceIDs, searchterms);
                                }
                                </c:forEach>
                            }
                        </script>
                    </div>
                </div>

                <div class="cntt">
                    <div class="form-group">
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <input required class="mdl-textfield__input" type="text" id="text1" name="profilename" placeholder="Profilname" />
                        <!--<label class="mdl-textfield__label" for="text2"></label>-->
                    </div>
                        <br>

                    <!--<div class="input-group">-->
                        <input type="hidden" name="sendingTimeSelectValue" id="sendingTimeSelectValue" value="">
                        <select required name="emailSendingTimeSelect" class="custom-select" id="emailSendingTimeSelect">
                            <option value="" selected>Tägliche E-Mail Sendezeit wählen...</option>
                            <option value="1">00:00</option>
                            <option value="2">00:30</option>
                            <option value="3">01:00</option>
                            <option value="4">01:30</option>
                            <option value="5">02:00</option>
                            <option value="6">02:30</option>
                            <option value="7">03:00</option>
                            <option value="8">03:30</option>
                            <option value="9">04:00</option>
                            <option value="10">04:30</option>
                            <option value="11">05:00</option>
                            <option value="12">05:30</option>
                            <option value="13">06:00</option>
                            <option value="14">06:30</option>
                            <option value="15">07:00</option>
                            <option value="16">07:30</option>
                            <option value="17">08:00</option>
                            <option value="18">08:30</option>
                            <option value="19">09:00</option>
                            <option value="20">09:30</option>
                            <option value="21">10:00</option>
                            <option value="22">10:30</option>
                            <option value="23">11:00</option>
                            <option value="24">11:30</option>
                            <option value="25">12:00</option>
                            <option value="26">12:30</option>
                            <option value="27">13:00</option>
                            <option value="28">13:30</option>
                            <option value="29">14:00</option>
                            <option value="30">14:30</option>
                            <option value="31">15:00</option>
                            <option value="32">15:30</option>
                            <option value="33">16:00</option>
                            <option value="34">16:30</option>
                            <option value="35">17:00</option>
                            <option value="36">17:30</option>
                            <option value="37">18:00</option>
                            <option value="38">18:30</option>
                            <option value="39">19:00</option>
                            <option value="40">19:30</option>
                            <option value="41">20:00</option>
                            <option value="42">20:30</option>
                            <option value="43">21:00</option>
                            <option value="44">21:30</option>
                            <option value="45">22:00</option>
                            <option value="46">22:30</option>
                            <option value="47">23:00</option>
                            <option value="48">23:30</option>
                        </select>

                    <div class="container">
                        <table class="table table-borderless">
                            <tbody>
                            <c:forEach var="source" items="${sources}">
                                <tr>
                                    <td>
                                        <label style="padding-left: 50px; font-size: 18px; height: 30px;" class="checkboxContainer">${source.sourceName}
                                            <input class="newsCheckbox" id="${source.sourceID}" style="width: 100%; height: 30px; left: 0; z-index: 2;" type="checkbox">
                                            <span class="checkmark"></span>
                                        </label>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                        <input type="hidden" id="sourceIdList" name="sourceIdList" value="">
                    </div>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <input style="display:inline; width: 70%" class="mdl-textfield__input" type="text" id="text2" placeholder="Schlagwort" />
                        <button onclick="addNewSearchword(true); eventlisteners();" type="button" style="display:inline; width:28%; border-radius: 0 !important;">Hinzufügen</button>
                        <!--<label class="mdl-textfield__label" for="text2" ></label>-->
                    </div>
                        <p id="searchwordErrorMsg" style="color: red;"></p>
                </div>

                <ul class="searchwords">
                </ul>

                <input type="hidden" name="searchwordsInput" id="searchwordsInput" value="">

                <script>
                    function eventlisteners() {
                        var closebtns = document.getElementsByClassName("deleteSearchword");
                        var i;

                        for (i = 0; i < closebtns.length; i++) {
                            closebtns[i].addEventListener("click", function () {
                                var id = this.parentElement.innerHTML.substring(0,this.parentElement.innerText.length-2);
                                var li = document.getElementById(id);
                                if(li != null){
                                    li.remove();
                                }

                                var index = searchwords.indexOf(id);
                                if (index > -1) {
                                    searchwords.splice(index, 1);
                                }
                            });
                        }
                    }
                </script>
                    <p style="color: red;" id="inputError"></p>
                </div>

            <div class="btn-wrapper">
                <button style="border-radius: 0; width: 28%; height: 70px; font-size: 25px;" class="mdl-button mdl-js-button" id="cancel">Zurück</button>
                <button id="createButtonSubmit" form="createProfileForm" style="border-radius: 0; width: 70%; height: 70px; font-size: 25px;" class="mdl-button mdl-js-button mdl-button--primary">Erstellen</button>
            </div>

            </form>
        </div>

        <div class="mdl-grid">
            <div class="mdl-cell mdl-cell--5-col mdl-cell--12-col-tablet mdl-shadow--2dp center m-t-100">
            </div>
        </div>
<!-- Floating Button Ende -->

<!-- Floating Button: Profil ändern -->
<div id="overlayChange" onclick="closeFAB();"></div>
<div class="fabChange">
    <!--<img src="../../resources/media/threeDots_showMore_Icon.png">-->
    <form class='cntt-wrapper' id="createProfileFormChange" name="createProfileFormChange" action="/clipping/changeProfileData" method="post">
        <div id="fab-hdrChange">
            <h3>Profil ändern</h3>
        </div>

        <div class="cnttChange">
            <div class="form-group">
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    <input required class="mdl-textfield__input" type="text" id="profilenameChange" name="profilenameChange" placeholder="Profilname" />
                    <input type="hidden" id="oldProfilenameChange" name="oldProfilenameChange" value=""/>
                    <!--<label class="mdl-textfield__label" for="text2"></label>-->
                </div>
                <br>

                <!--<div class="input-group">-->
                <input type="hidden" name="sendingTimeSelectValueChange" id="sendingTimeSelectValueChange" value="">
                <select required name="emailSendingTimeSelectChange" class="custom-select" id="emailSendingTimeSelectChange">
                    <option value="" selected>Tägliche E-Mail Sendezeit wählen...</option>
                    <option value="1">00:00</option>
                    <option value="2">00:30</option>
                    <option value="3">01:00</option>
                    <option value="4">01:30</option>
                    <option value="5">02:00</option>
                    <option value="6">02:30</option>
                    <option value="7">03:00</option>
                    <option value="8">03:30</option>
                    <option value="9">04:00</option>
                    <option value="10">04:30</option>
                    <option value="11">05:00</option>
                    <option value="12">05:30</option>
                    <option value="13">06:00</option>
                    <option value="14">06:30</option>
                    <option value="15">07:00</option>
                    <option value="16">07:30</option>
                    <option value="17">08:00</option>
                    <option value="18">08:30</option>
                    <option value="19">09:00</option>
                    <option value="20">09:30</option>
                    <option value="21">10:00</option>
                    <option value="22">10:30</option>
                    <option value="23">11:00</option>
                    <option value="24">11:30</option>
                    <option value="25">12:00</option>
                    <option value="26">12:30</option>
                    <option value="27">13:00</option>
                    <option value="28">13:30</option>
                    <option value="29">14:00</option>
                    <option value="30">14:30</option>
                    <option value="31">15:00</option>
                    <option value="32">15:30</option>
                    <option value="33">16:00</option>
                    <option value="34">16:30</option>
                    <option value="35">17:00</option>
                    <option value="36">17:30</option>
                    <option value="37">18:00</option>
                    <option value="38">18:30</option>
                    <option value="39">19:00</option>
                    <option value="40">19:30</option>
                    <option value="41">20:00</option>
                    <option value="42">20:30</option>
                    <option value="43">21:00</option>
                    <option value="44">21:30</option>
                    <option value="45">22:00</option>
                    <option value="46">22:30</option>
                    <option value="47">23:00</option>
                    <option value="48">23:30</option>
                </select>

                <div class="container">
                    <table class="table table-borderless">
                        <tbody>
                        <c:forEach var="source" items="${sources}">
                            <tr>
                                <td>
                                    <label style="padding-left: 50px; font-size: 18px; height: 30px;" class="checkboxContainer">${source.sourceName}
                                        <input class="newsCheckboxChange" id="${source.sourceID}Change" style="width: 100%; height: 30px; left: 0; z-index: 2;" type="checkbox">
                                        <span class="checkmark"></span>
                                    </label>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                    <input type="hidden" id="sourceIdListChange" name="sourceIdListChange" value="">
                </div>
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                    <input style="display:inline; width: 70%" class="mdl-textfield__input" type="text" id="text2Change" placeholder="Schlagwort" />
                    <button onclick="addNewSearchwordChange(); eventlistenersChange();" type="button" style="display:inline; width:28%; border-radius: 0 !important;">Hinzufügen</button>
                    <!--<label class="mdl-textfield__label" for="text2" ></label>-->
                </div>
                <p id="searchwordErrorMsgChange" style="color: red;"></p>
            </div>

            <ul class="searchwordsChange">
            </ul>

            <input type="hidden" name="searchwordsInputChange" id="searchwordsInputChange" value="">

            <script>
                function eventlistenersChange() {
                    var closebtns = document.getElementsByClassName("deleteSearchwordChange");
                    var i;

                    for (i = 0; i < closebtns.length; i++) {
                        closebtns[i].addEventListener("click", function () {
                            var id = this.parentElement.innerHTML.substring(0,this.parentElement.innerText.length-2);
                            var li = document.getElementById(id);
                            if(li != null){
                                li.remove();
                            }

                            var index = searchwordsChange.indexOf(id);
                            if (index > -1) {
                                searchwordsChange.splice(index, 1);
                            }
                        });
                    }
                }
            </script>
            <p style="color: red;" id="inputErrorChange"></p>
        </div>

        <div class="btn-wrapper">
            <button type="button" style="border-radius: 0; width: 28%; height: 70px; font-size: 25px;" class="mdl-button mdl-js-button" id="cancelChange">Zurück</button>
            <button type="button" form="createProfileForm" style="border-radius: 0; width: 70%; height: 70px; font-size: 25px;" class="mdl-button mdl-js-button mdl-button--primary" id="changeButtonSubmit">Ändern</button>
        </div>

    </form>
</div>



<!-- SCHLAGWORTLISTE -->
<script>
    /* toggle checkbox when list group item is clicked */
    $('.list-group a').click(function(e){

        e.stopPropagation();

        var $this = $(this).find("[type=checkbox]");
        if($this.is(":checked")) {
            $this.prop("checked",false);
        }
        else {
            $this.prop("checked",true);
        }

        if ($this.hasClass("all")) {
            $this.trigger('click');
        }
    });
    $('.all').click(function(e){
        e.stopPropagation();
        var $this = $(this);
        if($this.is(":checked")) {
            $this.parents('.list-group').find("[type=checkbox]").prop("checked",true);
        }
        else {
            $this.parents('.list-group').find("[type=checkbox]").prop("checked",false);
            $this.prop("checked",false);
        }
    });

    $('[type=checkbox]').click(function(e){
        e.stopPropagation();
    });

    $('.remove').click(function(){
        $('.all').prop("checked",false);
        var items = $("#list2 input:checked:not('.all')");
        items.each(function(idx,item){
            var choice = $(item);
            choice.prop("checked",false);
            choice.parent().appendTo("#list1");
        });
    });
</script>


    <script>
        function load(firstProfileID, errorMsgText, successMsgText) {
            var nanobar = new Nanobar();
            nanobar.go(30);
            if(firstProfileID != null && firstProfileID.toString().length > 0) {
                showArticlesFromProfileID(firstProfileID);
            }
            document.getElementById("templateSelect").addEventListener("change", prepareTemplateFilling);
            var successMsg = getUrlParameter('successMsg');
            if(successMsg && successMsg != null){
                toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": true,
                    "positionClass": "toast-top-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };

                toastr.success(successMsgText);
            }

            var errorMsg = getUrlParameter('errorMsg');
            if(errorMsg && errorMsg != null){
                toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": true,
                    "positionClass": "toast-top-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };

                toastr["error"]('Error: \r\n' + errorMsgText);
            }
            window.history.pushState("", "", '/clipping');

            document.getElementById("changeButtonSubmit").onclick = function (){
                var form = document.getElementById("createProfileFormChange");
                for(var i=0; i < form.elements.length; i++){
                    if(form.elements[i].hasAttribute('required') && form.elements[i].value === ''){
                        document.getElementById("inputErrorChange").innerHTML = 'Überprüfen Sie ihre Eingaben, Sie haben nicht alle erforderlichen Felder ausgefüllt!';
                        setTimeout(function(){
                            document.getElementById("inputErrorChange").innerHTML = '';
                        }, 3000);
                        return false;
                    }
                }
                var foundCheckedBox = false;
                var checkboxes = document.getElementsByClassName("newsCheckboxChange");
                for(var m=0; m<checkboxes.length; m++){
                    if(checkboxes[m].checked){
                        foundCheckedBox = true;
                    }
                }
                if(!foundCheckedBox || searchwordsChange.length < 1){
                    document.getElementById("inputErrorChange").innerHTML = 'Überprüfen Sie ihre Eingaben, Sie haben nicht alle erforderlichen Felder ausgefüllt!';
                    setTimeout(function(){
                        document.getElementById("inputErrorChange").innerHTML = '';
                    }, 3000);
                    return false;
                }
                createSearchwordInputChange();
            };

            document.getElementById("createButtonSubmit").onclick = function (){
                var formCreate = document.getElementById("createProfileForm");
                for(var i=0; i < formCreate.elements.length; i++){
                    if(formCreate.elements[i].hasAttribute('required') && formCreate.elements[i].value === ''){
                        document.getElementById("inputError").innerHTML = 'Überprüfen Sie ihre Eingaben, Sie haben nicht alle erforderlichen Felder ausgefüllt!';
                        setTimeout(function(){
                            document.getElementById("inputError").innerHTML = '';
                        }, 3000);
                        return false;
                    }
                }
                var foundCheckedBox = false;
                var checkboxes = document.getElementsByClassName("newsCheckbox");
                for(var m=0; m<checkboxes.length; m++){
                    if(checkboxes[m].checked){
                        foundCheckedBox = true;
                    }
                }
                if(!foundCheckedBox || searchwords.length < 1){
                    document.getElementById("inputError").innerHTML = 'Überprüfen Sie ihre Eingaben, Sie haben nicht alle erforderlichen Felder ausgefüllt!';
                    setTimeout(function(){
                        document.getElementById("inputError").innerHTML = '';
                    }, 3000);
                    return false;
                }
                createInputForSubmitting();
            };

            nanobar.go(100);
        }
    </script>

    <script src="../../resources/js/nanobar.js"></script>
    <link rel="script" href="../../resources/js/clipping.js">
    <script src="../../resources/js/index.js"></script>
    <script src="../../resources/js/floatingbtn.js"></script>
    <script src="../../resources/js/stickynavbar.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.2/js/toastr.min.js"></script>


</body>
</html>
