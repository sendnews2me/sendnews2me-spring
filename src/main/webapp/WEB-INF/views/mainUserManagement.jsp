<%--
Description:
  Diese View bietet dem Main-Admin die Möglichkeit seine registrierten Mitarbeiter zu verwalten.

--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Userverwaltung</title>

    <link rel="stylesheet" href="../../resources/css/style_1.css">
    <link rel="stylesheet" href="../../resources/css/floatingbtn.css">
    <link rel="stylesheet" href="../../resources/css/stickysidebar.css">
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

    <!--<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

    <!-- RESPONSIVE NAVBAR -->
    <c:if test="${sessionScope.user == null}">
        <%-- User ist noch nicht eingeloggt: Gast Navigationsleiste --%>
        <link rel="stylesheet" href="../../resources/css/navbarStyle_StandardUser.css">
    </c:if>
    <c:if test="${sessionScope.user != null && sessionScope.permits == 1}">
        <%-- Normaler User ist eingeloggt: User Navigationsleiste --%>
        <link rel="stylesheet" href="../../resources/css/navbarStyle_StandardUser.css">
    </c:if>
    <c:if test="${sessionScope.user != null && sessionScope.permits == 2}">
        <%-- Firmen-Admin ist eingeloggt: Firmen-Admin Navigationsleiste --%>
        <link rel="stylesheet" href="../../resources/css/navbarStyle_Admin.css">
    </c:if>
    <c:if test="${sessionScope.user != null && sessionScope.permits >= 3}">
        <%-- Firmen-Admin ist eingeloggt: Firmen-Admin Navigationsleiste --%>
        <link rel="stylesheet" href="../../resources/css/navbarStyle_Admin.css">
    </c:if>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="../../resources/js/index.js"></script>

</head>

<body onload="load('${param.errorMsg}', '${param.successMsg}');">

<header role="banner" class="header">
    <div id="cd-logo"><a class="cd-logo" href="/"><img src="../../resources/media/Logo_white.png" height="40px"
                                                       alt="Logo"></a></div>

    <nav class="main-nav">
        <c:if test="${sessionScope.user == null}">
            <%-- User ist noch nicht eingeloggt: Gast Navigationsleiste --%>
            <ul>
                <li><a class="cd-signin" style="color: white">Anmelden</a></li>
                <li><a class="cd-signup" style="color: white">Registrieren</a></li>
            </ul>
        </c:if>
        <c:if test="${sessionScope.user != null && sessionScope.permits == 1}">
            <%-- Normaler User ist eingeloggt: User Navigationsleiste --%>
            <ul>
                <li><a class="cd-navbutton" href="/clipping">Pressespiegel</a></li>
                <li><a  ></a></li>
                <li><a class="cd-navbutton"href="/account/settings">Einstellungen</a></li>
                <li><a class="cd-logout" href="/account/logout">Abmelden</a></li>
            </ul>
        </c:if>
        <c:if test="${sessionScope.user != null && sessionScope.permits == 2}">
            <%-- Firmen-Admin ist eingeloggt: Firmen-Admin Navigationsleiste --%>
            <ul>
                <li><a class="cd-navbutton" href="/clipping">Pressespiegel</a></li>
                <!--<li><a class="cd-navbutton" href="/clipping/suggestSource">Quellverwaltung</a></li>-->
                <li><a class="cd-navbutton" href="/clipping/template">Templates</a></li>
                <li><a class="cd-navbutton" href="/account/logging">Logdateien</a></li>
                <li><a class="cd-navbutton" href="/account/manageUsers">Mitarbeiter</a></li>
                <li><a  ></a></li>
                <li><a class="cd-navbutton"href="/account/settings" <%--style="background: #7d7d7d; border: #3E5D89"--%>>Einstellungen</a></li>
                <li><a class="cd-logout" href="/account/logout">Abmelden</a></li>
            </ul>
        </c:if>
        <c:if test="${sessionScope.user != null && sessionScope.permits >= 3}">
            <%-- Firmen-Admin ist eingeloggt: Firmen-Admin Navigationsleiste --%>
            <ul>
                <li><a class="cd-navbutton" href="/clipping">Pressespiegel</a></li>
                <li><a class="cd-navbutton" href="/clipping/suggestSource">Quellen</a></li>
                <!--<li><a class="cd-navbutton" href="/clipping/template">Templateverwaltung</a></li>-->
                <li><a class="cd-navbutton" href="/account/logging">Logdateien</a></li>
                <li><a class="cd-navbutton" href="/account/mainManageUsers">Nutzer</a></li>
                <li><a  ></a></li>
                <li><a class="cd-navbutton"href="/account/settings" <%--style="background: #7d7d7d; border: #3E5D89"--%>>Einstellungen</a></li>
                <li><a class="cd-logout" href="/account/logout">Abmelden</a></li>
            </ul>
        </c:if>
    </nav>
</header>


<div class="w3-row">
    <!--<div class="w3-col s1 m2 l3"><p>links</p></div>-->
    <div>
        <div class="container" style="overflow-x: auto;">
            <br>
            <br>
            <div class="topLine_Firma"><p>Neue Firma hinzufügen / löschen</p></div>
            <div class="middleDiv_Firma" >
                <form class="nameForm" name="addFirma" action="/updateCompany" method="post" >
                    <input type="text" name="FirmenName" placeholder="Name der Firma">
                    <input type="hidden" name="companyUpdateHow" id="companyUpdateHow" value="1">
                    <button type="button" onclick="document.getElementById('companyUpdateHow').value='0'; this.form.submit();" class="buttonDistance">Löschen</button>
                    <button type="button" onclick="document.getElementById('companyUpdateHow').value='1'; this.form.submit();" class="buttonDistance">Hinzufügen</button>
                </form>
            </div>
            <br>
            <br>
            <h2 style="color: white;">Userverwaltung</h2>
            <br>
            <p style="color:white;">Die folgenden Benutzer sind registriert:</p>
            <br>
            <table style="color: white;" class="table table-hover">
                <thead>
                <tr>
                    <th>Email</th>
                    <th>Validiert</th>
                    <th>Validieren</th>
                    <th>Firma</th>
                    <th>Admin</th>
                    <th>Löschen</th>
                </tr>
                </thead>
                <tbody> <!-- Show all users from the same company -->
                <c:forEach var="user" items="${allUsers}">
                    <form id="formUpdateUser${user.userID}" name="formUpdateUser" action="/account/updateUser" method="post">
                        <tr>
                            <td style="color: white;"><input type="hidden" id="permitToggleEmail${user.userID}" name="email"
                                                             value="${user.userMail}">${user.userMail}</td>
                            <td style="color: white;">
                                <c:if test="${user.validated == false}">
                                    <img class="validationimg" src="../../resources/media/roteskreuz.png"
                                         style="width:30px;height:30px; margin-top: 10px;">
                                </c:if>
                                <c:if test="${user.validated == true}">
                                    <img class="validationimg" src="../../resources/media/grünerhacken.png"
                                         style="width:35px;height:30px; margin-top: 10px;">

                                </c:if>
                            </td>
                            <td>
                                <c:if test="${user.permits < 3}">
                                    <c:if test="${sessionScope.user.userMail != user.userMail}">
                                        <c:if test="${user.validated == true}">
                                            <button type="button" onclick="this.form.validationType.value = 1; this.form.submit();">Invalidieren</button>
                                        </c:if>

                                        <c:if test="${user.validated == false}">
                                            <button type="button" onclick="this.form.validationType.value = 2; this.form.submit();">Validieren</button>
                                        </c:if>
                                        <input type="hidden" name="validationType" id="validationType${user.userID}" value="0">
                                    </c:if>
                                </c:if>
                            </td>
                            <td>
                                <input type="hidden" name="companyID" value="${user.companyID}">${user.companyName}
                            </td>
                            <td style="color: white;">
                                <c:if test="${user.permits < 3}">
                                    <!-- Rounded switch -->
                                    <c:if test="${sessionScope.user.userMail != user.userMail}">

                                        <c:if test="${user.permits == 1}">
                                            <label class="switch">
                                                <input onclick="this.form.submit()" name="toggle" type="checkbox">
                                                <span class="slider round"></span>
                                            </label>
                                        </c:if>
                                        <c:if test="${user.permits == 2}">
                                            <label class="switch">
                                                <input onclick="this.form.submit()" name="toggle" type="checkbox"
                                                       checked="checked">
                                                <span class="slider round"></span>
                                            </label>
                                        </c:if>
                                    </c:if>
                                    <!--  {user.permits} -->
                                </c:if>

                            </td>
                            <td>
                                <c:if test="${user.permits < 3}">
                                    <a id="deleteTrashcan${user.userID}" style="cursor:pointer;" onclick="deleteUser('${user.userID}', '${user.userMail}');" class="trash"> &#128465; </a>
                                    <script>
                                        function deleteUser(userID, userMail){
                                            document.getElementById('formUpdateUser' + userID).validationType.value = 3;
                                            var del = confirm("Benutzer "+userMail+" wirklich löschen?");
                                            if(del){
                                                document.getElementById('formUpdateUser' + userID).submit();
                                            }
                                        }
                                    </script>
                                </c:if>
                            </td>
                        </tr>
                    </form>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
    <!--<div class="w3-col s1 m2 l3"><p>rechts</p></div>-->


</div>

<script>
    function load(errorMsgText, successMsgText) {
        var nanobar = new Nanobar();
        nanobar.go(30);
        var successMsg = getUrlParameter('successMsg');
        if(successMsg && successMsg != null){
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };

            toastr.success(successMsgText);
        }

        var errorMsg = getUrlParameter('errorMsg');
        if(errorMsg && errorMsg != null){
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };

            toastr.error('Error: \r\n' + errorMsgText);
        }
        window.history.pushState("", "", '/account/mainManageUsers');
        nanobar.go(100);
    }
</script>

<script src="../../resources/js/nanobar.js"></script>

<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

<script src="../../resources/js/clipping.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.2/js/toastr.min.js"></script>

</body>

</html>
