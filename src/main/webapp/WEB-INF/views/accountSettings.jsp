<%--
  Created by IntelliJ IDEA.
  User: fpreuschoff
  Date: 04.04.2018
  Time: 13:28

  Description:
  Diese View stellt die Account-Einstellungen dar.
  -Passwort ändern
  -Account löschen
  -E-Mail ändern

  //TODO: Account-Einstellungsseite schöner machen
  //TODO: Funktionen der Account-Einstellungen im Backend implementieren

--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Account-Einstellungen</title>

    <link rel="stylesheet" href="../../resources/css/style_1.css">
    <link rel="stylesheet" href="../../resources/css/floatingbtn.css">
    <link rel="stylesheet" href="../../resources/css/stickysidebar.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">


    <!-- RESPONSIVE NAVBAR -->
    <c:if test="${sessionScope.user == null}">
        <%-- User ist noch nicht eingeloggt: Gast Navigationsleiste --%>
        <link rel="stylesheet" href="../../resources/css/navbarStyle_StandardUser.css">
    </c:if>
    <c:if test="${sessionScope.user != null && sessionScope.permits == 1}">
        <%-- Normaler User ist eingeloggt: User Navigationsleiste --%>
        <link rel="stylesheet" href="../../resources/css/navbarStyle_StandardUser.css">
    </c:if>
    <c:if test="${sessionScope.user != null && sessionScope.permits == 2}">
        <%-- Firmen-Admin ist eingeloggt: Firmen-Admin Navigationsleiste --%>
        <link rel="stylesheet" href="../../resources/css/navbarStyle_Admin.css">
    </c:if>
    <c:if test="${sessionScope.user != null && sessionScope.permits >= 3}">
        <%-- Firmen-Admin ist eingeloggt: Firmen-Admin Navigationsleiste --%>
        <link rel="stylesheet" href="../../resources/css/navbarStyle_Admin.css">
    </c:if>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>

</head>


<body onload="load('${param.errorMsg}', '${param.successMsg}');">

<header role="banner" class="header">
    <div id="cd-logo"><a class="cd-logo" href="/"><img src="../../resources/media/Logo_white.png" height="40px"
                                                       alt="Logo"></a></div>

    <nav class="main-nav">
        <c:if test="${sessionScope.user == null}">
            <%-- User ist noch nicht eingeloggt: Gast Navigationsleiste --%>
            <ul>
                <li><a class="cd-signin" style="color: white">Anmelden</a></li>
                <li><a class="cd-signup" style="color: white">Registrieren</a></li>
            </ul>
        </c:if>
        <c:if test="${sessionScope.user != null && sessionScope.permits == 1}">
            <%-- Normaler User ist eingeloggt: User Navigationsleiste --%>
            <ul>
                <li><a class="cd-navbutton" href="/clipping">Pressespiegel</a></li>
                <li><a  ></a></li>
                <li><a class="cd-navbutton"href="/account/settings">Einstellungen</a></li>
                <li><a class="cd-logout" href="/account/logout">Abmelden</a></li>
            </ul>
        </c:if>
        <c:if test="${sessionScope.user != null && sessionScope.permits == 2}">
            <%-- Firmen-Admin ist eingeloggt: Firmen-Admin Navigationsleiste --%>
            <ul>
                <li><a class="cd-navbutton" href="/clipping">Pressespiegel</a></li>
                <!--<li><a class="cd-navbutton" href="/clipping/suggestSource">Quellverwaltung</a></li>-->
                <li><a class="cd-navbutton" href="/clipping/template">Templates</a></li>
                <li><a class="cd-navbutton" href="/account/logging">Logdateien</a></li>
                <li><a class="cd-navbutton" href="/account/manageUsers">Mitarbeiter</a></li>
                <li><a  ></a></li>
                <li><a class="cd-navbutton"href="/account/settings" <%--style="background: #7d7d7d; border: #3E5D89"--%>>Einstellungen</a></li>
                <li><a class="cd-logout" href="/account/logout">Abmelden</a></li>
            </ul>
        </c:if>
        <c:if test="${sessionScope.user != null && sessionScope.permits >= 3}">
            <%-- Firmen-Admin ist eingeloggt: Firmen-Admin Navigationsleiste --%>
            <ul>
                <li><a class="cd-navbutton" href="/clipping">Pressespiegel</a></li>
                <li><a class="cd-navbutton" href="/clipping/suggestSource">Quellen</a></li>
                <!--<li><a class="cd-navbutton" href="/clipping/template">Templateverwaltung</a></li>-->
                <li><a class="cd-navbutton" href="/account/logging">Logdateien</a></li>
                <li><a class="cd-navbutton" href="/account/mainManageUsers">Nutzer</a></li>
                <li><a  ></a></li>
                <li><a class="cd-navbutton"href="/account/settings" <%--style="background: #7d7d7d; border: #3E5D89"--%>>Einstellungen</a></li>
                <li><a class="cd-logout" href="/account/logout">Abmelden</a></li>
            </ul>
        </c:if>
    </nav>
</header>

<div class="w3-row">
    <!--<div class="w3-col s1 m2 l3"><p>links</p></div>-->
    <div>
        <div class="container" style="margin-bottom: 30px; margin-top: 20px; padding: 35px; padding-top: 10px; overflow-x: auto; background: #D3D3D3;">

            <h3 style="color: black; margin-top: 10px;">Einstellungen von ${sessionScope.user.userMail}</h3>

            <hr/>

            <br>
            <h3>Passwort ändern</h3>

            <form name="changePwForm" action="/account/changePw" method="post">
                <input required style="max-width: 400px; margin-bottom: 10px;" type="password" class="form-control" name="oldPw" placeholder="Altes Passwort" aria-label="neues Passwort" >
                <input required style="max-width: 400px; margin-bottom: 10px;" type="password" class="form-control" name="newPw" placeholder="Neues Passwort" aria-label="neues Passwort" >
                <input required style="max-width: 400px; margin-bottom: 10px;" type="password" class="form-control" name="repeatedPw" placeholder="Passwort wiederholen" aria-label="neues Passwort" >
                <p>
                    <span style="color: green;">${changePwSucceed}</span>
                    <span style="color: red;">${changePwFailed}</span>
                </p>
                <input style="background: #3E5D89;" type="submit" class="btn btn-outline-secondary" value="Passwort ändern" />
            </form>

            <br>
            <hr/>
            <br>


            <c:if test="${sessionScope.user != null && sessionScope.user.validated == true}">
                <h3>Pressespiegel downloaden</h3>
                <br>
                <select id="profileSelectDownload" required style="max-width: 400px;" class="custom-select" >
                    <option value="" selected>Profil wählen...</option>
                    <c:forEach var="profile" items="${profiles}">
                        <option value="${profile.profilID}">${profile.profilName}</option>
                    </c:forEach>
                </select>
                <script>
                    function setDownloadHref(profileID, profileName){
                        document.getElementById("downloadButton").setAttribute("href", "/download/check/" + profileID + "/" + profileName);
                    }
                </script>
                <br>
                <br>
                <a style="background: #3E5D89; padding-left: 20px; padding-right: 20px; padding-top: 10px; padding-bottom: 10px; cursor:pointer; color: white; border-radius: 10px;" id="downloadButton" >Download</a>
                <br>
                <br>
                <hr/>
                <br>

                <c:if test="${sessionScope.user != null && sessionScope.permits < 3}">
                    <h3>Neue Nachrichtenquelle beantragen:</h3>
                    <form id="sendMailToSystemAdmin" action="/account/sendMailToSystemAdmin" method="post">
                        <div class="form-group">
                            <textarea required style="margin-left: -12px;" name="textareaEmailText" form="sendMailToSystemAdmin" class="form-control" rows="5" id="comment" placeholder="Nachrichtenquelle mit Begründung eintragen..."></textarea>
                        </div>
                        <input style="background: #3E5D89;" type="submit" class="btn btn-outline-secondary" value="Beantragen" />
                    </form>

                    <br>
                    <hr/>
                    <br>
                </c:if>
            </c:if>

            <form name="deleteAccount" method="post" action="/account/delete" onsubmit="return confirm('Soll der Account wirklich gelöscht werden?')">
                <input type="submit" class="btn btn-outline-secondary" value="Account löschen" style="background-color: #a60000;"/>
            </form>
            <br>
        </div>
    </div>
</div>


<script>
    function load(errorMsgText, successMsgText) {
        var nanobar = new Nanobar();
        nanobar.go(30);
        try{
        var successMsg = getUrlParameter('successMsg');
        if(successMsg && successMsg != null){
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };

            toastr.success(successMsgText);
        }

        var errorMsg = getUrlParameter('errorMsg');
        if(errorMsg && errorMsg != null){
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };

            toastr.error('Error: \r\n' + errorMsgText);
        }
        }catch(e){
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };

            toastr.error('Error');
        }
        window.history.pushState("", "", '/account/settings');

        document.getElementById("profileSelectDownload").onchange = function(){
            setDownloadHref(document.getElementById("profileSelectDownload").options[document.getElementById("profileSelectDownload").selectedIndex].value, document.getElementById("profileSelectDownload").options[document.getElementById("profileSelectDownload").selectedIndex].text);
        };

        nanobar.go(100);
    }
</script>

<script src="../../resources/js/nanobar.js"></script>

<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

<script src="../../resources/js/index.js"></script>
<script src="../../resources/js/clipping.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.2/js/toastr.min.js"></script>

</body>

</html>
